# Cloqd

Cloqd is the official reference implementation of the Cloq node daemon.

## Cloq Protocol Golang Runtime Environment

The Cloq Protocol runtime environment is a fully featured implementation
of the protocol.

### Build From Source

We recommend you build the node application from source.

* Install [Golang](https://golang.org/)
* Clone the repository locally :
  * Clone using SSH

    ```sh
    git clone git@gitlab.com:cloq/go-cloq.git
    ```

* Fetch the dependencies using Go mod :

    ```sh
    cd go-cloq/
    go mod download
    ```

* Build the binary

    ```sh
    cd cmd/
    go build
    ```

* Run the pilot node

    ```sh
    ./tokei --pilot=true --miner=true
    ```
