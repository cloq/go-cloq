package app

import (
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/modules/chain"
	"gitlab.com/cloq/go-cloq/modules/storage"

	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/service/ac"
	"gitlab.com/cloq/go-cloq/service/centrifuge"
)

// NodeApp contains all the high level states and workflow for Node module
type NodeApp struct {
	// config is the config passed to the app.
	NodeConfig  config.NodeConfig
	chainConfig config.ChainConfig
	Node        *centrifuge.Node
	exitChan    chan bool
	exited      sync.Mutex
}

// NewNodeApp initiates a node application
func NewNodeApp(nodeConfig config.NodeConfig, chainConfig config.ChainConfig, genesisBlock *types.Block) (*NodeApp, error) {

	dataDir := nodeConfig.DataDirectory
	db, err := storage.NewStorage(dataDir, false)
	if err != nil {
		fmt.Println("failed to create db service with error ", err)
		return nil, err
	}
	miningConf := config.MinerConfig{Active: false}

	if nodeConfig.Pilot || nodeConfig.Miner {
		miningConf.Active = true
		miningConf.Active = true
	}

	Blockchain, err := chain.New(&chainConfig, db)
	if err != nil {
		fmt.Println("failed to create blockchain service with error ", err)
		return nil, err
	}
	err = Blockchain.LoadGenesis(genesisBlock)
	if err != nil {
		fmt.Println("failed to load genesis block with error :", err)
		return nil, err
	}

	AdmissionControl := ac.NewService(chainConfig, Blockchain, miningConf)
	Node := centrifuge.NewNode(chainConfig, nodeConfig, AdmissionControl, Blockchain)

	app := &NodeApp{
		NodeConfig:  nodeConfig,
		chainConfig: chainConfig,
		Node:        Node,
		exitChan:    make(chan bool, 1),
	}
	app.exited.Lock()

	return app, nil
}

// Start node app
func (app *NodeApp) Start() {

	app.Node.Start()
	app.WaitForExit()

	signalHandler := make(chan os.Signal, 1)
	signal.Notify(signalHandler, os.Interrupt, syscall.SIGTERM)

	go app.listenForInterrupts(signalHandler)

}

func (app *NodeApp) Exit() {
	app.Node.Shutdown()
	app.exited.Unlock()
	os.Exit(0)
}

// listenForInterrupts
func (app *NodeApp) listenForInterrupts(signalHandler chan os.Signal) {
	<-signalHandler

	app.exitChan <- true
}
func (app *NodeApp) WaitForExit() {
	<-app.exitChan

	app.Exit()

	fmt.Println("Exiting...")
}
