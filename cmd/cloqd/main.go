package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/cloq/go-cloq/cmd/cloqd/app"
	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/core/types"
)

func init() {
}

func main() {
	app := &cli.App{
		Action: StartNode,
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:        "port",
				Usage:       "Use a randomized port",
				Value:       0,
				DefaultText: "random",
			},
			&cli.BoolFlag{
				Name:        "pilot",
				Usage:       "running in pilot mode",
				EnvVars:     nil,
				FilePath:    "",
				Required:    true,
				Hidden:      false,
				Value:       false,
				DefaultText: "False",
				Destination: nil,
				HasBeenSet:  false,
			},
			&cli.BoolFlag{
				Name:        "miner",
				Aliases:     nil,
				Usage:       "Run miner",
				EnvVars:     nil,
				FilePath:    "",
				Required:    true,
				Hidden:      false,
				Value:       false,
				DefaultText: "False",
				Destination: nil,
				HasBeenSet:  false,
			},
			&cli.StringSliceFlag{
				Name:        "bootstrap",
				Aliases:     nil,
				Usage:       "Connect to Bootstrap Peers trough Libp2p MultiAddress",
				EnvVars:     nil,
				FilePath:    "",
				Required:    false,
				Hidden:      false,
				TakesFile:   false,
				Value:       nil,
				DefaultText: "",
				HasBeenSet:  false,
				Destination: nil,
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}

func StartNode(ctx *cli.Context) error {

	pilotFlag := ctx.Bool("pilot")
	minerFlag := ctx.Bool("miner")
	bootstrapPeers := ctx.StringSlice("bootstrap")

	fmt.Printf("Miner : %t | Pilot : %t | Bootstrap Peer : %s", pilotFlag, minerFlag, bootstrapPeers)

	blockJSONBytes, err := ioutil.ReadFile("genesis.json")
	if err != nil {
		panic(err)
	}
	blk := types.NewEmptyBlock()
	err = json.Unmarshal(blockJSONBytes, blk)
	if err != nil {
		panic(err)
	}
	genesisHash, _ := blk.Hash()
	chainConfig := config.WhirlpoolTestnet(genesisHash)

	cfg := config.NewConfig(chainConfig, pilotFlag, minerFlag, bootstrapPeers)

	node, err := app.NewNodeApp(cfg, chainConfig, blk)
	if err != nil {
		panic(err)
	}

	node.Start()

	return nil
}
