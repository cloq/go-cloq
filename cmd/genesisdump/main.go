package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"gitlab.com/cloq/go-cloq/modules/chain"
	"gitlab.com/cloq/go-cloq/protocol"
)

func main() {

	genesisBlock, genesisHash, err := chain.CreateGenesisBlock(
		[]byte("genesis-dump-for-cloq"),
		time.Now().Unix(),
		"1clqonwardpirates",
		protocol.BaseReward,
		protocol.RegressionPoWLimit,
	)

	genesisBlockJSON, err := json.MarshalIndent(genesisBlock, "", "	")
	if err != nil {
		fmt.Println("failed to create genesis block with error :", err)
		return
	}

	err = ioutil.WriteFile("genesis.json", genesisBlockJSON, 0666)
	if err != nil {
		fmt.Println("failed to create genesis block with error :", err)
		return
	}
	fmt.Println("Created New Genesis Block")
	fmt.Println(genesisHash.String())
	fmt.Println(string(genesisBlockJSON))
}
