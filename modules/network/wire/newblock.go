package wire

import (
	"bytes"
)

// BlockSummary represents a summary of the new block.
type BlockSummary struct {
	Hash       []byte `cbor:"blockHash"`  // 32-byte hash of the header
	Difficulty []byte `cbor:"difficulty"` // block's difficulty NOT cumulative difficulty a/k/a chainwork
	TXCount    uint32 `cbor:"txCount"`    // transaction count in the block
}

var (
	// MaxBlockPayload is a softlimit on total single block message size
	MaxBlockPayload = 4000000
)

// NewBlockMessage encapsulates blocks for broadcasting and replying to getblocks.
type NewBlockMessage struct {
	Header BlockSummary `cbor:"blockSummary"`
	Block  []byte       `cbor:"blockBody"`
}

// Marshal implements the serialize interface
func (blkMsg NewBlockMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, blkMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalNewBlockMessage implements the deserialize interface
func UnmarshalNewBlockMessage(buf []byte) (NewBlockMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewBlockMessage{}, err
	}
	blksMsg := msg.(NewBlockMessage)
	return blksMsg, nil
}

// NewNewBlockMessage creates a new instance of block message
func NewNewBlockMessage(hash, diff []byte, txcount uint32, blockBytes []byte) NewBlockMessage {

	return NewBlockMessage{
		Header: BlockSummary{
			Hash:       hash,
			Difficulty: diff,
			TXCount:    txcount,
		},
		Block: blockBytes,
	}
}

// Topic returns message topic
func (blkMsg NewBlockMessage) Topic() uint32 {
	return NewBlock
}

// Checksum comptues payload checksum
func (blkMsg NewBlockMessage) Checksum() []byte {
	return checksum(blkMsg)
}
