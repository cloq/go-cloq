package wire

import (
	"bytes"
)

//StatusOKMessage is an empty message where we acknowledge peer.
type StatusOKMessage struct {
	ACK []byte
}

// Marshal implements the serialize interface
func (statusOK StatusOKMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, statusOK, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalStatusOKMessage implements the deserialize interface
func UnmarshalStatusOKMessage(buf []byte) (StatusOKMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return StatusOKMessage{}, err
	}
	statusOKMsg := msg.(StatusOKMessage)
	return statusOKMsg, nil
}

// NewStatusOKMessage creates an instance of ACK message
func NewStatusOKMessage() StatusOKMessage {
	return StatusOKMessage{
		ACK: []byte("OK"),
	}
}

// Topic implements the Message interface
func (statusOK StatusOKMessage) Topic() uint32 {
	return StatusOK
}

// Checksum implements checksum calculation over status messages.
func (statusOK StatusOKMessage) Checksum() []byte {

	return checksum(statusOK)
}
