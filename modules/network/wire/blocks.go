package wire

import (
	"bytes"
)

var (
	// MaxBlocksPerMessage is the maximum number of blocks in a single message
	MaxBlocksPerMessage = 32
	// TestnetMagic is the current deployement magic bytes for encoding
	// network messages
	TestnetMagic uint32 = 0xafe8d4c5
)

// BlocksMessage is a response to getblocks it is a list of serialized blocks.
type BlocksMessage struct {
	BlockHashes [][]byte `cbor:"blockHashes"`
	BlockBodies [][]byte `cbor:"blockBodies"`
}

// NewBlocksMessage creates a new blocks message
func NewBlocksMessage() BlocksMessage {
	return BlocksMessage{
		BlockBodies: make([][]byte, 0, MaxBlocksPerMessage),
	}
}

// Topic returns message topic
func (blkMsg BlocksMessage) Topic() uint32 {
	return Blocks
}

// Checksum computes headers message checksum
func (blkMsg BlocksMessage) Checksum() []byte {
	return checksum(blkMsg)
}

// Marshal implements the serialize interface.
func (blkMsg BlocksMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, blkMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalBlocksMessage implements the deserialize interface.
func UnmarshalBlocksMessage(buf []byte) (BlocksMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewBlocksMessage(), err
	}
	blksMsg := msg.(BlocksMessage)
	return blksMsg, nil
}
