package wire

import (
	"bytes"
)

// TransactionMessage represents an encapsulated transaction over the wire.
type TransactionMessage struct {
	Body []byte `cbor:"txBody"` // transaction body : RLP(Tx)
}

// Marshal implements the serialize interface
func (txMsg TransactionMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, txMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalTransactionMessage implements the deserialize interface
func UnmarshalTransactionMessage(buf []byte) (TransactionMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return TransactionMessage{}, err
	}
	txMsg := msg.(TransactionMessage)
	return txMsg, nil
}

// NewTransactionMessage creates an instance of a TransactionMessage
func NewTransactionMessage(hash []byte, body []byte) TransactionMessage {
	return TransactionMessage{
		Body: body,
	}
}

// Topic returns message topic
func (txMsg TransactionMessage) Topic() uint32 {
	return NewTransaction
}

// Checksum computes payload checksum
func (txMsg TransactionMessage) Checksum() []byte {
	return checksum(txMsg)
}
