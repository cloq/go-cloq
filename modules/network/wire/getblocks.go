package wire

import (
	"bytes"
)

// GetBlocksMessage the blocks to fetch by their hashes.
type GetBlocksMessage struct {
	Start       []byte   `cbor:"startingHash"`
	BlockHashes [][]byte `cbor:"blockHashes"`
	Reverse     bool     `cbor:"reverse"`
	Full        bool     `cbor:"fullBlocks"`
}

// NewGetBlocksMessage creates a new getblocks message.
func NewGetBlocksMessage() GetBlocksMessage {
	return GetBlocksMessage{}
}

// Topic returns message topic
func (getblocksMsg GetBlocksMessage) Topic() uint32 {
	return GetBlocks
}

// Checksum returns message checksum
func (getblocksMsg GetBlocksMessage) Checksum() []byte {
	return checksum(getblocksMsg)
}

// Marshal implements the serialize interface.
func (getblocksMsg GetBlocksMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, getblocksMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalGetBlocksMessage implements the deserialize interface.
func UnmarshalGetBlocksMessage(buf []byte) (GetBlocksMessage, error) {
	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewGetBlocksMessage(), err
	}
	getBlocksMsg := msg.(GetBlocksMessage)
	return getBlocksMsg, nil
}
