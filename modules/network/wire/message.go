// Package wire implements serialization, framing of network messages.package wire
// All Messages follow a unified representation :
package wire

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"

	"github.com/fxamacker/cbor/v2"
	"golang.org/x/crypto/sha3"
)

var (
	// MessageTag is appended to payloads to compute their checksum.
	MessageTag = []byte{'q', 'n', 'e', 't', 's', 'u', 'm'}
	// ProtoVersion is the version of the p2p protocol
	ProtoVersion uint32 = 3000
)

// Message interface represents wire messages, the messages follow
// a definite structure :
// Each wire message represents a network cbor encoded byte array.
// It's composed of a header and a body.
// The header specifies the data type, network id and a checksum.
// The body encodes the message payload.
type Message interface {
	Marshal() []byte
	Topic() uint32
	Checksum() []byte
}

const (
	// MessageHeaderSize is the size of our header struct in bytes.
	// Values were choosen to give optimal padding alignment
	MessageHeaderSize = 16
)

// Header represents a message header :
// Magic : 4 bytes (specific to network parameters)
// Topic : 2 bytes (specific to a certain protocol topic)
// Length : 4 bytes (payload length)
// Checksum : 4 bytes (SHA3(PayloadData))[:4]
// Topic defines a uint32 topic code.
type Header struct {
	magic    uint32
	topic    uint32
	length   uint32
	checksum [4]byte
}

// String implements pretty printing message headers.
func (hdr Header) String() string {

	return fmt.Sprintf("Magic : %x | Topic : %x | Length : %d | Checksum : %x", hdr.magic, hdr.topic, hdr.length, hdr.checksum[:])
}

// NewHeader creates a new header given a message and a protocol version.
func NewHeader(magic uint32, topic uint32, length uint32, checksum [4]byte) Header {

	var hdr = Header{
		magic:    magic,
		topic:    topic,
		length:   length,
		checksum: checksum,
	}

	return hdr
}

// ReadHeader reads a message header from a io.Reader
func ReadHeader(r io.Reader) (Header, int, error) {

	var hdr Header

	err := binary.Read(r, binary.LittleEndian, &hdr.magic)

	if err != nil {
		return hdr, 0, err
	}

	err = binary.Read(r, binary.LittleEndian, &hdr.topic)

	if err != nil {
		return hdr, 0, err
	}

	err = binary.Read(r, binary.LittleEndian, &hdr.length)

	if err != nil {
		return hdr, 0, err
	}

	_, err = r.Read(hdr.checksum[:])
	if err != nil {
		return hdr, 0, err
	}

	return hdr, 0, nil
}

// WriteHeader writes a message header to a io.Writer
func WriteHeader(w io.Writer, hdr Header) (int, error) {

	var err error
	var n int

	err = binary.Write(w, binary.LittleEndian, &hdr.magic)
	if err != nil {
		return n, err
	}
	n = n + 4

	err = binary.Write(w, binary.LittleEndian, &hdr.topic)
	if err != nil {
		return n, err
	}
	n = n + 4

	err = binary.Write(w, binary.LittleEndian, &hdr.length)
	if err != nil {
		return n, err
	}
	_, err = w.Write(hdr.checksum[:])

	return (n + 4), err
}

// WriteMessage writes a message to a writer.
func WriteMessage(w io.Writer, msg Message, magic uint32) (int, error) {

	payload, err := cbor.Marshal(msg)
	if err != nil {
		return 0, err
	}

	payloadLength := uint32(len(payload))

	var hdr = NewHeader(magic, msg.Topic(), payloadLength, payloadTagHash(payload))

	n, err := WriteHeader(w, hdr)

	if err != nil {
		return n, err
	}

	n, err = w.Write(payload)
	return n, err

}

// ReadMessage reads and parses a message from a reader.
func ReadMessage(r io.Reader) (Message, int, error) {

	hdr, n, err := ReadHeader(r)
	if err != nil {
		return nil, n, err
	}

	var topic = hdr.topic
	var checksum = hdr.checksum
	var buf = make([]byte, hdr.length)

	n, err = io.ReadFull(r, buf)
	if err != nil || uint32(n) != hdr.length {
		return nil, n, err
	}

	if payloadTagHash(buf) != checksum {
		return nil, n, errors.New("checksum mismatch")
	}

	msg := NewEmptyMessage(topic)

	if msg == nil {
		return nil, 0, errors.New("invalid message topic")
	}
	err = cbor.Unmarshal(buf, msg)

	return msg, n, err
}

// UnmarshalWireMessage XXX
func UnmarshalWireMessage(buf []byte) (Message, error) {

	r := bytes.NewReader(buf)

	message, _, err := ReadMessage(r)

	return message, err
}

// MarshalWireMessage XXX
func MarshalWireMessage(msg Message) []byte {
	buf := new(bytes.Buffer)

	_, err := WriteMessage(buf, msg, TestnetMagic)
	if err != nil {
		return nil
	}
	return buf.Bytes()
}

// GetMessageID computes a short message identifier
func GetMessageID(msg Message) []byte {
	var sum [20]byte
	sha3 := sha3.New256()
	sha3.Write(MessageTag)
	buf := msg.Marshal()
	if buf == nil {
		return nil
	}
	sha3.Write(buf)

	copy(sum[:], sha3.Sum(nil)[28:])
	return sum[:]

}

// checksum computes the checksum of a message
func checksum(msg Message) []byte {

	var sum [4]byte
	sha3 := sha3.New256()
	sha3.Write(MessageTag)
	buf := msg.Marshal()
	if buf == nil {
		return nil
	}
	sha3.Write(buf)

	copy(sum[:], sha3.Sum(nil)[28:])
	return sum[:]

}

// payloadTagHash computes the payload tagged hash
func payloadTagHash(p []byte) [4]byte {

	var sum [4]byte

	sha3 := sha3.New256()
	sha3.Write(MessageTag)
	sha3.Write(p)

	copy(sum[:], sha3.Sum(nil)[28:])
	return sum
}

// NewEmptyMessage creates a new message given a topic
func NewEmptyMessage(topic uint32) Message {

	switch topic {
	case Status:
		return &StatusMessage{}
	case StatusOK:
		return &StatusOKMessage{}
	case GetPeers:
		return &GetPeersMessage{}
	case Peers:
		return &PeersMessage{}
	case GetData:
		return &GetDataMessage{}
	case NodeData:
		return &NodeDataMessage{}
	case Unknown:
		return &UnknownMessage{}
	case GetHeaders:
		return &GetHeadersMessage{}
	case Headers:
		return &HeadersMessage{}
	case GetBlocks:
		return &GetBlocksMessage{}
	case Blocks:
		return &BlocksMessage{}
	case NewTransaction:
		return &TransactionMessage{}
	case NewBlock:
		return &NewBlockMessage{}
	case Reject:
		return &RejectMessage{}
	case Ping:
		return &PingMessage{}
	case Pong:
		return &PongMessage{}
	default:
		return nil
	}
}
