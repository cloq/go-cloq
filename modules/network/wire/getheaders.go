package wire

import (
	"bytes"
)

// GetHeadersMessage specifies a starting hash, and an ending hash.
// If the ending hash is empty the max headers we collect is 500 starting
// from the first hash, nodes can specify an ordering with the reverse flag.
type GetHeadersMessage struct {
	StartingPoint []byte `cbor:"startingHash"` // 32-byte hash of the starting block.
	MaxHeaders    uint32 `cbor:"maxHeaders"`   // number of headers to fetch
	Reverse       bool   `cbor:"reverse"`      // if reverse is true we go backwards from startingPoint
}

// Marshal implements the serialize interface
func (getheadersMsg GetHeadersMessage) Marshal() []byte {
	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, getheadersMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalGetHeadersMessage implements the deserialize interface
func UnmarshalGetHeadersMessage(buf []byte) (GetHeadersMessage, error) {
	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewGetHeadersMessage(), err
	}
	getHeadersMsg := msg.(GetHeadersMessage)
	return getHeadersMsg, nil
}

// NewGetHeadersMessage creates a new instance of GetHeadersMessage.
func NewGetHeadersMessage() GetHeadersMessage {
	return GetHeadersMessage{}
}

// Topic returns message topic
func (getheadersMsg GetHeadersMessage) Topic() uint32 {
	return GetHeaders
}

// Checksum returns message checksum
func (getheadersMsg GetHeadersMessage) Checksum() []byte {
	return checksum(getheadersMsg)
}
