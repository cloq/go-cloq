package wire

import (
	"bytes"
)

// GetDataMessage is a prefiltered inv bundle response to inv advertisments.
type GetDataMessage struct {
	InvType `cbor:"invType"`
	Hashes  [][]byte `cbor:"hashes"`
}

// NewGetDataMessage creates a new instance of GetDataMessage
func NewGetDataMessage() GetDataMessage {
	return GetDataMessage{}
}

// Marshal implements the serialize interface
func (getdata GetDataMessage) Marshal() []byte {
	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, getdata, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalGetDataMessage implements the deserialize interface
func UnmarshalGetDataMessage(buf []byte) (GetDataMessage, error) {
	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return NewGetDataMessage(), err
	}
	getDataMsg := msg.(GetDataMessage)
	return getDataMsg, nil
}

// Topic returns GetDataMessage response topic
func (getdata GetDataMessage) Topic() uint32 {
	return GetData
}

// Checksum computes a bundle's checksum
func (getdata GetDataMessage) Checksum() []byte {
	return checksum(getdata)
}

// UnknownMessage is a response to getdata, getheaders ... in the case the data
// is not known to us.
type UnknownMessage struct {
	unknown []byte
}

// Topic returns getpeers topic 0x03 the payload is empty
func (unkMsg UnknownMessage) Topic() uint32 {
	return Unknown
}

// Checksum returns an empty checksum
func (unkMsg UnknownMessage) Checksum() []byte {
	return checksum(unkMsg)
}

// Marshal implements the serialize interface.
func (unkMsg UnknownMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, unkMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalUnknownMessage implements the deserialize interface.
func UnmarshalUnknownMessage(buf []byte) (UnknownMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return UnknownMessage{}, err
	}
	unknownMsg := msg.(UnknownMessage)
	return unknownMsg, nil
}
