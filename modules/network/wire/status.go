package wire

import (
	"bytes"
)

// StatusMessage represents an advertisment of the current node network
// information.
type StatusMessage struct {
	ProtocolVersion uint32 // P2P Protocol version
	BranchID        uint32 // NetworkID is used to specify the chain we're interested in
	GenesisHash     []byte // Genesis hash we started from
	TipHeight       uint64 // Our chain tip height
	TipHash         []byte // Our chain tip hash
	TipRoot         []byte // Our chain tip state root
	ChainWork       []byte // Our chain tip total work
}

// Marshal implements the serialize interface
func (statusMsg StatusMessage) Marshal() []byte {

	var b = new(bytes.Buffer)

	_, err := WriteMessage(b, statusMsg, TestnetMagic)

	if err != nil {
		return nil
	}

	return b.Bytes()
}

// UnmarshalStatusMessage implements the deserialize interface
func UnmarshalStatusMessage(buf []byte) (StatusMessage, error) {

	msg, err := UnmarshalWireMessage(buf)
	if err != nil {
		return StatusMessage{}, err
	}
	statusMsg := msg.(StatusMessage)
	return statusMsg, nil
}

// NewStatusMessage creates a new instance of status message
func NewStatusMessage(magic uint32, branchid uint32, bestheight uint64, bestwork, bestroot, besthash, genesis []byte) StatusMessage {

	return StatusMessage{
		ProtocolVersion: magic,
		BranchID:        branchid,
		TipHeight:       bestheight,
		ChainWork:       bestwork,
		TipHash:         besthash,
		TipRoot:         bestroot,
		GenesisHash:     genesis,
	}
}

// Topic implements the Message interface
func (statusMsg StatusMessage) Topic() uint32 {
	return Status
}

// Checksum implements checksum calculation over status messages.
func (statusMsg StatusMessage) Checksum() []byte {
	return checksum(statusMsg)
}
