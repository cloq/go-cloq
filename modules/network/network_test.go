package network

import (
	"math/big"
	"testing"
	"time"

	"github.com/perlin-network/noise"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"go.uber.org/zap"
)

func TestNetworkGossip(t *testing.T) {

	peers := NewPeerStore()
	protocolHandler := NewProtocolHandler(peers.handleConnectedPeer, peers.handleEvictedPeer, peers.handleAdmittedPeer, nil, handleGossipMessage)

	logger, _ := zap.NewDevelopment()
	defer logger.Sync()

	alice, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}

	bob, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}

	charlie, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}

	nodes := []*noise.Node{alice.Host, bob.Host, charlie.Host}

	if err := alice.Listen(); err != nil {
		panic(err)
	}

	if err := bob.Listen(); err != nil {
		panic(err)
	}

	if err := charlie.Listen(); err != nil {
		panic(err)
	}

	// Have Bob and Charlie learn about Alice. Bob and Charlie do not yet know of each other.

	if _, err := bob.Dial(alice.Host.Addr()); err != nil {
		panic(err)
	}

	if _, err := charlie.Dial(bob.Host.Addr()); err != nil {
		panic(err)
	}

	// Using Kademlia, Bob and Charlie will learn of each other. Alice, Bob, and Charlie should
	// learn about each other once they run (*kademlia.Protocol).Discover().

	t.Logf("Alice discovered %d peer(s).\n", len(alice.Discover()))
	t.Logf("Bob discovered %d peer(s).\n", len(bob.Discover()))
	t.Logf("Charlie discovered %d peer(s).\n", len(charlie.Discover()))
	for _, node := range nodes {
		for _, client := range append(node.Inbound(), node.Outbound()...) {
			client.WaitUntilReady()
		}
	}
	charlie.Broadcast(wire.NewPingMessage())
	time.Sleep(5 * time.Second)
	// Check is Nodes are strored.
	charlidID, err := alice.Peers.GetPeer(charlie.Host.ID())
	if err != nil {
		t.Fatal("failed to fetch peer information with error :", err)
	}
	t.Log(charlidID)

}

func TestNetworkPeers(t *testing.T) {

	peers := NewPeerStore()
	protocolHandler := NewProtocolHandler(peers.handleConnectedPeer, peers.handleEvictedPeer, peers.handleAdmittedPeer, nil, handleGossipMessage)

	logger, _ := zap.NewDevelopment()
	defer logger.Sync()

	alice, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}

	bob, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}

	charlie, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}

	nodes := []*noise.Node{alice.Host, bob.Host, charlie.Host}

	if err := alice.Listen(); err != nil {
		panic(err)
	}

	if err := bob.Listen(); err != nil {
		panic(err)
	}

	if err := charlie.Listen(); err != nil {
		panic(err)
	}

	// Have Bob and Charlie learn about Alice. Bob and Charlie do not yet know of each other.

	if _, err := bob.Dial(alice.Host.Addr()); err != nil {
		panic(err)
	}

	if _, err := charlie.Dial(bob.Host.Addr()); err != nil {
		panic(err)
	}

	// Using Kademlia, Bob and Charlie will learn of each other. Alice, Bob, and Charlie should
	// learn about each other once they run (*kademlia.Protocol).Discover().

	t.Logf("Alice discovered %d peer(s).\n", len(alice.Discover()))
	t.Logf("Bob discovered %d peer(s).\n", len(bob.Discover()))
	t.Logf("Charlie discovered %d peer(s).\n", len(charlie.Discover()))
	for _, node := range nodes {
		for _, client := range append(node.Inbound(), node.Outbound()...) {
			client.WaitUntilReady()
		}
	}
	charlie.Broadcast(wire.NewPingMessage())
	time.Sleep(5 * time.Second)
	// Check is Nodes are strored.
	charliePeer, err := alice.Peers.GetPeer(charlie.Host.ID())
	if err != nil {
		t.Fatal("failed to fetch peer information with error :", err)
	}
	bobPeer, err := alice.Peers.GetPeer(bob.Host.ID())
	if err != nil {
		t.Fatal("failed to fetch peer information with error :", err)
	}

	if bobPeer.ID != bob.Host.ID().ID.String() || charliePeer.ID != charlie.Host.ID().ID.String() {
		t.Fatalf("failed to assert remote peer ID expected %s got %s", bobPeer.ID, bob.Host.ID().String())
	}

	testWorkBob := big.NewInt(934738454234)
	testWorkAlice := big.NewInt(865738454234)
	testGenesis := crypto.SHA3([]byte("genesis-test"))
	bobStatus := wire.NewStatusMessage(0x9999, 0x9843b, 10012, testWorkBob.Bytes(), crypto.SHA3([]byte("bob")), crypto.SHA3([]byte("bobwork")), testGenesis)
	charlieStatus := wire.NewStatusMessage(0x9999, 0x9843b, 10012, testWorkBob.Bytes(), crypto.SHA3([]byte("bob")), crypto.SHA3([]byte("bobwork")), testGenesis)
	aliceStatus := wire.NewStatusMessage(0x9999, 0x9843b, 9000, testWorkAlice.Bytes(), crypto.SHA3([]byte("bob")), crypto.SHA3([]byte("bobwork")), testGenesis)

	alice.Peers.UpdatePeer(bob.Host.ID(), bobStatus)
	alice.Peers.UpdatePeer(charlie.Host.ID(), charlieStatus)

	bestHash, bestWork, candidates := alice.GetBestAdvertisedPeers(1, aliceStatus)
	t.Log("bestahsh", bestHash.String())
	t.Log("bestwork", bestWork.String())
	t.Log("candidate :", candidates)
}

func TestNetworkHandler(t *testing.T) {
	peers := NewPeerStore()
	protocolHandler := NewProtocolHandler(peers.handleConnectedPeer, peers.handleEvictedPeer, peers.handleAdmittedPeer, nil, handleGossipMessage)

	logger, _ := zap.NewDevelopment()
	defer logger.Sync()

	alice, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}
	bob, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}
	alice.RegisterHandler(testHandleMessage)
	bob.RegisterHandler(testHandleMessage)

	if err := alice.Listen(); err != nil {
		panic(err)
	}

	if err := bob.Listen(); err != nil {
		panic(err)
	}

	err = alice.SendMessageToPeer(bob.Host.ID(), wire.NewPingMessage())
	if err != nil {
		t.Fatal("failed to send message to peer with error :", err)
	}
	err = bob.SendMessageToPeer(alice.Host.ID(), wire.NewPongMessage())
	if err != nil {
		t.Fatal("failed to send message to peer with error :", err)
	}
}
func TestNetworkHandlerWithRequest(t *testing.T) {

	logger, _ := zap.NewDevelopment()
	defer logger.Sync()

	peers := NewPeerStore()
	protocolHandler := NewProtocolHandler(peers.handleConnectedPeer, peers.handleEvictedPeer, peers.handleAdmittedPeer, nil, handleGossipMessage)
	alice, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}
	bob, err := NewNetwork(protocolHandler, logger, peers)
	if err != nil {
		panic(err)
	}
	alice.RegisterHandler(testHandleMessage)
	bob.RegisterHandler(testHandleMessage)

	if err := alice.Listen(); err != nil {
		panic(err)
	}

	if err := bob.Listen(); err != nil {
		panic(err)
	}

	resp, err := alice.SendRequestToPeer(bob.Host.ID(), wire.NewPingMessage())
	if err != nil {
		t.Fatal("failed to send message to peer with error :", err)
	}
	msg, err := wire.UnmarshalWireMessage(resp)
	if err != nil {
		t.Fatal("failed to unmarshal message  with error :", err)
	}
	msg, ok := msg.(*wire.PongMessage)
	if !ok {
		t.Fatalf("expected message of type %T got %T", wire.PongMessage{}, msg)
	}
	t.Log("received response from peer :", msg)
	err = bob.SendMessageToPeer(alice.Host.ID(), wire.NewPongMessage())
	if err != nil {
		t.Fatal("failed to send message to peer with error :", err)
	}

	t.Log(alice.GetActivePeers())
}

// testHandleMessage is registered as a network handler for incoming peer connections.
// This method is only used to run message sending tests.
func testHandleMessage(ctx noise.HandlerContext) error {
	messageBytes := ctx.Data()
	_, err := wire.UnmarshalWireMessage(messageBytes)
	if err != nil {
		return err
	}
	if ctx.IsRequest() {
		return ctx.Send(wire.NewPongMessage().Marshal())
	}

	return nil
}
