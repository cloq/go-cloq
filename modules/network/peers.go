package network

import (
	"bytes"
	"fmt"
	"math/big"
	"sync"

	"github.com/perlin-network/noise"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"gitlab.com/cloq/go-cloq/pkg/bytesutil"
)

// Peer represents an active network peer.
// Each peer has a unique identifier ID = Hash(Public Key)
// The LastAdvertisedTip is the one from its most recent status message.
// Initially the last advertised tip is nil once a peer is connected
// it will broadcast a status message, if the peer is outbound the status
// message will be a response to the inital one sent.
// If the peer is inbound it will immediately broadcast a status message.
type Peer struct {
	ID                   string
	Client               *noise.Client
	LastAdvertisedTip    []byte
	LastAdvertisedStatus wire.StatusMessage
	Nonce                int
}

// NewPeer creates a new instance of Peer.
func NewPeer(cli *noise.Client) *Peer {
	return &Peer{
		ID:                ComputePeerID(cli.ID()),
		Client:            cli,
		LastAdvertisedTip: nil,
	}
}

// PeerStore is an on-disk storage interface for discovered peers.
// PeerStore is used to connect to peers after the node shuts down.
type PeerStore struct {
	mu    sync.RWMutex
	store map[string]*Peer
}

// NewPeerStore creates a new instance of peer store.
func NewPeerStore() *PeerStore {
	return &PeerStore{
		store: make(map[string]*Peer, 10),
	}
}

// ComputePeerID computes a peer identifier from the noise public key.
func ComputePeerID(id noise.ID) string {
	return id.ID.String()
}

// GetPeer returns a peer object by it's id
func (ps *PeerStore) GetPeer(id noise.ID) (*Peer, error) {

	ps.mu.RLock()
	defer ps.mu.RUnlock()

	val, ok := ps.store[ComputePeerID(id)]
	if !ok {
		return nil, fmt.Errorf("peer with id %s not found in store", ComputePeerID(id))
	}
	return val, nil
}

// AddNewPeer adds a new peer to the underlying peer store
func (ps *PeerStore) AddNewPeer(peer *Peer) {
	ps.mu.Lock()
	defer ps.mu.Unlock()

	ps.store[peer.ID] = peer
}

// UpdatePeer updates a peer information if it exists otherwise add it.
func (ps *PeerStore) UpdatePeer(id noise.ID, status wire.StatusMessage) {
	ps.mu.Lock()
	defer ps.mu.Unlock()

	peer := ps.store[ComputePeerID(id)]
	peer.LastAdvertisedStatus = status
	peer.LastAdvertisedTip = status.TipHash
	ps.store[peer.ID] = peer
}

// RemovePeer evicts a peer from the peer store
func (ps *PeerStore) RemovePeer(id noise.ID) {
	ps.mu.Lock()
	defer ps.mu.Unlock()

	peer, ok := ps.store[ComputePeerID(id)]
	if !ok {
		return
	}
	peer.Client.Close()
	delete(ps.store, ComputePeerID(id))
}

// getBestAdvertisedPeers returns the most up to date peer based on status selection.
func (ps *PeerStore) getBestAdvertisedPeers(maxPeers int, ourStatus wire.StatusMessage) (crypto.Hash, *big.Int, []noise.ID) {
	ps.mu.Lock()
	defer ps.mu.Unlock()

	candidateHashes := make(map[[32]byte]uint64)
	candidateRoots := make(map[[32]byte]uint64)
	candidatePeers := make(map[[32]byte]noise.ID)
	candidateChainWork := make(map[[32]byte]*big.Int)

	ourChainWork := new(big.Int).SetBytes(ourStatus.ChainWork)

	for _, peer := range ps.store {

		peerTip := peer.LastAdvertisedStatus
		// if the peer's tip is superior to ours
		if peerTip.TipHeight > ourStatus.TipHeight {
			tipHash := bytesutil.ToBytes32(peerTip.TipHash)
			tipRoot := bytesutil.ToBytes32(peerTip.TipRoot)
			candidateHashes[tipHash]++
			candidateRoots[tipRoot]++
			candidatePeers[tipHash] = peer.Client.ID()
			candidateChainWork[tipHash] = new(big.Int).SetBytes(peerTip.ChainWork)

		}
	}

	var mostVotes uint64
	var winningHash [32]byte
	var winningWork *big.Int
	bestAdvertised := make([]noise.ID, 0, maxPeers)

	for hash, count := range candidateHashes {
		if count > mostVotes {
			mostVotes = count
			winningHash = hash
			winningWork = candidateChainWork[winningHash]
		}
		if winningWork.Cmp(ourChainWork) > 0 && !bytes.Equal(winningHash[:], ourStatus.TipHash) {
			bestAdvertised = append(bestAdvertised, candidatePeers[winningHash])
		}
	}

	return winningHash, winningWork, bestAdvertised
}
