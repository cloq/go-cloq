package network

const (
	// InstanceKeyFile defines the file where we store our cryptographic keys.
	InstanceKeyFile = "node.key"
	// WhirlpoolProtocolID defines the libp2p network segregation id for Cloq.
	WhirlpoolProtocolID = "/cloq/whirlpool/1.0.0"
	// WhirlpoolDHTID defines the libp2p-kad-dht module segregation id
	WhirlpoolDHTID = "/cloq/whirlpool/kad/1.0.0"
	// MaxPeers define the maximum number of peers we connect with and handle.
	MaxPeers = 16
	// MinPeers define the minimum number of peers we connect with and handle.
	MinPeers = 4
)

// Config implements a configuration system for a given P2P service instance.
type Config struct {
	BootstrapNodes  []string // list of nodes used to bootstrap the service
	ProtocolID      string   // either Default or other
	BindingAddress  string   // Host listening IP address
	Port            uint16   // Host listening port
	MaxInboundConn  uint     // Max number of inbound connections
	MaxOutboundConn uint     // Max number of outbound connections
}
