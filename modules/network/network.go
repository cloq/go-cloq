package network

import (
	"context"
	"errors"
	"math/big"

	"gitlab.com/cloq/go-cloq/core/crypto"

	"github.com/perlin-network/noise"
	"github.com/perlin-network/noise/gossip"
	"github.com/perlin-network/noise/kademlia"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"go.uber.org/zap"
)

var (
	errMarshalMessage = errors.New("failed to marshal wire message")
)

// Network abstract the P2P networking module and handles
// connecting to remote peers, gossiping messages and peer
// discovery through s/kademlia.
type Network struct {
	ctx context.Context

	Host   *noise.Node
	Kad    *kademlia.Protocol
	Gossip *gossip.Protocol

	Peers    *PeerStore
	Inbound  []*noise.Client
	Outbound []*noise.Client
}

// NewNetwork creates a new network instance and binds the respective protocols
// to the underlying Node service.
func NewNetwork(handler ProtocolHandler, logger *zap.Logger, peerstore *PeerStore) (*Network, error) {

	host, err := noise.NewNode(noise.WithNodeLogger(logger), noise.WithNodeMaxInboundConnections(4), noise.WithNodeMaxOutboundConnections(4))
	if err != nil {
		return nil, err
	}
	ps := peerstore

	kad := kademlia.New(kademlia.WithProtocolEvents(kademlia.Events{OnPeerAdmitted: handler.OnPeerAdmitted, OnPeerEvicted: handler.OnPeerEvicted}))
	gossip := gossip.New(kad, gossip.WithEvents(gossip.Events{OnGossipReceived: handler.OnGossipReceived}))

	host.Bind(kad.Protocol(), gossip.Protocol())
	host.Bind(noise.Protocol{OnPeerConnected: handler.OnPeerConnected})
	return &Network{
		ctx:    context.Background(),
		Host:   host,
		Kad:    kad,
		Gossip: gossip,
		Peers:  ps,
	}, nil
}

// Listen initiates listening to remote peers trough a go-routine.
// The go routine runs under the called context and there's one listening
// go routine per network instance.
// To avoid mis use from the caller we explicitly panic on bad options.
func (net *Network) Listen() error {
	if err := net.Host.Listen(); err != nil {
		panic(err)
	}
	net.Host.Logger().Info("Node start listening ", zap.String("Address", net.Host.Addr()))
	return nil
}

// Dial establishes a connection to a peer from the configuration.
// Initially one peer is selected randomly from the list upon succesful
// connection a Peer object is returned with which messages can be exchanged.
func (net *Network) Dial(Address string) (*Peer, error) {
	cli, err := net.Host.Ping(net.ctx, Address)
	if err != nil {
		return nil, err
	}
	return NewPeer(cli), nil
}

// Broadcast gossips a message to all connected peers on this
// network
func (net *Network) Broadcast(message wire.Message) error {

	messageBytes := message.Marshal()
	if messageBytes == nil {
		return errMarshalMessage

	}
	net.Gossip.Push(net.ctx, messageBytes)
	return nil
}

// Discover returns a list of peers discovered by this network instance
// through the underlying kademlia instance.
func (net *Network) Discover() []noise.ID {
	return net.Kad.Discover()
}

// SendMessageToPeer sends a wire message to a peer fire and forget style.
func (net *Network) SendMessageToPeer(peer noise.ID, message wire.Message) error {

	messageBytes := message.Marshal()
	if messageBytes == nil {
		return errMarshalMessage
	}

	err := net.Host.Send(net.ctx, peer.Address, messageBytes)

	return err
}

// SendRequestToPeer sends a wire message to a peer, request style.
func (net *Network) SendRequestToPeer(peer noise.ID, message wire.Message) ([]byte, error) {

	messageBytes := message.Marshal()
	if messageBytes == nil {
		return nil, errMarshalMessage
	}

	resp, err := net.Host.Request(net.ctx, peer.Address, messageBytes)
	return resp, err
}

// RegisterHandler registers a new handler for this network instance.
func (net *Network) RegisterHandler(handler noise.Handler) {
	net.Host.Handle(handler)
}

// IsInbound checks if the peer is inbound
func (net *Network) IsInbound(peer noise.ID) bool {

	inbound := net.Host.Inbound()

	for _, cli := range inbound {
		iden := cli.ID().ID
		if iden.String() == peer.ID.String() {
			return true
		}
	}
	return false
}

// Shutdown network service by disconnecting active peers and closing listener.
func (net *Network) Shutdown() error {
	return net.Host.Close()
}

// DiscoveredPeers returns all kademlia discovered peers.
func (net *Network) DiscoveredPeers() []noise.ID {
	return net.Kad.Table().Peers()
}

// updateActivePeers updates current connected peers.
func (net *Network) updateActivePeers() {
	net.Inbound = net.Host.Inbound()
	net.Outbound = net.Host.Outbound()
}

// GetActivePeers returns a list of connected peers
func (net *Network) GetActivePeers() []*noise.Client {

	net.updateActivePeers()

	activePeers := make([]*noise.Client, 0, 8)

	for _, cli := range net.Inbound {
		activePeers = append(activePeers, cli)
	}
	for _, cli := range net.Outbound {
		activePeers = append(activePeers, cli)
	}

	return activePeers
}

// GetBestAdvertisedPeers returns a list of peers with the best advertised
// tip
func (net *Network) GetBestAdvertisedPeers(maxPeers int, ourStatus wire.StatusMessage) (crypto.Hash, *big.Int, []noise.ID) {
	return net.Peers.getBestAdvertisedPeers(maxPeers, ourStatus)
}
