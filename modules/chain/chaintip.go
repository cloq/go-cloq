package chain

import (
	"fmt"

	"gitlab.com/cloq/go-cloq/core/crypto"
)

// Tip wraps information about the blockchain tip.
type Tip struct {
	MostWorkHash   crypto.Hash
	MostWorkRoot   crypto.Hash
	MostWorkHeight uint64

	MedianTimestamp uint64

	TotalChainWork crypto.Hash
}

func (tip Tip) String() string {

	return fmt.Sprintf("Tip Hash : %s | Tip Root : %s | Tip Height : %d | Median Timestamp : %d | Tip ChainWork : %s",
		tip.MostWorkHash.String(), tip.MostWorkRoot.String(), tip.MostWorkHeight, tip.MedianTimestamp, tip.TotalChainWork.String())

}
