package chain

import (
	"encoding/hex"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/consensus/pow"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/internal/testgen"
	"gitlab.com/cloq/go-cloq/modules/storage"
	"gitlab.com/cloq/go-cloq/protocol"
)

func TestBlockchain(t *testing.T) {

	dir, err := ioutil.TempDir(os.TempDir(), "test_data")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	store, err := storage.NewStorage(dir, true)
	if err != nil {
		t.Fatal("failed to create temp storage")
	}
	blockchain, err := New(config.WhirlpoolRegnet(crypto.EmptyHash), store)
	if err != nil {
		t.Fatal("failed to create new blockchain")
	}
	blocks := testgen.GenBlocks()

	for _, blk := range blocks {
		insertBlock := func(blk *types.Block, chain *Blockchain) error {
			return chain.AppendBlock(blk, MAIN)
		}

		err := insertBlock(blk, blockchain)
		if err != nil {
			t.Fatal("failed to insert block with error :", err)
		}
	}

	for _, expected := range blocks {
		fetchBlock := func(blk *types.Block, chain *Blockchain) (*types.Block, error) {
			blkHash, _ := blk.Hash()
			return chain.GetBlockByHash(blkHash)

		}
		actual, err := fetchBlock(expected, blockchain)
		expectedHash, _ := expected.Hash()
		if err != nil {
			t.Fatalf("failed to fetch block %s with error :  %s", hex.EncodeToString(expectedHash.Bytes()), err)
		}
		assert.Equal(t, expected, actual)
	}
	t.Logf("Tip : %s", blockchain.GetChainTip().String())
}

func TestBlockchainFindAncestor(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), "test_data")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	store, err := storage.NewStorage(dir, true)
	if err != nil {
		t.Fatal("failed to create temp storage")
	}
	blockchain, err := New(config.WhirlpoolRegnet(crypto.EmptyHash), store)
	if err != nil {
		t.Fatal("failed to create new blockchain")
	}
	blocks := testgen.GenBlocks()

	for _, block := range blocks {
		blkHash, _ := block.Hash()
		t.Logf("(Main Chain) Block : Hash : %s | Parent :  %s | Height : %d", hex.EncodeToString(blkHash.Bytes()), hex.EncodeToString(block.Parent.Bytes()), block.Height)

		err := blockchain.AppendBlock(block, MAIN)
		if err != nil {
			t.Fatal("failed to append block to chain with error ", err)
		}
	}

	tip := blocks[len(blocks)-1]
	tipHash, _ := tip.Hash()
	t.Logf("Tip Hash :  %s, Tip Height : %d", hex.EncodeToString(tipHash.Bytes()), tip.Height)
	chain1, chain2 := testgen.GenBlocksWithFork(tip, 10)
	for i := len(chain1) - 1; i >= 0; i-- {
		ancestor1 := chain1[i].GetParent()
		ancestor2 := chain2[i].GetParent()

		if ancestor1.Equal(ancestor2) {
			t.Logf("found common ancestor %s", ancestor1.String())
		}
	}
	for _, block := range chain1 {
		blkHash, _ := block.Hash()
		t.Logf("(Chain 1) Block : Hash : %s | Parent :  %s | Height : %d", hex.EncodeToString(blkHash.Bytes()), hex.EncodeToString(block.Parent.Bytes()), block.Height)
		err := blockchain.AppendBlock(block, MAIN)
		if err != nil {
			t.Fatal("failed to append block to chain with error ", err)
		}
	}
	for _, block := range chain2 {
		blkHash, _ := block.Hash()
		t.Logf("(Chain 2) Block : Hash : %s | Parent :  %s | Height : %d", hex.EncodeToString(blkHash.Bytes()), hex.EncodeToString(block.Parent.Bytes()), block.Height)

		err := blockchain.AppendBlock(block, MAIN)
		if err != nil {
			t.Fatal("failed to append block to chain with error ", err)
		}
	}
	for _, block := range chain1 {
		hash, _ := block.Hash()
		blk, err := blockchain.GetBlockByHash(hash)
		if err != nil {
			t.Fatalf("failed to fetch block %s at %d with error :%v ", hash, block.Height, err)
		}
		assert.Equal(t, block, blk)
	}
	for _, block := range chain2 {
		hash, _ := block.Hash()
		blk, err := blockchain.GetBlockByHash(hash)
		if err != nil {
			t.Fatalf("failed to fetch block %s at %d with error :%v ", hash, block.Height, err)
		}
		assert.Equal(t, block, blk)
	}
	for _, block := range blocks {
		hash, _ := block.Hash()
		blk, err := blockchain.GetBlockByHash(hash)
		if err != nil {
			t.Fatalf("failed to fetch block %s at %d with error :%v ", hash, block.Height, err)
		}
		assert.Equal(t, block, blk)
	}
	commonAncestor, err := blockchain.findCommonAncestor(chain1[len(chain1)-1].Header, chain2[len(chain2)-1].Header)
	if err != nil {
		t.Fatal("failed to find common ancestor with error ", err)
	}

	assert.Equal(t, tip.Header, commonAncestor)
}

func TestBlockchainReorg(t *testing.T) {
	dir, err := ioutil.TempDir(os.TempDir(), "test_data")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	store, err := storage.NewStorage(dir, true)
	if err != nil {
		t.Fatal("failed to create temp storage")
	}
	blockchain, err := New(config.WhirlpoolRegnet(crypto.EmptyHash), store)
	if err != nil {
		t.Fatal("failed to create new blockchain")
	}
	blocks := testgen.GenBlocks()
	tip := blocks[len(blocks)-1]
	tipHash, _ := tip.Hash()
	t.Logf("Tip Hash :  %s, Tip Height : %d", hex.EncodeToString(tipHash.Bytes()), tip.Height)

	chain1, chain2 := testgen.GenBlocksWithFork(tip, 10)

	blocks = append(blocks, chain1[1:]...)

	for _, block := range blocks {
		blkHash, _ := block.Hash()
		t.Logf("(Main Chain) Block : Hash : %s | Parent :  %s | Height : %d", hex.EncodeToString(blkHash.Bytes()), hex.EncodeToString(block.Parent.Bytes()), block.Height)

		err := blockchain.AppendBlock(block, MAIN)
		if err != nil {
			t.Fatal("failed to append block to chain with error ", err)
		}
	}
	for _, block := range chain2 {
		blkHash, _ := block.Hash()

		t.Logf("(Side Chain) Block : Hash : %s | Parent :  %s | Height : %d", hex.EncodeToString(blkHash.Bytes()), hex.EncodeToString(block.Parent.Bytes()), block.Height)

		err := blockchain.AppendBlock(block, SIDE)
		if err != nil {
			t.Fatal("failed to append block to chain with error ", err)
		}
	}
	newTip := chain2[len(chain2)-1]
	currentTip := chain1[len(chain1)-1]

	blocksToDisconnect, blocksToConnect, err := blockchain.resolveReorg(currentTip.Header, newTip.Header)
	if err != nil {
		t.Fatal("failed to process chain re-org with error", err)
	}
	var length = len(chain1) - 1
	for idx, block := range blocksToDisconnect {
		blkHash, _ := chain1[length-idx].Hash()
		t.Logf("Chain[%d] : Hash %s", idx, blkHash)
		t.Logf("Block To Disconnect => Block Hash : %s ", block)
		assert.Equal(t, blkHash, block)
	}
	for idx, block := range blocksToConnect {
		blkHash, _ := chain2[length-idx].Hash()
		t.Logf("Chain[%d] : Hash %s", idx, blkHash)
		t.Logf("Block To Connect => Block Hash : %s ", block)
		assert.Equal(t, blkHash, block)
	}

}

func TestBlockchainMedianTime(t *testing.T) {

	store := createStorageTest(t)
	chain, err := New(config.WhirlpoolRegnet(crypto.EmptyHash), store)
	if err != nil {
		t.Fatal("failed to create chain with error ", err)
	}
	blocks := testgen.GenBlocksWithValidTime(28, 0)

	for _, block := range blocks {
		err := chain.AppendBlock(block, MAIN)
		if err != nil {
			t.Fatal("failed to put new block with error :", err)
		}
	}

	medianTime, err := chain.ComputeMedianTimeSpan(blocks[len(blocks)-1].GetHeight())
	if err != nil {
		t.Fatal("failed to compute mediantime with error :", err)
	}

	for _, block := range blocks {
		blkHash, _ := block.Hash()
		t.Logf("Block Height [%d] | Hash = %s | Timestamp : %d", block.GetHeight(), blkHash, block.GetTimestamp())
	}

	t.Log(medianTime)
}

func TestBlockchainState(t *testing.T) {

	store := createStorageTest(t)
	genesis, genesisHash, err := CreateGenesisBlock(
		[]byte("genesis-test-record"),
		time.Now().Unix(),
		"1clqunspendable",
		5*10e9,
		protocol.RegressionPoWLimit,
	)
	if err != nil {
		t.Fatal("failed to create new blockchain")
	}

	blockchain, err := New(config.WhirlpoolRegnet(genesisHash), store)
	if err != nil {
		t.Error("failed to create genesis block with error :", err)
	}
	t.Logf("New genesis bblock found : hash %s", genesisHash)

	err = blockchain.AppendBlock(genesis, MAIN)
	if err != nil {
		t.Error("failed to append genesis block with error :", err)
	}
	_, err = blockchain.GetHeaderByHash(genesisHash)
	if err != nil {
		t.Fatal("failed to fetch genesis with error ", err)
	}
	_, err = ProcessBlockTransition(genesis, blockchain.state, false)
	if err != nil {
		t.Error("failed to process genesis block with error :", err)
	}
	blockchain.currentTip = Tip{
		MostWorkHash:    genesisHash,
		MostWorkHeight:  genesis.GetHeight(),
		MostWorkRoot:    genesis.GetStateRoot(),
		TotalChainWork:  crypto.SetBig(genesis.GetChainWork().Big()),
		MedianTimestamp: genesis.GetTimestamp(),
	}

	parent := genesis.Header
	for i := genesis.GetHeight(); i < 2; i++ {
		coinbase := testgen.GenCoinbase(i)
		blk, err := blockchain.CreateNextBlock([]*types.Transaction{coinbase})
		if err != nil {
			t.Fatal("failed to create next block with err ", err)
		}
		t.Log("created next block", blk.String())
		root, err := ProcessBlockTransition(blk, blockchain.state, true)
		if err != nil {
			t.Fatal("failed to create next block with err ", err)
		}
		blk.Root = crypto.SetBytes(root)

		blk, err = pow.SolveBlock(blk)
		if err != nil {
			t.Fatal("failed to create next block with err ", err)
		}
		t.Log("Block Solved")

		newTip, ok, err := blockchain.ApplyBlock(blk, parent)
		if !ok || err != nil {
			t.Fatalf("iter[%d] failed to apply new block with error :%s", i, err)
		}
		t.Log("New Tip : ", newTip)
		parent = blk.Header.Clone()
	}

}

func TestBlockchainDiffAdjust(t *testing.T) {
	store := createStorageTest(t)
	chain, err := New(config.WhirlpoolRegnet(crypto.EmptyHash), store)
	if err != nil {
		t.Fatal("failed to create chain with error ", err)
	}
	blocks := testgen.GenBlocksWithValidTime(60, 0)

	for _, block := range blocks {
		err := chain.AppendBlock(block, MAIN)
		if err != nil {
			t.Fatal("failed to put new block with error :", err)
		}
	}
	height := blocks[len(blocks)-1].GetHeight()
	target := blocks[len(blocks)-1].GetTarget().Big()
	newTarget, err := chain.ComputeTargetThreshold(height)
	if err != nil {
		t.Fatal("failed to compute target threshold with error :", err)
	}
	t.Log("Starting Target :", target.Text(16))
	t.Log("New Target (Adjusted):", newTarget.Text(16))
	assert.True(t, target.Cmp(newTarget) == 1)
}

func TestBlockchainDiffAdjustWithSide(t *testing.T) {
	store := createStorageTest(t)
	chain, err := New(config.WhirlpoolRegnet(crypto.EmptyHash), store)
	if err != nil {
		t.Fatal("failed to create chain with error ", err)
	}
	blocks := testgen.GenBlocksWithValidTime(60, 0)
	for _, block := range blocks {
		err := chain.AppendBlock(block, MAIN)
		if err != nil {
			t.Fatal("failed to put new block with error :", err)
		}
	}

	sidebranch := testgen.GenBlocksWithValidTime(10, 60)

	for _, block := range sidebranch {
		err := chain.AppendBlock(block, SIDE)
		if err != nil {
			t.Fatal("failed to put new block with error :", err)
		}
	}

	height := blocks[len(blocks)-1].GetHeight()
	target := blocks[len(blocks)-1].GetTarget().Big()
	newTarget, err := chain.ComputeTargetThreshold(height)
	if err != nil {
		t.Fatal("failed to compute target threshold with error :", err)
	}
	t.Log("Starting Target :", target.Text(16))
	t.Log("New Target (Adjusted):", newTarget.Text(16))
	assert.True(t, target.Cmp(newTarget) == 1)
}

func createStorageTest(t *testing.T) *storage.Storage {
	dir, err := ioutil.TempDir(os.TempDir(), "test_data")
	if err != nil {
		t.Fatal("failed to create temp dir")
	}
	store, err := storage.NewStorage(dir, true)
	if err != nil {
		t.Fatal("failed to create temp storage")
	}

	return store
}
