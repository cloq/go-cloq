package chain

import (
	"math/big"
	"sort"

	"gitlab.com/cloq/go-cloq/modules/storage/ledger"
)

// Timestamps represents a list of block timestamps
type Timestamps []int64

func (ts Timestamps) Len() int           { return len(ts) }
func (ts Timestamps) Swap(i, j int)      { ts[i], ts[j] = ts[j], ts[i] }
func (ts Timestamps) Less(i, j int) bool { return ts[i] < ts[j] }

// Targets represents a list of block targets
type Targets []*big.Int

func (ts Targets) Len() int           { return len(ts) }
func (ts Targets) Swap(i, j int)      { ts[i], ts[j] = ts[j], ts[i] }
func (ts Targets) Less(i, j int) bool { return ts[i].Cmp(ts[j]) == -1 }

// CalcDifficulty
var (

	// Difficulty Adjustment Constants :

	// PoWMaxAdjustDown is the threshold on the adjustment factor for lower difficulty.
	PoWMaxAdjustDown int64 = 32
	// PoWMaxAdjustUp is the threshold on the adjustment factor for higher difficulty.
	PoWMaxAdjustUp int64 = 16
	// PoWDampingFactor is the damping factor.
	PoWDampingFactor int64 = 4

	// PoWTargetSpacing is the spacing between blocks we want to adjust for (34 seconds).
	PoWTargetSpacing int64 = 34
	// PoWAveragingWindow is the window for averaging the timespan computation.
	PoWAveragingWindow int64 = 17
	// PoWMedianBlockSpan the number of blocks to compute the median work.
	PoWMedianBlockSpan int64 = 11
	// AveragingWindowTimespan is the timespan we expect with 150 sec windows.
	AveragingWindowTimespan = PoWAveragingWindow * PoWTargetSpacing
	// MinActualTimespan actual timespan with the adjust factor down
	MinActualTimespan = (AveragingWindowTimespan) * (100 - PoWMaxAdjustUp) / 100
	// MaxActualTimespan actual timespan with adjust factor up
	MaxActualTimespan = (AveragingWindowTimespan) * (100 + PoWMaxAdjustDown) / 100
)

// medianTime computes the median timestamp of the last PoWMedianBlockSpan blocks.
func medianTime(height uint64, chain BlockchainView, branch ledger.BlockMarker) (int64, error) {

	var startingPoint int64

	if height <= uint64(PoWMedianBlockSpan) {
		startingPoint = 0
	} else {
		startingPoint = int64(height - uint64(PoWMedianBlockSpan))
	}

	var prevTimestamps = make([]int64, 0, PoWMedianBlockSpan)

	for i := startingPoint; i < int64(height); i++ {

		blkHeader, err := chain.GetHeaderByHeight(uint64(i), branch)
		if err != nil || blkHeader == nil {
			return 0, err
		}
		prevTimestamps = append(prevTimestamps, int64(blkHeader.GetTimestamp()))
	}

	median := func(arr Timestamps) int64 {
		sort.Stable(arr)
		length := len(arr)
		if length <= 1 {
			return arr[0]
		}
		var idx = length / 2
		if length%2 != 0 {
			return arr[idx]
		}
		mLeft := arr[idx-1]
		mRight := arr[idx]

		return (mLeft + mRight) / 2
	}

	medianTimestamp := median(prevTimestamps)

	return medianTimestamp, nil

}

// actualTimespan computes the actual timespan diff
func actualTimespan(height uint64, chain BlockchainView, branch ledger.BlockMarker) (int64, error) {

	medTime, err := medianTime(height, chain, branch)
	if err != nil {
		return 0, err
	}

	window := height - uint64(PoWAveragingWindow)

	medAvgTime, err := medianTime(window, chain, branch)
	if err != nil {
		return 0, err
	}

	return medTime - medAvgTime, nil
}

// actualTimespanDamped
func actualTimespanDamped(height uint64, chain BlockchainView, branch ledger.BlockMarker) (int64, error) {

	actTimespan, err := actualTimespan(height, chain, branch)
	if err != nil {
		return 0, err
	}

	return AveragingWindowTimespan + ((actTimespan - AveragingWindowTimespan) / PoWDampingFactor), nil
}

// actualTimespanBounded
func actualTimespanBounded(height uint64, chain BlockchainView, branch ledger.BlockMarker) (int64, error) {

	bound := func(upper, lower, x int64) int64 {

		var m int64

		if upper > x {
			m = x
		} else {
			m = upper
		}

		if lower > m {
			return lower
		}
		return m
	}

	dampedTimespan, err := actualTimespanDamped(height, chain, branch)
	if err != nil {
		return 0, err
	}

	bounded := bound(MaxActualTimespan, MinActualTimespan, dampedTimespan)

	return bounded, nil
}

// meanTarget
func meanTarget(height uint64, chain BlockchainView, powLimit *big.Int, branch ledger.BlockMarker) (*big.Int, error) {

	if height <= uint64(PoWAveragingWindow) {
		return powLimit, nil
	}
	var startingPoint = height - uint64(PoWAveragingWindow)

	var i uint64
	var sumTarget = big.NewInt(0)
	var count = big.NewInt(PoWAveragingWindow)

	for i = startingPoint; i < height; i++ {
		blkHeader, err := chain.GetHeaderByHeight(i, branch)
		if err != nil {
			return nil, err
		}

		sumTarget.Add(sumTarget, blkHeader.GetTarget().Big())

	}

	return new(big.Int).Div(sumTarget, count), nil

}

// targetThreshold is now computed a block is considered valid if its hash is less than threshold
func targetThreshold(height uint64, chain BlockchainView, branch ledger.BlockMarker) (*big.Int, error) {

	powLimit := new(big.Int).Set(chain.GetPoWLimit())
	if height <= uint64(PoWAveragingWindow) {
		return powLimit, nil
	}
	meantarget, err := meanTarget(height, chain, powLimit, branch)

	if err != nil {
		return nil, err
	}

	bigAvgWindow := new(big.Int).SetInt64(AveragingWindowTimespan)
	meanTargetAvg := new(big.Int).Div(meantarget, bigAvgWindow)

	timespanBound, err := actualTimespanBounded(height, chain, branch)
	if err != nil {
		return nil, err
	}

	bigTimespanBound := new(big.Int).SetInt64(timespanBound)

	targetThres := new(big.Int).Mul(meanTargetAvg, bigTimespanBound)
	if powLimit.Cmp(targetThres) == -1 {
		return powLimit, nil
	}

	return targetThres, nil
}
