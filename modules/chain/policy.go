package chain

import "gitlab.com/cloq/go-cloq/core/types"

// IsBetter method handles chain forks in the case of proof of work
// it chooses block with most work, if it fails split at random.
// It is used to select a header when both have the same height
// and extends the same parent.
func IsBetter(tip *types.Header, challenger *types.Header) bool {

	if tip.GetChainWork().Big().Cmp(challenger.GetChainWork().Big()) > 0 {
		return true
	} else if tip.GetChainWork().Big().Cmp(challenger.GetChainWork().Big()) < 0 {
		return false
	}

	// break ties based on header hash
	tipHash, err := tip.Hash()
	if err != nil {
		panic(err)
	}
	challengerHash, err := tip.Hash()
	if err != nil {
		panic(err)
	}

	return tipHash.Big().Cmp(challengerHash.Big()) < 0
}
