package chain

import (
	"encoding/hex"
	"fmt"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/protocol"
)

// locateParent of a block by it's hash
func (chain *Blockchain) locateParent(blkHash crypto.Hash) (crypto.Hash, error) {

	header, err := chain.GetBlockByHash(blkHash)
	if err != nil {
		fmt.Println("Error : couldn't find block :", hex.EncodeToString(blkHash.Bytes()))
		return crypto.EmptyHash, err
	}
	return header.Parent, nil
}

// findCommonAncestor finds the common ancestors of two competing blocks
func (chain *Blockchain) findCommonAncestor(b1, b2 *types.Header) (*types.Header, error) {

	ancestorB1 := b1.GetParent()
	ancestorB2 := b2.GetParent()

	var err error
	// maxIter marks when the ancestry is too deep
	var maxIter uint64
	for !ancestorB1.Equal(ancestorB2) {

		if maxIter >= protocol.MaxSoftReorg {
			return nil, ErrMaxChainReorg
		}
		ancestorB1, err = chain.locateParent(ancestorB1)
		if err != nil {
			return nil, err
		}
		ancestorB2, err = chain.locateParent(ancestorB2)
		if err != nil {
			return nil, err
		}
		if ancestorB1.Equal(ancestorB2) {
			fmt.Println("Found valid ancestor hash :", hex.EncodeToString(ancestorB1.Bytes()))
			return chain.GetHeaderByHash(ancestorB1)
		}
		maxIter++
	}

	return nil, ErrUnknownParent
}

// locateHistory walks the chain from start to end hash
// and returns all blocks in between in reverse order (end...start).
func (chain *Blockchain) locateHistory(start, end crypto.Hash) ([]crypto.Hash, error) {

	hashes := make([]crypto.Hash, 0, 16)

	for iter := end; iter != start; {

		header, err := chain.GetHeaderByHash(iter)
		if err != nil {
			return nil, err
		}
		headerHash, _ := header.Hash()
		hashes = append(hashes, headerHash)
		iter = header.GetParent()

	}
	return hashes, nil
}
