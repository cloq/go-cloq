package chain

import (
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"

	"github.com/pkg/errors"
	"gitlab.com/cloq/go-cloq/consensus/config"
	"gitlab.com/cloq/go-cloq/core/addr"
	"gitlab.com/cloq/go-cloq/modules/storage/state"
	"gitlab.com/cloq/go-cloq/pkg/safemath"
	"gitlab.com/cloq/go-cloq/protocol"
)

// VerifyTransaction : processes and validates a transaction then puts it into pool
// it also runs to verify unknown transactions in a block, since mempool transactions
// are considered valid we only verify transaction that we don't have.
// The checks are based on the consensus rules in the documentations.
func VerifyTransaction(newTx *types.Transaction, tipHeight uint64, state state.StateView, chainConfig config.ChainConfig) (bool, error) {

	// syntactic correctness
	ok, err := newTx.Validate(false)
	if !ok || err != nil {
		return false, errors.Wrap(err, "invalid transaction")
	}
	// check that we are on the same network and branch
	if chainConfig.GetBranchID() != newTx.GetBranchID() {
		return false, errors.New("transaction is on the wrong branchID")
	}
	if chainConfig.ConsensusVersion != newTx.GetVersion() {
		return false, errors.New("invalid transaction version")
	}

	inputs, err := newTx.Inputs()
	if err != nil {
		return false, err
	}
	var prevHeights = make([]uint64, 0, len(inputs))

	outputs, err := newTx.Outputs()
	if err != nil {
		return false, err
	}
	// are all out prevouts claimable ?
	inputHashes, err := newTx.ExecutedContracts()
	if err != nil {
		return false, err
	}
	executedContracts, err := state.GetBatch(inputHashes)

	// maybe provide a fraud proof ?
	if err != nil {
		return false, errors.Wrap(err, "transaction is spending non-existant outpoints :")
	}
	// since the GetBatch function returns nil slices we always run this check
	if len(executedContracts) != len(inputs) {
		return false, err
	}

	var totalValueIn uint64

	for idx, execContract := range executedContracts {

		outpoint := execContract.GetContract()

		ok, err := isContractFinal(outpoint.Locktime(), tipHeight)

		if !ok || err != nil {
			return ok, err
		}

		ok, err = isValidAddress(outpoint.Address(), chainConfig.NetworkID(), inputs[idx].Validator)
		if !ok || err != nil {
			return ok, err
		}

		totalValueIn, err = safemath.AddUint64(totalValueIn, outpoint.Value())
		if err != nil {
			return false, err
		}

		prevHeights = append(prevHeights, execContract.Height())

	}

	totalValueOut, err := getTotalValueOut(outputs)
	if err != nil {
		return false, err
	}

	ok, err = isValidMoneyRange(totalValueIn, totalValueOut, newTx.GetFee())

	if !ok || err != nil {
		return ok, err
	}
	// QVM will concurrently execute every single contract, it holds copies
	// to the data we use instead of pointers to have less friction in routine
	qvm, err := InitQVM(newTx, prevHeights, tipHeight, 10000)
	if err != nil {
		return false, err
	}
	successful, err := qvm.Run(nil)
	return successful, err
}

// VerifyCoinbase : process and verify coinbase transaction
func VerifyCoinbase(coinbaseTx *types.Transaction, blkFees, blkHeight uint64) (bool, error) {

	ok, err := coinbaseTx.Validate(true)
	if !ok || err != nil {
		return ok, err
	}
	expectedBlockSubsidy := protocol.GetBlockIssuance(blkHeight)

	createdOutputs, err := coinbaseTx.Outputs()
	if err != nil {
		return false, err
	}

	currentBlockSubsidy, err := getTotalValueOut(createdOutputs)
	if err != nil {
		return false, err
	}

	if expectedBlockSubsidy+blkFees != currentBlockSubsidy {
		return false, errors.New("block inflating supply")

	}
	return true, nil
}
func isContractFinal(locktime, currentHeight uint64) (bool, error) {
	if locktime < currentHeight {
		return false, errors.New("outpoint being executed still timelocked")
	}
	return true, nil
}

func isValidAddress(address string, netid string, script []byte) (bool, error) {
	contractAddr, err := addr.Encode(script, netid)
	if err != nil || contractAddr != address {
		return false, err
	}

	return true, nil
}

func isValidMoneyRange(totalValueIn, totalValueOut, fee uint64) (bool, error) {
	if totalValueIn >= protocol.MaxUnits || totalValueOut >= protocol.MaxUnits {
		return false, errors.New("transaction is inflating coins")
	}
	if totalValueIn < totalValueOut {
		return false, errors.New("total amount executed can't be higher than out")
	}
	// do we check that coins aren't going to waste ?
	// totalValueIn - totalValueOut = fee upper bound
	// thus the transaction fee must be greater or equal than 0
	// and lesser than or equal to fee upper bound
	if fee != (totalValueIn - totalValueOut) {
		return false, errors.New("fee higher than current available coins")
	}

	return true, nil
}
func getTotalValueOut(outputs []types.Output) (uint64, error) {
	var totalValueOut uint64
	var err error
	for _, out := range outputs {
		totalValueOut, err = safemath.AddUint64(totalValueOut, out.Value)
		if err != nil {
			return 0, err
		}
	}
	return totalValueOut, err
}

// IsCoinbase checks if the transaction is a coinbase transaction according
// to consensus rules
func IsCoinbase(tx *types.Transaction) bool {
	if len(tx.Commitment.Inputs) != 1 || len(tx.Commitment.Outputs) < 1 {
		return false
	}
	if tx.Commitment.Inputs[0].Origin != crypto.EmptyHash {
		return false
	}

	if len(tx.Commitment.Inputs[0].Validator) < protocol.MinCoinbaseValidator || len(tx.Commitment.Inputs[0].Validator) > protocol.MaxCoinbaseValidator {
		return false
	}
	return true
}
