package storage

import (
	"os"
	"path"

	"gitlab.com/cloq/go-cloq/modules/storage/ledger"
	"gitlab.com/cloq/go-cloq/modules/storage/state"
	"gitlab.com/cloq/go-cloq/pkg/db/leveldb"
	"gitlab.com/cloq/go-cloq/pkg/db/memdb"
)

const (
	statedbFilePath  = ".state"
	ledgerdbFilePath = " chain.db"
	triedbFilePath   = ".triedb"
)

// Storage wraps a the underlying state and blockchain databases.
type Storage struct {
	storagePath string
	StateDB     *state.Snapshot
	LedgerDB    *ledger.LedgerDB
}

// NewStorage initiate a new storage unit for persistent data.
func NewStorage(dirname string, mem bool) (*Storage, error) {
	if mem {
		stateSet := memdb.New(512)
		stateTrie := memdb.New(512)
		stateStorage := state.NewSnapshot(nil, stateSet, stateTrie)
		chainStorage, err := ledger.NewLedgerDB(path.Join(dirname, ledgerdbFilePath))
		if err != nil {
			return nil, err
		}
		return &Storage{
			storagePath: dirname,
			StateDB:     stateStorage,
			LedgerDB:    chainStorage,
		}, nil

	}
	statedbPath := path.Join(dirname, statedbFilePath, "db")
	triedbPath := path.Join(dirname, triedbFilePath, "db")
	ledgerdbPath := path.Join(dirname, ledgerdbFilePath)
	if _, err := os.Stat(statedbPath); os.IsNotExist(err) {
		_ = os.MkdirAll(statedbPath, 0711)
	}
	if _, err := os.Stat(triedbPath); os.IsNotExist(err) {
		_ = os.MkdirAll(triedbPath, 0711)
	}

	stateSet, err := leveldb.NewLevelDB(statedbPath)
	if err != nil {
		return nil, err
	}
	stateTrie, err := leveldb.NewLevelDB(triedbPath)
	if err != nil {
		return nil, err
	}
	stateStorage := state.NewSnapshot(nil, stateSet, stateTrie)
	chainStorage, err := ledger.NewLedgerDB(ledgerdbPath)
	if err != nil {
		return nil, err
	}
	return &Storage{
		storagePath: dirname,
		StateDB:     stateStorage,
		LedgerDB:    chainStorage,
	}, nil
}

// Close the underlying storage
func (storage *Storage) Close() error {

	err := storage.LedgerDB.Close()
	if err != nil {
		return err
	}
	return storage.StateDB.Close()
}
