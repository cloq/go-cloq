package state

import (
	"gitlab.com/cloq/go-cloq/core/types"
)

// OpenContractSet represents an authenticated dictionnary of open-contracts.
// An implementation should be capable of providing  :
// An authenticated Root
// Put/Get/Delete operations.
// Audit Proofs of membership and non-membership.
// Reverting to a previous root.
type OpenContractSet interface {
	Root() []byte
	Put([]*types.OpenContract) error
	Get([]byte) (*types.OpenContract, error)
	GetBatch([][]byte) ([]*types.OpenContract, error)
	IsOpen([]byte) bool
	Delete([]*types.OpenContract) error
	GetMerkleProof([]byte) ([][]byte, bool, []byte, []byte, error)
	VerifyInclusionProof([][]byte, []byte, []byte) bool
	VerifyNonInclusionProof([][]byte, []byte, []byte, []byte) bool
	Rollback([]byte) error
	LoadLastSavedState([]byte) error
}

// StateView is a read only interface for accessing state data
type StateView interface {
	Get([]byte) (*types.OpenContract, error)
	IsOpen([]byte) bool
	GetBatch([][]byte) ([]*types.OpenContract, error)
	VerifyInclusionProof([][]byte, []byte, []byte) bool
	VerifyNonInclusionProof([][]byte, []byte, []byte, []byte) bool
	GetMerkleProof([]byte) ([][]byte, bool, []byte, []byte, error)
	GetMerkleProofCompact(key []byte) ([]byte, [][]byte, int, bool, []byte, []byte, error)
	GetMerkleProofPast([]byte, []byte) ([][]byte, bool, []byte, []byte, error)
	GetMerkleProofCompactPast([]byte, []byte) ([]byte, [][]byte, int, bool, []byte, []byte, error)
}
