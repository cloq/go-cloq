package state

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"os"
	"path"
	"testing"

	"gitlab.com/cloq/go-cloq/core/types"

	"gitlab.com/cloq/go-cloq/pkg/db/memdb"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cloq/go-cloq/pkg/db/leveldb"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/pkg/bech32"
)

func genTxdata(size int) []*types.OpenContract {

	var txids [][32]byte
	indexes := make([]uint32, 0)
	values := make([]uint64, 0)
	addresses := make([]string, 0)
	var h crypto.Hash
	for i := 0; i < size; i++ {
		key := make([]byte, 32)
		_, err := rand.Read(key)
		if err != nil {
			panic(err)
		}
		h = crypto.SHA3Hash(key)
		txids = append(txids, h)
		indexes = append(indexes, uint32(i*3))
		r := crypto.Blake256(txids[i][:])
		addr, _ := bech32.EncodeCloqAddress("cq", r[:])
		addresses = append(addresses, addr)
		values = append(values, uint64(i*100000/25))

	}
	ocss := make([]*types.OpenContract, 0)

	for i, txid := range txids {
		out := types.Output{Address: addresses[i], Value: values[i], Locktime: uint64(i)}
		ocs := types.NewOpenContract(types.NewContract(txid, indexes[i], out), uint64(size%375), false)
		ocss = append(ocss, ocs)
	}
	fmt.Println(len(ocss))
	return ocss

}

// TestSnapshotWork
func TestSnapshotWork(t *testing.T) {

	t.Run("TestSnapshotPutGetDelete", func(t *testing.T) {
		statedbPath := path.Join(".ocs", "db")
		triedbPath := path.Join(".trie", "db")

		if _, err := os.Stat(statedbPath); os.IsNotExist(err) {
			_ = os.MkdirAll(statedbPath, 0711)

		}

		if _, err := os.Stat(triedbPath); os.IsNotExist(err) {
			_ = os.MkdirAll(triedbPath, 0711)

		}
		stateDB, err := leveldb.NewLevelDB(statedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}
		triedb, err := leveldb.NewLevelDB(triedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}

		defer os.RemoveAll(".ocs")
		defer os.RemoveAll(".trie")

		state := NewSnapshot(nil, stateDB, triedb)
		defer state.Close()

		randOCS := genTxdata(200)
		err = state.Put(randOCS)
		if err != nil {
			t.Error("state Put failed with error : ", err)
		}
		t.Log("post state update root : ", hex.EncodeToString(state.Root()))

		for _, ocs := range randOCS[:100] {
			oh, _ := ocs.ContractHash()
			newOC, err := state.Get(oh.Bytes())
			if err != nil {
				t.Error("state Get failed with error : ", err)
			}

			assert.Equal(t, ocs, newOC)
		}

		err = state.Delete(randOCS[:100])
		if err != nil {
			t.Error("state Delete failed with error : ", err)
		}
		t.Log("post state delete root : ", hex.EncodeToString(state.Root()))
	})
	t.Run("TestSnapshotProofs", func(t *testing.T) {
		statedbPath := path.Join(".ocs", "db")
		triedbPath := path.Join(".trie", "db")

		if _, err := os.Stat(statedbPath); os.IsNotExist(err) {
			_ = os.MkdirAll(statedbPath, 0711)

		}

		if _, err := os.Stat(triedbPath); os.IsNotExist(err) {
			_ = os.MkdirAll(triedbPath, 0711)

		}
		stateDB, err := leveldb.NewLevelDB(statedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}
		triedb, err := leveldb.NewLevelDB(triedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}

		defer os.RemoveAll(".ocs")
		defer os.RemoveAll(".trie")

		state := NewSnapshot(nil, stateDB, triedb)
		defer state.Close()
		randOCS := genTxdata(200)
		err = state.Put(randOCS)
		if err != nil {
			t.Error("state Put failed with error : ", err)
		}
		for _, ocs := range randOCS {
			oh, _ := ocs.ContractHash()
			val, _ := ocs.Hash()

			proof, status, _, _, err := state.GetMerkleProof(oh[:])
			if err != nil {
				t.Error("failed to get inclusion proof with error :", err)
			}
			if !status {
				t.Error("status should be unspent")
			}

			if !state.VerifyInclusionProof(proof, oh[:], val[:]) {
				t.Error("failed to verify supposedly correct inclusion proof")
			}
		}
		err = state.Delete(randOCS[:100])
		if err != nil {
			t.Error("state Delete failed with error : ", err)
		}
		for _, ocs := range randOCS[:100] {
			oh, _ := ocs.ContractHash()

			proof, status, proofKey, proofVal, err := state.GetMerkleProof(oh[:])
			if err != nil {
				t.Error("failed to get inclusion proof with error : ", err)
			}
			if status {
				t.Error("failed to delete ocs")
			}

			if !state.VerifyNonInclusionProof(proof, oh[:], proofVal, proofKey) {
				t.Error("non inclusion proofs should verify after delete op")
			}
		}
	})
	t.Run("TestSnapshotConsistency", func(t *testing.T) {
		statedbPath := path.Join(".ocs", "db")
		triedbPath := path.Join(".trie", "db")

		if _, err := os.Stat(statedbPath); os.IsNotExist(err) {
			_ = os.MkdirAll(statedbPath, 0711)

		}

		if _, err := os.Stat(triedbPath); os.IsNotExist(err) {
			_ = os.MkdirAll(triedbPath, 0711)

		}
		stateDB, err := leveldb.NewLevelDB(statedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}
		triedb, err := leveldb.NewLevelDB(triedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}

		defer os.RemoveAll(".ocs")
		defer os.RemoveAll(".trie")

		state := NewSnapshot(nil, stateDB, triedb)
		defer state.Close()
		randOCS := genTxdata(200)
		err = state.Put(randOCS)
		if err != nil {
			t.Error("state Put failed with error : ", err)
		}
		expected := state.Root()
		t.Log("post state update root : ", hex.EncodeToString(expected))

		err = state.Delete(randOCS[:100])
		if err != nil {
			t.Error("state Delete failed with error : ", err)
		}
		stateRootBeforeClose := state.Root()
		t.Log("post state delete root : ", hex.EncodeToString(state.Root()))
		t.Log("closing state and reloading from disk...")

		state.Close()
		stateDB, err = leveldb.NewLevelDB(statedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}
		triedb, err = leveldb.NewLevelDB(triedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}

		state = NewSnapshot(nil, stateDB, triedb)

		err = state.LoadLastSavedState(stateRootBeforeClose)
		if err != nil {
			t.Error("state Load failed with error : ", err)
		}
		t.Log("post state reopen root : ", hex.EncodeToString(state.Root()))
		assert.Equal(t, stateRootBeforeClose, state.Root())
		err = state.Put(randOCS[:100])
		if err != nil {
			t.Error("state Put failed with error : ", err)
		}
		actual := state.Root()

		if !bytes.Equal(expected, actual) {
			t.Error("trie root is inconsistent : expected ", hex.EncodeToString(expected), " got", hex.EncodeToString(actual))
		}
	})
	t.Run("TestSnapshotBackend", func(t *testing.T) {
		statedbPath := path.Join(".ocs", "db")
		triedbPath := path.Join(".trie", "db")

		if _, err := os.Stat(statedbPath); os.IsNotExist(err) {
			_ = os.MkdirAll(statedbPath, 0711)

		}

		if _, err := os.Stat(triedbPath); os.IsNotExist(err) {
			_ = os.MkdirAll(triedbPath, 0711)

		}
		stateDB, err := leveldb.NewLevelDB(statedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}
		triedb, err := leveldb.NewLevelDB(triedbPath)
		if err != nil {
			t.Error("leveldb failed with error : ", err)
		}

		defer os.RemoveAll(".ocs")
		defer os.RemoveAll(".trie")

		state := NewSnapshot(nil, stateDB, triedb)
		defer state.Close()

		stateCache := memdb.New(200)
		trieCache := memdb.New(200)

		randOCS := genTxdata(200)

		stateInMem := NewSnapshot(nil, stateCache, trieCache)

		err = state.Put(randOCS)
		if err != nil {
			t.Error("state Put failed with error : ", err)
		}
		err = stateInMem.Put(randOCS)
		if err != nil {
			t.Error("state Put failed with error : ", err)
		}

		if !bytes.Equal(state.Root(), stateInMem.Root()) {
			t.Fatal("failed to assert disk/memory state consistency")
		}
		err = state.Delete(randOCS[:100])
		if err != nil {
			t.Error("state Delete failed with error : ", err)
		}
		err = stateInMem.Delete(randOCS[:100])
		if err != nil {
			t.Error("state Delete failed with error : ", err)
		}

		if !bytes.Equal(state.Root(), stateInMem.Root()) {
			t.Fatal("failed to assert disk/memory state consistency")
		}
	})
	t.Run("TestSnapshotView", func(t *testing.T) {
		getProof := func(key []byte, stateview StateView) ([]byte, [][]byte, int, bool, []byte, []byte, error) {
			return stateview.GetMerkleProofCompact(key)
		}
		state := NewCachedSnapshot(nil)
		randOCS := genTxdata(200)
		err := state.Put(randOCS)
		if err != nil {
			t.Fatal("failed to put contracts with error :", err)
		}
		key, _ := randOCS[2].ContractHash()

		view := NewStateView(state)

		bitmap, ap, index, included, proofKey, proofVal, err := getProof(key.Bytes(), view)
		if err != nil {
			t.Fatal("failed to generate proof with error :", err)
		}
		t.Log("AuditPath  Size:", len(ap))
		t.Log("Bitmap : ", hex.EncodeToString(bitmap))
		t.Log("Key : ", hex.EncodeToString(key.Bytes()))
		t.Log("ProofKey : ", hex.EncodeToString(proofKey))
		t.Log("Index", index)
		t.Log("Included :", included)
		t.Log("Valid Proof", state.VerifyCompactInclusionProof(bitmap, index, ap, key.Bytes(), proofVal))
	})
}
