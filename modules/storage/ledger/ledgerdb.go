package ledger

import (
	"errors"
	"math/big"

	"gitlab.com/cloq/go-cloq/core/types"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/pkg/leb128"

	bolt "go.etcd.io/bbolt"
)

// BlockMarker represents the current status of a given block in the cache.
type BlockMarker uint8

const (
	// UNKNOWN means the block status is not known.
	UNKNOWN BlockMarker = iota
	// MAIN means the block is currently on the main branch.
	MAIN
	// SIDE means the block is currently extending a different branch.
	SIDE
	// ORPHAN means the block is orphaned no block extends it.
	ORPHAN
)

const (
	// tipHashKey is used as a key to the last serialized tip hash
	tipHashKey = "tiphash"
	// tipHeightKey is used as a key to the last serialized tip height
	tipHeightKey = "tipheight"
	// tipRootKey is used as a key to the last serialized tip root
	tipRootKey = "tiproot"
	// tipWorKey is used as a key to the last serialized tip total work
	tipWorkKey = "tiptd"

	// mainBranchMarker is used to mark blocks on the main branch
	mainBranchMarker = "mainBranch"
	// sideBranchMarker is used to mark blocks on a side branch
	sideBranchMarker = "sideBranch"
	// orphanBranchMarker is used to mark orphaned blocks
	orphanBranchMarker = "orphanBranch"
	// invalidBlockMarker is used to mark invalid blocks
	invalidBlockMarker = "invalidBlock"
)

// One boltdb holds a few buckets :
// Headers Bucket : (HdrHash,RawHeader)
// Blocks Bucket : (HdrHash,[]Txids)
// Txs Bucket : (Txid,RawTx)
// indexer : (Height,Hash)
// oprhans : (HdrHash,SerializedBlock)
var (
	hdrBucket      = "headers"
	blkBucket      = "blocks"
	unclesBucket   = "uncles"
	txBucket       = "txs"
	indexerBucket  = "blkIndex"
	branchBucket   = "branchIndex"
	orphanBucket   = "orphans"
	sideBucket     = "sides"
	chainTipBucket = "chainTip"
	buckets        = []string{
		hdrBucket,
		blkBucket,
		unclesBucket,
		txBucket,
		indexerBucket,
		branchBucket,
		orphanBucket,
		sideBucket,
		chainTipBucket,
	}
)
var (
	// ErrMaxChainReorg is returned if a reorg trigger the maximum client limit
	ErrMaxChainReorg = errors.New("fork is longer than soft reorg limit")
	// ErrUnknownParent is returned if a parent is unknown to us
	ErrUnknownParent = errors.New("unknown parent for block")
	// ErrBlockNotFound is returned if we don't have a particular block
	ErrBlockNotFound = errors.New("block not found")
	// ErrHeaderNotFound is returned if we don't have a particular header
	ErrHeaderNotFound = errors.New("header not found")
)

// Ledger is a storage interface for blocks
type Ledger interface {
	GetHeaderByHash([]byte) (*types.Header, error)

	GetTXByHash([]byte) (*types.Transaction, error)

	GetBlockByHash([]byte) (*types.Block, error)

	GetHeaderByHeight(uint64) (*types.Header, error)

	GetBlockByHeight(uint64) (*types.Block, error)

	PutNewHeader(*types.Header, BlockMarker) error

	PutNewBlock(*types.Block, BlockMarker) error
	PutNewOrphan(*types.Block) error

	UpdateTip(crypto.Hash, uint64, crypto.Hash) error
	GetTipHash() (crypto.Hash, error)
	GetTipHeight() (uint64, error)
	GetTipRoot() (crypto.Hash, error)
	IsOrphan(crypto.Hash) (bool, error)

	PutNewTransaction(*types.Transaction) error
}

// LedgerDB is an ondisk backend store
type LedgerDB struct {
	path   string   // path to the underlying file
	store  *bolt.DB // underlying handle to the boltdb file
	isOpen bool     // is our bolt instance open ?
}

// Close a ledger
func (bdb *LedgerDB) Close() error {
	if bdb.isOpen {
		return bdb.store.Close()
	}

	return errors.New("blockdb is already closed")
}

// NewLedgerDB creates a new boltdb with two buckets
func NewLedgerDB(fp string) (*LedgerDB, error) {

	blkdb := new(LedgerDB)

	blkdb.path = fp

	database, err := bolt.Open(fp, 0600, nil)
	if err != nil {
		return nil, err
	}

	for _, bucket := range buckets {
		err = database.Update(func(tx *bolt.Tx) error {
			_, err := tx.CreateBucketIfNotExists([]byte(bucket))
			if err != nil {
				return err
			}
			return nil
		})
		if err != nil {
			return nil, err
		}
	}

	blkdb.store = database

	return blkdb, nil
}

// GetHeaderByHash returns the unserialized header by hash
func (bdb *LedgerDB) GetHeaderByHash(h []byte) (*types.Header, error) {

	var hdr = types.NewEmptyHeader()

	fetchErr := bdb.store.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(hdrBucket))
		buf := b.Get(h)
		if buf == nil {
			return ErrHeaderNotFound
		}
		err := hdr.Unmarshal(buf)
		if err != nil {
			return err
		}
		return nil
	})

	return hdr, fetchErr

}

// GetBlockByHash returns an unserialized block by its hash
func (bdb *LedgerDB) GetBlockByHash(h []byte) (*types.Block, error) {

	var blk = types.NewEmptyBlock()

	fetchErr := bdb.store.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blkBucket))

		buf := b.Get(h)
		if buf == nil {
			return ErrBlockNotFound
		}
		err := blk.Unmarshal(buf)
		if err != nil {
			return err
		}
		b = tx.Bucket([]byte(hdrBucket))
		buf = b.Get(h)
		if buf == nil {
			return ErrBlockNotFound
		}
		err = blk.Header.Unmarshal(buf)
		return err
	})

	return blk, fetchErr
}

// GetBlockBytesByHash returns an unserialized block by its hash
func (bdb *LedgerDB) GetBlockBytesByHash(h []byte) ([]byte, error) {

	var buf []byte
	fetchErr := bdb.store.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(blkBucket))
		buf = b.Get(h)
		if buf == nil {
			return ErrBlockNotFound
		}
		return nil
	})

	return buf, fetchErr
}

// GetTXByHash retuns an unserialized transaction by its hash
func (bdb *LedgerDB) GetTXByHash(h []byte) (*types.Transaction, error) {

	var transaction = new(types.Transaction)
	fetchErr := bdb.store.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(txBucket))
		buf := b.Get(h)
		if buf == nil {
			return errors.New("transaction not found")
		}

		return transaction.Unmarshal(buf)
	})

	return transaction, fetchErr
}

// GetHeaderByHeight returns an unserialized header by its hash
func (bdb *LedgerDB) GetHeaderByHeight(height uint64, marker BlockMarker) (*types.Header, error) {

	var heightBytes = leb128.FromUint64(height)
	var key []byte
	switch marker {
	case MAIN:
		key = append([]byte(mainBranchMarker), heightBytes...)
	case SIDE:
		key = append([]byte(sideBranchMarker), heightBytes...)
	case ORPHAN:
		key = append([]byte(orphanBranchMarker), heightBytes...)
	}
	var headerHash crypto.Hash
	// fetch headerhash from indexer
	fetchErr := bdb.store.View(func(tx *bolt.Tx) error {
		indexer := tx.Bucket([]byte(indexerBucket))
		h := indexer.Get(key)
		if h == nil {
			return ErrHeaderNotFound
		}
		headerHash.SetBytes(h)
		return nil
	})

	if fetchErr != nil {
		return nil, fetchErr
	}

	return bdb.GetHeaderByHash(headerHash.Bytes())
}

// GetBlockByHeight returns an unserialized block by it's height
func (bdb *LedgerDB) GetBlockByHeight(height uint64, marker BlockMarker) (*types.Block, error) {

	var heightBytes = leb128.FromUint64(height)
	var headerHash crypto.Hash
	var key []byte

	switch marker {
	case MAIN:
		key = append([]byte(mainBranchMarker), heightBytes...)
	case SIDE:
		key = append([]byte(sideBranchMarker), heightBytes...)
	case ORPHAN:
		key = append([]byte(orphanBranchMarker), heightBytes...)
	}
	// fetch headerhash from indexer
	fetchErr := bdb.store.View(func(tx *bolt.Tx) error {
		indexer := tx.Bucket([]byte(indexerBucket))
		h := indexer.Get(key)
		if h == nil {
			return errors.New("blockheight not found in indexer")
		}
		headerHash.SetBytes(h)
		return nil
	})
	if fetchErr != nil {
		return nil, fetchErr
	}

	return bdb.GetBlockByHash(headerHash.Bytes())
}

// PutNewTransaction stores a new transaction
func (bdb *LedgerDB) PutNewTransaction(transaction *types.Transaction) error {

	txid, err := transaction.Hash()
	if err != nil {
		return err
	}

	buf, err := transaction.Marshal()

	if err != nil {
		return err
	}

	putErr := bdb.store.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(txBucket))

		return b.Put(txid.Bytes(), buf)
	})

	return putErr

}

// PutNewHeader stores a new block header
func (bdb *LedgerDB) PutNewHeader(h *types.Header, marker BlockMarker) error {

	hdrHash, err := h.Hash()
	if err != nil {
		return err
	}
	hdrHeight := h.GetHeight()

	buf, err := h.Marshal()

	if err != nil {
		return err
	}

	putErr := bdb.store.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(hdrBucket))

		err := b.Put(hdrHash.Bytes(), buf)
		if err != nil {
			return err
		}
		return nil
	})

	if putErr != nil {
		return putErr
	}
	err = bdb.IndexBlockByHeight(hdrHash.Bytes(), hdrHeight, marker)
	if err != nil {
		return err
	}
	return bdb.MarkBranch(hdrHash.Bytes(), marker)
}

// PutNewBlock stores a new block header
func (bdb *LedgerDB) PutNewBlock(blk *types.Block, marker BlockMarker) error {

	blkHash, err := blk.Hash()
	if err != nil {
		return err
	}
	buf, err := blk.Marshal()

	if err != nil {
		return err
	}

	putErr := bdb.store.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blkBucket))

		err := b.Put(blkHash.Bytes(), buf)
		if err != nil {
			return err
		}
		return nil
	})

	if putErr != nil {
		return putErr
	}
	return bdb.PutNewHeader(blk.Header, marker)
}

// UpdateTip updates the current tip
func (bdb *LedgerDB) UpdateTip(tipHash crypto.Hash, tipHeight uint64, tipRoot crypto.Hash, tipWork crypto.Hash) error {

	tipHeightVal := leb128.FromUint64(tipHeight)

	putErr := bdb.store.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(chainTipBucket))

		err := b.Put([]byte(tipHashKey), tipHash.Bytes())
		if err != nil {
			return err
		}
		err = b.Put([]byte(tipHeightKey), tipHeightVal)
		if err != nil {
			return err
		}
		err = b.Put([]byte(tipWorkKey), tipWork.Bytes())
		if err != nil {
			return err
		}
		err = b.Put([]byte(tipRootKey), tipRoot.Bytes())
		return err
	})
	return putErr
}

// GetTipHash reads the last stored tip
func (bdb *LedgerDB) GetTipHash() (crypto.Hash, error) {

	newTip := []byte(tipHashKey)
	var h crypto.Hash
	putErr := bdb.store.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(chainTipBucket))

		val := b.Get(newTip)
		if val == nil {
			return errors.New("tip not found")
		}
		h.SetBytes(val)
		return nil
	})
	return h, putErr
}

// GetTipChainWork reads the last stored tip
func (bdb *LedgerDB) GetTipChainWork() (*big.Int, error) {

	newTip := []byte(tipWorkKey)
	var h = new(big.Int)
	putErr := bdb.store.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(chainTipBucket))

		val := b.Get(newTip)
		if val == nil {
			return errors.New("tip not found")
		}
		h.SetBytes(val)
		return nil
	})
	return h, putErr
}

// GetTipRoot reads the last stored tip
func (bdb *LedgerDB) GetTipRoot() (crypto.Hash, error) {

	newTip := []byte(tipRootKey)
	var h crypto.Hash
	putErr := bdb.store.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(chainTipBucket))

		val := b.Get(newTip)
		if val == nil {
			return errors.New("tip not found")
		}
		h.SetBytes(val)
		return nil
	})
	return h, putErr
}

// GetTipHeight reads the last stored tip
func (bdb *LedgerDB) GetTipHeight() (uint64, error) {

	newTip := []byte(tipHeightKey)

	var height uint64
	var err error

	putErr := bdb.store.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(chainTipBucket))

		val := b.Get(newTip)
		if val == nil {
			return errors.New("tip not found")
		}
		height, err = leb128.ToUint64(val)
		return err
	})

	return height, putErr
}

// GetBlocksInRange returns a list of blocks in range Height start,end (inclusive)
// from the canonical chain we alawys return main branch blocks.
func (bdb *LedgerDB) GetBlocksInRange(start, end uint64, marker BlockMarker) ([]*types.Block, error) {

	blocks := make([]*types.Block, 0, end-start)

	for iter := start; iter <= end; iter++ {

		blk, err := bdb.GetBlockByHeight(iter, marker)
		if err != nil {
			return nil, err
		}
		blocks = append(blocks, blk)

	}
	return blocks, nil
}

// GetHeadersInRange returns a list of headers in range Height start,end (inclusive)
// from the canonical chain.
func (bdb *LedgerDB) GetHeadersInRange(start, end uint64, marker BlockMarker) ([]*types.Header, error) {

	headers := make([]*types.Header, 0, end-start)
	for iter := start; iter <= end; iter++ {

		hdr, err := bdb.GetHeaderByHeight(iter, marker)
		if err != nil {
			return headers, err
		}
		headers = append(headers, hdr)

	}
	return headers, nil
}

// GetBranch returns the branch on which this block is set
func (bdb *LedgerDB) GetBranch(blkHash []byte) BlockMarker {

	var branch string
	putErr := bdb.store.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(branchBucket))

		val := b.Get(blkHash)
		if val == nil {
			return errors.New("value not found for key")
		}
		branch = string(val)
		return nil
	})

	if putErr != nil {
		return UNKNOWN
	}
	switch branch {
	case mainBranchMarker:
		return MAIN
	case sideBranchMarker:
		return SIDE
	case orphanBranchMarker:
		return ORPHAN
	default:
		return UNKNOWN
	}
}

// MarkBranch marks a block as part of a branch
func (bdb *LedgerDB) MarkBranch(blkHash []byte, marker BlockMarker) error {

	var val []byte

	switch marker {
	case MAIN:
		val = []byte(mainBranchMarker)
	case SIDE:
		val = []byte(sideBranchMarker)
	case ORPHAN:
		val = []byte(orphanBranchMarker)
	default:
		val = []byte(orphanBranchMarker)
	}

	putErr := bdb.store.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(branchBucket))

		err := b.Put(blkHash, val)
		if err != nil {
			return err
		}
		return nil
	})

	return putErr
}

// IndexBlockHeight stores a block into the height-indexer
func (bdb *LedgerDB) IndexBlockByHeight(blkHash []byte, blkHeight uint64, marker BlockMarker) error {

	var heightBytes = leb128.FromUint64(blkHeight)

	var heightIndex []byte

	switch marker {
	case MAIN:
		heightIndex = append([]byte(mainBranchMarker), heightBytes...)
	case SIDE:
		heightIndex = append([]byte(sideBranchMarker), heightBytes...)
	case ORPHAN:
		heightIndex = append([]byte(orphanBranchMarker), heightBytes...)
	default:
		heightIndex = append([]byte(invalidBlockMarker), heightBytes...)
	}

	putErr := bdb.store.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(indexerBucket))

		err := b.Put(heightIndex, blkHash)
		if err != nil {
			return errors.New("value not found for key")
		}
		return nil
	})

	return putErr
}
