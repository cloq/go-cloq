package service

import (
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"

	"sync"
)

// Result represents the result of processing data.
type Result struct {
	Ok  bool
	Err error
}

// Status describe the current status of a service
type Status uint8

const (
	// UNKNOWN is an iota protective guard
	UNKNOWN = iota
	// STARTED service has started and is running
	STARTED
	// ACTIVE service is active
	ACTIVE
	// STOPPED service ran and stopped
	STOPPED
)

// Service interface
type Service interface {
	Start() error
	Stop() error
	Status()
}

// Message interface
type Message interface {
	Topic() string
}

// The following are the messages the services can send to each other
// to signal new data or requests.

var (
	topicNewTX       = "newTx"
	topicNewBlock    = "newBlock"
	topicBlockBatch  = "blockBatch"
	topicHeaderBatch = "headerBatch"

	_ Message = (*IncomingBlock)(nil)
	_ Message = (*IncomingTx)(nil)
	_ Message = (*HeaderBatch)(nil)
	_ Message = (*BlockBatch)(nil)
)

// BlockBatch represent a batch of block bodies and their hashes (to be filled)
type BlockBatch struct {
	Hashes      [][]byte
	BlockBodies []*types.Block
	ResultChan  chan Result
}

// HeaderBatch represent a batch of block headers
type HeaderBatch struct {
	Headers    []*types.Header
	ResultChan chan Result
}

// Topic implements the message interface.
func (bb *BlockBatch) Topic() string {
	return topicBlockBatch
}

// Topic implements the message interface.
func (hb *HeaderBatch) Topic() string {
	return topicHeaderBatch
}

// IncomingTx wraps transactions received on the network that will be sent
// to the Processor for validation.
type IncomingTx struct {
	TXID       []byte
	TX         types.Transaction
	Processed  bool
	ResultChan chan<- Result // channel to receive the result
}

// NewIncomingTx creates a new incomingTx instance
func NewIncomingTx(tx *types.Transaction, processed bool, resChan chan<- Result) IncomingTx {
	id, _ := tx.TXID()
	return IncomingTx{
		TXID:       id[:],
		TX:         *tx,
		Processed:  processed,
		ResultChan: resChan,
	}
}

// Topic implements internal message interface
func (incTx *IncomingTx) Topic() string {
	return topicNewTX
}

// IncomingBlock wraps a block received either on the network or from miner.
type IncomingBlock struct {
	// Did we mine this block
	Mined      bool
	Hash       crypto.Hash
	Block      *types.Block
	Processed  bool
	ResultChan chan<- error
}

// Topic implements internal message interface
func (incBlk *IncomingBlock) Topic() string {
	return topicNewBlock
}

// IsProcessed if we've already processed the tx
func (incBlk *IncomingBlock) IsProcessed() bool {
	return incBlk.Processed
}

// PubSub implements an internal PubSub system for the different node components
type PubSub struct {
	mu     sync.RWMutex
	subs   map[string][]chan [32]byte
	closed bool
}

// NewPubSub creates a new instance of pubsub
func NewPubSub() *PubSub {
	ps := &PubSub{}
	ps.subs = make(map[string][]chan [32]byte)
	return ps
}

// Subscribe to a given topic
func (ps *PubSub) Subscribe(topic string, ch chan [32]byte) {
	ps.mu.Lock()
	defer ps.mu.Unlock()

	ps.subs[topic] = append(ps.subs[topic], ch)
}

// Publish a new msg on a topic channel
func (ps *PubSub) Publish(topic string, msg [32]byte) {

	ps.mu.RLock()
	defer ps.mu.RUnlock()
	if ps.closed {
		return
	}

	for _, ch := range ps.subs[topic] {
		go func(ch chan [32]byte) {
			ch <- msg
		}(ch)

	}
}

// Close the PubSub instance and channels
func (ps *PubSub) Close() {
	ps.mu.Lock()
	defer ps.mu.Unlock()
	if !ps.closed {
		ps.closed = true
		for _, subs := range ps.subs {
			for _, ch := range subs {
				close(ch)
			}
		}
	}
}
