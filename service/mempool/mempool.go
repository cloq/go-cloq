package mempool

import (
	"encoding/hex"
	"errors"
	"sync"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/service"
	"go.uber.org/zap"
)

var (
	// ErrCoinbaseTxNotAllowed coinbase tx must be built by miner only
	ErrCoinbaseTxNotAllowed = errors.New("coinbase tx not allowed in mempool")
	// ErrAlreadyExists transaction with same txid already exists in
	ErrAlreadyExists = errors.New("already exists")
	// ErrDoubleSpending transaction uses contracts in other mempool txs
	ErrDoubleSpending = errors.New("double-spending in mempool")
	// ErrNotFound when a transaction isn't present in mempool
	ErrNotFound = errors.New("tx not present in mempool")
)

const (

	// Don't queue more than 500 transaction
	maxPending = 500
)

// Mempool is a cache that lives as long as the application lives.
// Mempool is optional but recommended, mempool processes mainly incomingTxs
// and requests (RPC and AC).
type Mempool struct {
	mu              sync.RWMutex
	cache           *TransactionPool
	logger          *zap.Logger
	count           uint64
	lastBlockHeight uint64 // used for tracking expired transactions
}

// Store sends a transaction to the main mempool go routine
// through an internal channel.
func (p *Mempool) Store(newTx service.IncomingTx) error {
	p.mu.Lock()
	defer p.mu.Unlock()

	if !p.cache.Exists(newTx.TXID) {
		err := p.cache.Put(newTx)
		if err != nil {
			p.logger.Warn("transaction : failed to add to mempool", zap.String("TXID", hex.EncodeToString(newTx.TXID)), zap.Error(err))
			return err
		}
		p.logger.Info("transaction : added to mempool", zap.String("TXID", hex.EncodeToString(newTx.TXID)))
		return nil
	} else {
		p.logger.Info("transaction : added to mempool", zap.String("TXID", hex.EncodeToString(newTx.TXID)))
	}
	return ErrAlreadyExists
}

// NewMempool creates a new mempool instance
func NewMempool(height uint64) *Mempool {

	return &Mempool{
		cache:           NewTransactionPool(),
		count:           0,
		lastBlockHeight: height,
	}
}

// GetTransaction fetches a single transaction from the mempool.
func (p *Mempool) GetTransaction(txid crypto.Hash) (types.Transaction, error) {
	p.mu.RLock()
	defer p.mu.RUnlock()
	if p.cache.Exists(txid.Bytes()) {
		respTx, err := p.cache.Get(txid.Bytes())
		if err != nil {
			p.logger.Warn("Transaction : is not present in mempool %v", zap.String("TXID", hex.EncodeToString(respTx.TXID)), zap.Error(err))
			return types.Transaction{}, err
		}
		return respTx.TX, nil
	}
	return types.Transaction{}, ErrNotFound

}
