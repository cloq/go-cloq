package mempool

import (
	"errors"
	"sort"

	"gitlab.com/cloq/go-cloq/pkg/bytesutil"

	"gitlab.com/cloq/go-cloq/service"
)

var (
	// don't accept transactions bigger than this size
	maxTxSize = 2000000
	// defaultCap
	defaultCap     = 1024
	sortitionCache = 100
)

// TransactionPool represents a pool implementation based on golang map. The generic
// solution to bench against.
type TransactionPool struct {
	// transactions pool
	data map[[32]byte]service.IncomingTx

	// sorted is data keys sorted by Fee in a descending order
	// sorting happens at point of accepting new entry in order to allow
	// Block Generator to fetch highest-fee txs without delays in sorting
	sorted []keyFee

	spentInputs map[[32]byte]bool
	Capacity    uint32
	txsSize     uint32
}

// NewTransactionPool launches an instance of txpool
func NewTransactionPool() *TransactionPool {
	return &TransactionPool{
		data:        make(map[[32]byte]service.IncomingTx, defaultCap),
		sorted:      make([]keyFee, 0, sortitionCache),
		spentInputs: make(map[[32]byte]bool, defaultCap),
	}
}

// Put sets the value for the given key. It overwrites any previous value
// for that key;
func (m *TransactionPool) Put(t service.IncomingTx) error {

	// store tx
	txID, err := t.TX.TXID()
	if err != nil {
		return err
	}

	m.data[txID] = t

	txSize, err := t.TX.Size()
	if txSize > maxTxSize {
		return errors.New("tx size higher than consensus allowed")
	}
	m.txsSize += uint32(txSize)

	// sort keys by Fee
	// Bulk sort like (sort.Slice) performs a few times slower than
	// a simple binarysearch&shift algorithm.
	fee := t.TX.GetFee()

	index := sort.Search(len(m.sorted), func(i int) bool {
		return m.sorted[i].v < fee
	})

	m.sorted = append(m.sorted, keyFee{})
	copy(m.sorted[index+1:], m.sorted[index:])
	m.sorted[index] = keyFee{k: string(txID[:]), v: fee}

	executedContracts, err := t.TX.ExecutedContracts()

	for _, ec := range executedContracts {
		id := bytesutil.ToBytes32(ec)
		_, ok := m.spentInputs[id]
		if ok {
			return ErrDoubleSpending
		}
		m.spentInputs[id] = true
	}

	return nil
}

// Get returns a single tx.
func (m *TransactionPool) Get(txID []byte) (service.IncomingTx, error) {

	v, ok := m.data[bytesutil.ToBytes32(txID)]
	if !ok {
		return service.IncomingTx{}, ErrNotFound
	}

	return v, nil
}

// Exists returns true if the given key is in the pool.
func (m *TransactionPool) Exists(txID []byte) bool {

	_, ok := m.data[bytesutil.ToBytes32(txID)]
	return ok
}

// Size of the txs
func (m *TransactionPool) Size() uint32 {
	return m.txsSize
}

// Len returns the number of tx entries
func (m *TransactionPool) Len() int {
	return len(m.data)
}
