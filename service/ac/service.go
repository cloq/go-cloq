package ac

import (
	"context"
	"encoding/hex"
	"sync"
	"time"

	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/protocol"
	"go.uber.org/zap"

	hashset "github.com/deckarep/golang-set"
	"github.com/pkg/errors"
	"gitlab.com/cloq/go-cloq/service"

	"gitlab.com/cloq/go-cloq/modules/chain"
	"gitlab.com/cloq/go-cloq/service/mempool"
	"gitlab.com/cloq/go-cloq/service/miner"

	"gitlab.com/cloq/go-cloq/consensus/config"

	"gitlab.com/cloq/go-cloq/core/addr"
	"gitlab.com/cloq/go-cloq/core/crypto"

	"gitlab.com/cloq/go-cloq/pkg/merkle"
)

var (
	// EmptyRootHash specifies the merkle root of an empty list
	EmptyRootHash = merkle.Root([][]byte{})
)

// VMResult represents an execution result
type VMResult struct {
	success bool
	err     error
	name    string
}

// Service sits in the middle of the blockchain and new incoming blocks and
// transactions.
// All validation and verification is handled by this unit.
// Including forks, double spends, consensus rules..
// Once a block has been deemed sufficiently confirmed, and no forks
// are currently active it gets commited to the chain, when a fork is happening
// we wait until it is resolved to commit the updates to state and sync back to the network.
// the transition is applied to state.
// The fork policy is based on the achieved target, and total difficulty.
// case newBlock :
// Do non-contextual checks (timestamps + proof of work + proper parent ...)
// Do contextual checks (inputs exist at root(parent) + proper tx validation)
// TryAcceptBlock -> process if extending tip or side (if side see if we overtake)
type Service struct {
	Logger      *zap.Logger
	chainConfig config.ChainConfig // network parameters
	miningConf  config.MinerConfig // specify a list of addresses to receive mining rewards

	// Processor active chain
	blockchain      *chain.Blockchain // a queue of blocks to verify and process
	processedBlocks hashset.Set
	blockCandidates map[crypto.Hash]*types.Block

	// new blocks and transactions
	IncomingTransactions chan service.IncomingTx
	IncomingBlock        chan service.IncomingBlock

	// headers and blocks to process when syncing
	IncomingHeaders chan *service.HeaderBatch
	IncomingBlocks  chan *service.BlockBatch

	// mempool
	mempool *mempool.Mempool
	// mined blocks
	MinedBlocks  chan *types.Block
	wg           sync.WaitGroup
	shutdownChan chan struct{}
}

// NewService creates a new instance of the Processor worker.
func NewService(chainConf config.ChainConfig, blockchain *chain.Blockchain, miningConf config.MinerConfig) *Service {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}

	return &Service{
		Logger:               logger,
		chainConfig:          chainConf,
		miningConf:           miningConf,
		blockchain:           blockchain,
		processedBlocks:      hashset.NewSet(),
		blockCandidates:      make(map[crypto.Hash]*types.Block, 32),
		IncomingBlock:        make(chan service.IncomingBlock, 32),
		IncomingTransactions: make(chan service.IncomingTx, 100),
		IncomingHeaders:      make(chan *service.HeaderBatch, 500),
		IncomingBlocks:       make(chan *service.BlockBatch, 32),
		MinedBlocks:          make(chan *types.Block, 32),
		mempool:              mempool.NewMempool(blockchain.GetChainTip().MostWorkHeight),
		shutdownChan:         make(chan struct{}),
	}
}

// QueueNewBlock is an external call that enqueues a new block for processing.
func (s *Service) QueueNewBlock(newBlock service.IncomingBlock) {
	s.IncomingBlock <- newBlock
}

// QueueNewTransaction is an external call that enqueues a new transaction for processing.
func (s *Service) QueueNewTransaction(newTx service.IncomingTx) {
	s.IncomingTransactions <- newTx
}

// QueueHeaderBatch is an external call that enqueues a batch of block headers for processing.
func (s *Service) QueueHeadersBatch(headers *service.HeaderBatch) {
	s.IncomingHeaders <- headers
}

// QueueBlockBatch is an external call that enqueues a batch of blocks for processing.
func (s *Service) QueueBlockBatch(blocks *service.BlockBatch) {
	s.IncomingBlocks <- blocks
}

// Start processor as go routine
func (s *Service) Start(ctx context.Context) error {
	s.wg.Add(1)
	go s.start()

	return nil
}

// start the processor service main loop
func (s *Service) start() {

	s.Logger.Info("Service started")
	// Load tip
	defer s.wg.Done()

	genCoinbase := func(address string) *types.Transaction {
		v := []byte("CLEAR EYES FULL HEARTS <3 #CLOQ #STATELESS #2.0")
		nullInput := []types.Input{{Origin: crypto.EmptyHash, Validator: v}}
		output := []types.Output{{Address: address, Value: protocol.BaseReward, Locktime: protocol.CoinbaseMaturity}}
		return types.NewTransaction(protocol.WhirlpoolConsensusVer, protocol.WhirpoolBranchID, 0, 0, types.NewCommitment(nullInput, output), types.Witness{})
	}
	minerService := miner.NewMiner(1, s.MinedBlocks)
	if s.miningConf.Active {

		s.Logger.Info("Mining started")

		s.wg.Add(1)
		go minerService.Run()
	}

	// send the peer pings
	tickerPing := time.NewTicker(10 * time.Second)
	defer tickerPing.Stop()
	for {

		select {

		case _, ok := <-s.shutdownChan:
			if !ok {
				s.Logger.Info("Service shutting down")
				return
			}
		case <-tickerPing.C:
			if s.miningConf.Active {
				address, _ := addr.EncodeHash(crypto.SHA3Hash([]byte("test-mining-address-1")), s.chainConfig.NetworkID())
				cb := genCoinbase(address)
				blk, err := s.CreateNextBlock([]*types.Transaction{cb})
				if err != nil {
					s.Logger.Warn("failed to create valid block with error", zap.Error(err))
				}
				s.Logger.Info("Sending block to minerService")
				minerService.Solve(blk)
			}

		case newBlock := <-s.IncomingBlock:
			err := s.ProcessBlock(newBlock.Block)
			if err != nil {
				s.Logger.Warn("failed to process new block with error :", zap.Error(err))
				// if we don't have the block we should maybe ask for it
				newBlock.ResultChan <- err
			}
			newBlock.ResultChan <- nil
			s.Logger.Info("accepted new block :", zap.String("blockHash", newBlock.Hash.String()))

		case newTx := <-s.IncomingTransactions:
			err := s.ProcessTransaction(newTx)
			if err != nil {
				newTx.ResultChan <- service.Result{Ok: false, Err: err}
				s.Logger.Warn("failed to process new transaction with error :", zap.Error(err))
				return
			}
			newTx.ResultChan <- service.Result{Ok: true, Err: err}
			s.Logger.Info("accepted new transaction :", zap.String("TXID", hex.EncodeToString(newTx.TXID)))

		case newBlkBatch := <-s.IncomingBlocks:
			err := s.ProcessBodies(newBlkBatch.Hashes, newBlkBatch.BlockBodies)
			if err != nil {
				s.Logger.Warn("failed to process block batch with error :", zap.Error(err))
			}
		case newHdrBatch := <-s.IncomingHeaders:
			err := s.ProcessHeaders(newHdrBatch.Headers)
			if err != nil {
				newHdrBatch.ResultChan <- service.Result{Ok: false, Err: err}
				s.Logger.Warn("failed to process header batch with error :", zap.Error(err))
				return
			}
			newHdrBatch.ResultChan <- service.Result{Ok: true, Err: err}
		case minedBlock := <-s.MinedBlocks:
			blkHash, _ := minedBlock.Hash()
			s.Logger.Info("received block from minerService")
			err := s.ProcessBlock(minedBlock)
			if err != nil {
				s.Logger.Warn("failed to process mined block with error", zap.Error(err))
			}
			s.Logger.Info("accepted new mined block :", zap.String("blockHash", blkHash.String()))
		}

	}
}

// Shutdown stops the processor synchronously.
func (s *Service) Shutdown() {
	close(s.shutdownChan)
	s.wg.Wait()
	s.Logger.Info("Service shutdown")
}

// ProcessTransaction processes a new transaction
func (s *Service) ProcessTransaction(transaction service.IncomingTx) error {
	chainTip := s.blockchain.GetChainTip()
	currentStateRoot := chainTip.MostWorkRoot
	currentHeight := chainTip.MostWorkHeight

	stateView := s.blockchain.GetStateView(currentStateRoot.Bytes())

	ok, err := chain.VerifyTransaction(&transaction.TX, currentHeight, stateView, s.chainConfig)
	if !ok || err != nil {
		return err
	}
	// if the transaction is valid send it to mempool
	s.Logger.Info("Successfully processed transaction, sending to mempool", zap.String("service", "admission control"), zap.String("txid", hex.EncodeToString(transaction.TXID)))

	return s.mempool.Store(transaction)
}

// ProcessBlock process a new block.
// We start by doing non-contextual checks on the header
// by validating ancestry, proof of work and timestamps.
// At this point invalid blocks are rejected, valid blocks
// are then contextually validated against the current state view.
func (s *Service) ProcessBlock(blk *types.Block) error {

	currentTip := s.blockchain.GetChainTip()

	blkHash, err := blk.Hash()
	if err != nil {
		s.Logger.Error("failed to process new block with error :", zap.String("service", "admission control"), zap.Error(err))
		return err
	}
	ok, err := blk.Validate()
	if !ok || err != nil {
		s.Logger.Error("failed to process new block with error :", zap.String("service", "admission control"), zap.Error(err))
		return err
	}

	if s.processedBlocks.Contains(blkHash) {
		s.Logger.Info("block to process is already in cache", zap.String("service", "admission control"), zap.String("blockHash", blkHash.String()))
		return nil
	}

	ok, err = chain.VerifyHeaderWithoutContext(blk.Clone(), currentTip.TotalChainWork.Big(), &s.chainConfig, currentTip.MedianTimestamp)
	if !ok || err != nil {
		s.Logger.Error("failed to process new block with error :", zap.String("service", "admission control"), zap.Error(err))
		return err
	}
	// If the Header is valid we check it's body
	ok, err = chain.VerifyBlockWithoutContext(blk)
	if !ok || err != nil {
		s.Logger.Error("failed to process new block with error :", zap.String("service", "admission control"), zap.Error(err))
		return err
	}

	// Add this point the block is considered valid we add it to the candidate index
	s.blockCandidates[blkHash] = blk
	ok = s.processedBlocks.Add(blkHash)
	if !ok {
		s.Logger.Info("failed to add block to cache", zap.String("service", "admission control"), zap.String("blockHash", blkHash.String()))
	}
	// In order to validate the block's parent we see if it's in blockchain
	parent, err := s.blockchain.GetHeaderByHash(blk.GetParent())
	if err != nil {
		s.Logger.Warn("failed to find parent for block in blockchain...trying candidates pool", zap.String("service", "admission control"), zap.Error(err), zap.String("blockHash", blkHash.String()), zap.String("parent", blk.GetParent().String()))
		candidate, ok := s.blockCandidates[blk.GetParent()]
		if !ok {
			s.Logger.Warn("failed to find parent for block in candidate pool", zap.String("service", "admission control"), zap.Error(err), zap.String("blockHash", blkHash.String()), zap.String("parent", blk.GetParent().String()))
			s.Logger.Info("marking block has orphan")
			// TODO mark error as missing parent then send a Request blocks
			// message to the sender
			err := s.blockchain.AppendBlock(blk, chain.ORPHAN)
			return err
		}
		// TODO: Process Block Orphans
		// all orphan blocks that depend on this block
		// 	processOrphans(s.blockCandidates, candidate)
		parent = candidate.Header.Clone()
	}
	// At this point either we returned because the parent isn't in our possession
	// Or we found a valid one in the candidate pool.
	if parent.GetHeight()+1 != blk.GetHeight() {
		s.Logger.Error("new block has wrong relative height", zap.String("service", "admission control"), zap.Uint64("blockHeight", blk.GetHeight()), zap.Uint64("parent", parent.GetHeight()))
		return errors.Errorf("expected block height %d instead got %d marked as orphan %s", parent.GetHeight()+1, blk.GetHeight(), err)
	}
	// We start by validating the block header
	ok, err = chain.VerifyHeader(blk.Header, parent, s.blockchain, false)
	if !ok || err != nil {
		return errors.Wrap(err, "invalid header")
	}
	s.Logger.Info("[+] Valid Header")

	// We then verify the block's transactions
	tipHeight := s.blockchain.GetChainTip().MostWorkHeight
	stateView := s.blockchain.GetStateView(s.blockchain.GetChainTip().MostWorkRoot.Bytes())
	ok, err = chain.VerifyBlockTransactions(blk, tipHeight, stateView, s.chainConfig)
	if !ok || err != nil {
		s.Logger.Warn("failed to validate block transactions")
		return err
	}
	s.Logger.Info("[+] Valid Transactions")

	s.Logger.Info("[+] Valid Block Attempting to Apply State Transition...")

	newTip, ok, err := s.blockchain.ApplyBlock(blk, parent)

	if !ok && err != nil {
		s.Logger.Error("failed to append new block to blockchain", zap.String("service", "admission control"), zap.String("blockhash", blkHash.String()), zap.Error(err))
		return err
	}
	s.Logger.Info("New Tip :", zap.String("Tip Hash", newTip.MostWorkHash.String()), zap.String("Tip Root", newTip.MostWorkRoot.String()))
	return nil
}

// ProcessHeaders is called when processing headers in batch during the sync
func (s *Service) ProcessHeaders(headers []*types.Header) error {

	// Headers will come in reverse so we start from the end
	lastIdx := len(headers) - 1

	for iter := lastIdx; iter >= 0; iter-- {

		header := headers[iter]
		parent := header.GetParent()
		ancestor, err := s.blockchain.GetHeaderByHash(parent)
		if err != nil {
			return err
		}
		ok, err := chain.VerifyHeader(header, ancestor, s.blockchain, false)
		if !ok || err != nil {
			return err
		}
		err = s.blockchain.AppendHeader(header, chain.MAIN)
		if err != nil {
			return err
		}
		err = s.blockchain.UpdateTip(header)
		if err != nil {
			return err
		}
	}

	return nil
}

// ProcessBodies is called when processing block bodies in batch during the sync
func (s *Service) ProcessBodies(blockHashes [][]byte, blockBodies []*types.Block) error {

	for idx, blockBody := range blockBodies {

		blockHeader, err := s.blockchain.GetHeaderByHash(crypto.SetBytes(blockHashes[idx]))
		if err != nil {
			return err
		}
		parent, err := s.blockchain.GetHeaderByHash(blockHeader.Parent)
		if err != nil {
			return err
		}
		// load the header
		blockBody.Header = blockHeader
		// verify blockBody transactions
		tipHeight := blockHeader.GetHeight()
		tipRoot := parent.GetStateRoot()
		stateView := s.blockchain.GetStateView(tipRoot.Bytes())

		ok, err := chain.VerifyBlockTransactions(blockBody, tipHeight, stateView, s.chainConfig)
		if !ok || err != nil {
			return err
		}
		_, ok, err = s.blockchain.ApplyBlock(blockBody, parent)
		if !ok || err != nil {
			return err
		}
	}

	return nil

}

// ProcessBlockBatch is called when processing block bodies in batch during the sync
func (s *Service) ProcessBlockBatch(blockBatch []*types.Block) error {

	for _, blk := range blockBatch {

		header := blk.Header

		parent, err := s.blockchain.GetHeaderByHash(blk.GetParent())
		if err != nil {
			return err
		}
		ok, err := chain.VerifyHeader(header, parent, s.blockchain, false)
		if !ok || err != nil {
			return err
		}

		stateView := s.blockchain.GetStateView(parent.GetStateRoot().Bytes())

		ok, err = chain.VerifyBlockTransactions(blk, parent.GetHeight(), stateView, s.chainConfig)
		if !ok || err != nil {
			return err
		}

		_, ok, err = s.blockchain.ApplyBlock(blk, parent)
		if !ok || err != nil {
			return err
		}
	}

	return nil

}

// CreateNextBlock calls chain.createNextBlock and builds a valid block
// with transactions from the current service mempool.
func (s *Service) CreateNextBlock(txs []*types.Transaction) (*types.Block, error) {
	return s.blockchain.CreateNextBlock(txs)
}
