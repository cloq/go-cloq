# Service

The service package implements the three main components of a node.

The packages:

| package | description |
|-|-|
| `ac` | admission control for blocks, transactions and state management. |
| `storage` | block and state persistent on disk storage.|
| `manager` | manage P2P and RPC . |
| `miner` | Works on block proof of work |

Each package can be ran separately, the node architecture is modular and follows
idiomatic CSP (data is communicated strictly trough channels).
