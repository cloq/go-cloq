package centrifuge

import (
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"go.uber.org/zap"
)

// refreshPeerStatus runs a status update with outbound peers
func (n *Node) refreshPeerStatus() error {

	currentStatus := n.GetNodeStatus()
	peers := n.Network.Outbound

	for _, peer := range peers {

		pid := peer.ID()

		resp, err := n.Network.SendRequestToPeer(pid, currentStatus)
		if err != nil {
			n.Logger.Warn("failed to send request to peer with error ", zap.Error(err))
			continue
		}
		message, err := wire.UnmarshalWireMessage(resp)
		if err != nil {
			n.Logger.Warn("failed to unmarshal peer response with error ", zap.Error(err))
			continue
		}
		peerStatus, ok := message.(*wire.StatusMessage)
		if !ok {
			n.Logger.Warn("failed to assert peer response with error ", zap.Error(err))
			continue

		}
		n.Network.Peers.UpdatePeer(pid, *peerStatus)

	}
	return nil
}
