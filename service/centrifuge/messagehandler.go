package centrifuge

import (
	"errors"
	"sync"

	"github.com/perlin-network/noise"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
)

// MessageProcessor implements the MessageHandler interface.
type MessageProcessor struct {
	mu       sync.RWMutex
	handlers map[uint32]func(noise.ID, wire.Message, *noise.HandlerContext) error
}

// NewMessageProcessor creates a new instance of the message processor.
func NewMessageProcessor() *MessageProcessor {

	return &MessageProcessor{
		handlers: make(map[uint32]func(noise.ID, wire.Message, *noise.HandlerContext) error, 10),
	}
}

// Register a message handler for a given topic.
func (mp *MessageProcessor) Register(topic uint32, fun func(noise.ID, wire.Message, *noise.HandlerContext) error) error {
	mp.mu.Lock()
	defer mp.mu.Unlock()

	mp.handlers[topic] = fun
	return nil
}

// Handle a message by calling the respective handler.
func (mp *MessageProcessor) Handle(peer noise.ID, message wire.Message) error {

	mp.mu.Lock()
	defer mp.mu.Unlock()
	topic := message.Topic()

	handler, ok := mp.handlers[topic]
	if !ok || handler == nil {
		return errors.New("non-existing handler for message topic " + wire.GetTopic(topic))
	}
	return handler(peer, message, nil)
}

// HandleRequest handles request messages.
func (mp *MessageProcessor) HandleRequest(peer noise.ID, message wire.Message, ctx *noise.HandlerContext) error {
	mp.mu.Lock()
	defer mp.mu.Unlock()
	topic := message.Topic()

	handler, ok := mp.handlers[topic]
	if !ok || handler == nil {
		return errors.New("non-existing handler for message topic " + wire.GetTopic(topic))
	}
	return handler(peer, message, ctx)
}
