package centrifuge

import (
	"bytes"
	"encoding/hex"
	"errors"

	"github.com/perlin-network/noise"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/modules/chain"
	"gitlab.com/cloq/go-cloq/modules/network/wire"
	"gitlab.com/cloq/go-cloq/modules/storage/ledger"
	"gitlab.com/cloq/go-cloq/pkg/bytesutil"
	"gitlab.com/cloq/go-cloq/service"
	"go.uber.org/zap"
)

// Processing errors are errors reported due to an invalide

// OnMessagePing is called whenever a peer receives a ping message
func (n *Node) OnMessagePing(sender noise.ID, pingMessage *wire.PingMessage, ctx *noise.HandlerContext) error {

	if pingMessage.Topic() != wire.Ping {
		return errors.New("bad message topic for handler expected Ping message")
	}
	response := wire.NewPongMessage().Marshal()
	if response == nil {
		return errMessageType
	}
	if ctx != nil && ctx.IsRequest() {
		return ctx.Send(response)
	}
	return n.Network.SendMessageToPeer(sender, wire.NewPongMessage())

}

// OnMessagePong is called whenever a peer receives a pong message
func (n *Node) OnMessagePong(sender noise.ID, pongMessage *wire.PongMessage, ctx *noise.HandlerContext) error {
	if pongMessage.Topic() != wire.Pong {
		return errors.New("bad message topic for handler expected PongMessage")

	}
	return nil
}

// OnMessageStatus is called whenever a peer receives a version message.
func (n *Node) OnMessageStatus(sender noise.ID, versionMessage *wire.StatusMessage, ctx *noise.HandlerContext) error {

	ourConfig := n.ChainConfig
	ourNodeStatus := n.GetNodeStatus()
	if !bytes.Equal(ourConfig.GenesisID(), versionMessage.GenesisHash) || ourConfig.GetBranchID() != versionMessage.BranchID {
		n.Logger.Warn("peer has incompatible status ! ", zap.Any("node status", ourNodeStatus), zap.Any("peer status", versionMessage))
		if ctx != nil && ctx.IsRequest() {
			return ctx.Send(wire.NewRejectMessage().Marshal())
		}

		err := n.Network.SendMessageToPeer(sender, wire.NewRejectMessage())
		if err != nil {
			return err
		}

		return nil
	}

	updatedStatus := wire.NewStatusMessage(versionMessage.ProtocolVersion, versionMessage.BranchID, versionMessage.TipHeight, versionMessage.ChainWork, versionMessage.TipRoot, versionMessage.TipHash, versionMessage.GenesisHash)

	n.Logger.Info("Received new status message from peer :", zap.String("peer", sender.String()))
	n.Network.Peers.UpdatePeer(sender, updatedStatus)
	// If the peer is inbound we send a reply message
	// otherwise we assert the peer as valid and add it to
	// the active peers cache.
	if n.Network.IsInbound(sender) {
		n.Logger.Info("peer is inbound")
		if ctx != nil && ctx.IsRequest() {
			response := ourNodeStatus.Marshal()
			if response == nil {
				return errMessageType
			}
			err := ctx.Send(response)
			if err != nil {
				return err
			}
		} else if ctx == nil {
			err := n.Network.SendMessageToPeer(sender, ourNodeStatus)
			if err != nil {
				return err
			}
		}
	}

	// If the peer has better height than ours ask for headers
	if versionMessage.TipHeight > ourNodeStatus.TipHeight {
		// Send GetBlocksMessage and return (response is dispatched to BlocksHandler)
		message := wire.NewGetHeadersMessage()
		message.StartingPoint = ourNodeStatus.TipHash
		message.MaxHeaders = wire.MaxHeadersPerMessage
		message.Reverse = false

		if ctx != nil && ctx.IsRequest() {
			response := message.Marshal()
			if response == nil {
				return errMessageType
			}
			return ctx.Send(response)
		}
		// reverse message true will send back last header first
		return n.Network.SendMessageToPeer(sender, message)
	}
	return nil
}

// OnMessageGetBlocks prepares a reply to a GetBlocks message.
func (n *Node) OnMessageGetBlocks(sender noise.ID, requestedBlocks *wire.GetBlocksMessage, ctx *noise.HandlerContext) error {

	// prepare reply
	blocksMessage := wire.NewBlocksMessage()
	// end == nil implies peer wants all blocks from start
	store := n.Blockchain.GetChainView()
	blockHashes := requestedBlocks.BlockHashes

	for i := 0; i < len(blockHashes); i++ {

		hash := blockHashes[i]
		blk, err := store.GetBlockByHash(bytesutil.ToBytes32(hash))
		if err != nil {
			return err
		}

		blockBytes, err := blk.Marshal()
		if err != nil {
			return err
		}
		blocksMessage.BlockBodies = append(blocksMessage.BlockBodies, blockBytes)
		// in we ask for less limitBlocksToSend
		// this part doesn't execute and we flush the message with the prefilled
		// blocks

		if i%wire.MaxBlocksPerMessage == 0 {

			if ctx != nil {
				response := blocksMessage.Marshal()
				if response == nil {
					return errMessageType
				}
				err := ctx.Send(response)
				if err != nil {
					return err
				}
			}
			err := n.Network.SendMessageToPeer(sender, blocksMessage)
			if err != nil {
				return err
			}
			blocksMessage = wire.NewBlocksMessage()
		}
	}
	// queue any remaining blocks
	if ctx != nil && ctx.IsRequest() {
		response := blocksMessage.Marshal()
		if response == nil {
			return errMessageType
		}
		return ctx.Send(response)
	}
	return n.Network.SendMessageToPeer(sender, blocksMessage)
}

// OnMessageGetHeaders prepares a reply to GetHeaders message.
// TODO : Batch LevelDB get ops.
func (n *Node) OnMessageGetHeaders(sender noise.ID, requestedHeaders *wire.GetHeadersMessage, ctx *noise.HandlerContext) error {

	start := requestedHeaders.StartingPoint
	if start == nil {
		return errors.New("GetHeaders message starting point can't be nil")
	}
	n.Logger.Info("handling GetHeaders message from peer", zap.Any("getheaders", requestedHeaders))
	store := n.Blockchain.GetChainView()
	tip := store.GetChainTip()

	finalHdr, err := store.GetHeaderByHeight(tip.MostWorkHeight, ledger.MAIN)
	if err != nil {
		n.Logger.Warn("failed to handle GetHeaders message with error", zap.String("blockHash", hex.EncodeToString(start)), zap.Error(err))
		if ctx != nil && ctx.IsRequest() {
			err := ctx.Send(wire.NewRejectMessage().Marshal())
			if err != nil {
				return err
			}
		}
		return err
	}
	n.Logger.Warn("Found Block Start")

	finalHash, _ := finalHdr.Hash()
	Headers, err := store.GetHeadersInRange(bytesutil.ToBytes32(start), finalHash)

	if err != nil {
		n.Logger.Warn("failed to handle GetHeaders message with error", zap.String("blockHash", hex.EncodeToString(start)), zap.Error(err))
		if ctx != nil && ctx.IsRequest() {
			err := ctx.Send(wire.NewRejectMessage().Marshal())
			if err != nil {
				return err
			}
		}
		return err
	}

	serializedHeaders := make([][]byte, 0, len(Headers))

	for _, hdr := range Headers {

		serializedHdr, err := hdr.Marshal()

		if err != nil {
			n.Logger.Warn("failed to handle GetHeaders message with error", zap.String("blockHash", hex.EncodeToString(start)), zap.Error(err))
			if ctx != nil && ctx.IsRequest() {
				err := ctx.Send(wire.NewRejectMessage().Marshal())
				if err != nil {
					return err
				}
			}
			return err
		}
		serializedHeaders = append(serializedHeaders, serializedHdr)
	}
	// If reverse means the we want the last header first
	//
	// prepare reply
	n.Logger.Warn("Preparing GetHeaders reply")

	headersMessage := wire.HeadersMessage{
		BlockHeaders: serializedHeaders,
	}
	if ctx != nil && ctx.IsRequest() {
		response := headersMessage.Marshal()
		if response == nil {
			return errMessageType
		}
		return ctx.Send(response)
	}
	return n.Network.SendMessageToPeer(sender, headersMessage)
}

// OnMessageGetData prepares a reply to a get data message.
func (n *Node) OnMessageGetData(sender noise.ID, requestedData *wire.GetDataMessage, ctx *noise.HandlerContext) error {

	itemType := requestedData.InvType
	store := n.Blockchain.GetStateView(n.GetNodeStatus().TipRoot)
	if itemType == wire.Contract {
		// Making GetBatch request
		contracts, err := store.GetBatch(requestedData.Hashes)
		if err != nil {
			return err
		}
		resp := wire.NewNodeDataMessage()

		for _, contract := range contracts {

			contractBytes, err := contract.Marshal()
			if err != nil {
				return err
			}
			resp.Data = append(resp.Data, contractBytes)
		}
	} else {

		return errors.New("unknown getdata type")
	}
	return nil
}

// OnMessageHeaders handles an incoming list of headers to process
func (n *Node) OnMessageHeaders(sender noise.ID, headers *wire.HeadersMessage) error {

	headersToProcess := make([]*types.Header, 0, len(headers.BlockHeaders))
	headerHashes := make([][]byte, 0, len(headers.BlockHeaders))

	for _, headerBytes := range headers.BlockHeaders {
		hdr := &types.Header{}
		err := hdr.Unmarshal(headerBytes)
		if err != nil {
			n.Logger.Error("failed to unmarshal incoming headers with error :", zap.String("peer", sender.String()), zap.Error(err))
			return err
		}
		headerHash, _ := hdr.Hash()
		headerHashes = append(headerHashes, headerHash.Bytes())
		headersToProcess = append(headersToProcess, hdr)
	}

	for _, header := range headersToProcess {
		n.Logger.Info("Received new headers :", zap.Any("Header", header))
	}

	return nil
}

// OnMessageBlocks handles an incoming list of blocks to process
func (n *Node) OnMessageBlocks(sender noise.ID, blocks *wire.BlocksMessage) error {

	newBlockBatch := &service.BlockBatch{}
	newBlockBatch.BlockBodies = make([]*types.Block, 0, len(blocks.BlockBodies))
	newBlockBatch.Hashes = make([][]byte, 0, len(blocks.BlockHashes))
	for _, blockBytes := range blocks.BlockBodies {
		blk := &types.Block{}
		err := blk.Unmarshal(blockBytes)
		if err != nil {
			n.Logger.Error("failed to unmarshal incoming block bodies with error :", zap.String("peer", sender.String()), zap.Error(err))
			return err
		}

		newBlockBatch.BlockBodies = append(newBlockBatch.BlockBodies, blk)
	}

	for _, block := range newBlockBatch.Hashes {
		n.Logger.Info("Received new blocks :", zap.String("blockHash", hex.EncodeToString(block)))
	}

	res := make(chan service.Result, 1)
	newBlockBatch.ResultChan = res

	n.AdmissionControl.QueueBlockBatch(newBlockBatch)

	result := <-res

	return result.Err
}

// OnMessageNewBlock handles incoming broadcasted blocks, the function blocks
// until a response is read to decide whether or not a second interaction is required
// such as sending reject messages for invalid blocks or requesting parents in case
// of an orphan or missing parent.
func (n *Node) OnMessageNewBlock(sender noise.ID, newBlock *wire.NewBlockMessage) error {

	block := &types.Block{}

	err := block.UnmarshalFull(newBlock.Block)
	if err != nil {
		return n.Network.SendMessageToPeer(sender, wire.NewRejectMessage())
	}
	blockHash, err := block.Hash()
	if err != nil {
		return n.Network.SendMessageToPeer(sender, wire.NewRejectMessage())
	}
	ok, err := chain.VerifyBlockWithoutContext(block)
	if !ok || err != nil {
		return err
	}

	missingBlocks := make([][]byte, 0)

	view := n.Blockchain.GetChainView()

	// First try and see if we have the parent
	_, err = view.GetHeaderByHash(block.GetParent())
	if err != nil {
		if err == ledger.ErrHeaderNotFound {
			missingBlocks = append(missingBlocks, block.GetParent().Bytes())
		}
		return err
	}
	// If the block references uncles see if we have them
	if len(block.GetUncles()) > 0 {
		for _, uncle := range block.GetUncles() {
			_, err := view.GetHeaderByHash(uncle)
			if err != nil {
				if err == ledger.ErrHeaderNotFound {
					missingBlocks = append(missingBlocks, uncle.Bytes())
				}
				return err
			}
		}
	}
	// If we have any missing blocks ask for them
	if len(missingBlocks) > 0 {
		getBlocks := wire.NewGetBlocksMessage()
		getBlocks.BlockHashes = missingBlocks
		getBlocks.Full = true

		resp, err := n.Network.SendRequestToPeer(sender, getBlocks)

		if err != nil {
			return err
		}

		message, err := wire.UnmarshalWireMessage(resp)

		if err != nil {
			return err
		}

		blocks, ok := message.(*wire.BlocksMessage)
		if !ok {
			return errMessageType
		}

		err = n.OnMessageBlocks(sender, blocks)
		if err != nil {
			return err
		}
	}

	resp := make(chan error, 1)

	incomingBlock := service.IncomingBlock{
		Mined:      false,
		Processed:  false,
		Block:      block,
		Hash:       blockHash,
		ResultChan: resp,
	}
	n.AdmissionControl.QueueNewBlock(incomingBlock)

	err = <-resp
	return err
}

// OnMessageNewTransaction handles incoming broadcasted transactions
func (n *Node) OnMessageNewTransaction(sender noise.ID, newTx *wire.TransactionMessage) error {

	tx := &types.Transaction{}

	err := tx.Unmarshal(newTx.Body)
	if err != nil {
		return n.Network.SendMessageToPeer(sender, wire.NewRejectMessage())
	}
	txid, err := tx.TXID()
	if err != nil {
		return n.Network.SendMessageToPeer(sender, wire.NewRejectMessage())
	}

	resp := make(chan service.Result, 1)

	incomingTx := service.IncomingTx{
		TX:         *tx,
		Processed:  false,
		TXID:       txid.Bytes(),
		ResultChan: resp,
	}

	n.AdmissionControl.QueueNewTransaction(incomingTx)

	res := <-resp

	if !res.Ok || res.Err != nil {
		return err
	}
	return nil
}

// OnMessageReject disconnects the peer.
func (n *Node) OnMessageReject(sender noise.ID, rejectMessage *wire.RejectMessage, ctx *noise.HandlerContext) error {

	return errIncompatiblePeer
}
