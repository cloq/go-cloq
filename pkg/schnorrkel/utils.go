package schnorrkel

import (
	"crypto/rand"

	ristretto "github.com/gtank/ristretto255"
)

// https://github.com/w3f/schnorrkel/blob/718678e51006d84c7d8e4b6cde758906172e74f8/src/scalars.rs#L18
func divideScalarByCofactor(s []byte) []byte {
	l := len(s) - 1
	low := byte(0)
	for i := range s {
		r := s[l-i] & 0b00000111 // remainder
		s[l-i] >>= 3
		s[l-i] += low
		low = r << 5
	}

	return s
}

// RandomElement returns a random ristretto element
func RandomElement() (*ristretto.Element, error) {
	e := ristretto.NewElement()
	s := [64]byte{}
	_, err := rand.Read(s[:])
	if err != nil {
		return nil, err
	}

	return e.FromUniformBytes(s[:]), nil
}

// RandomScalar returns a random ristretto scalar
func RandomScalar() (*ristretto.Scalar, error) {
	s := [64]byte{}
	_, err := rand.Read(s[:])
	if err != nil {
		return nil, err
	}

	ss := ristretto.NewScalar()
	return ss.FromUniformBytes(s[:]), nil
}

// ScalarFromBytes returns a ristretto scalar from the input bytes
func ScalarFromBytes(b [32]byte) (*ristretto.Scalar, error) {
	s := ristretto.NewScalar()
	err := s.Decode(b[:])
	if err != nil {
		return nil, err
	}

	return s, nil
}
