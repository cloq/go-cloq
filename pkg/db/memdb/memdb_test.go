package memdb

import (
	"bytes"
	"fmt"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cloq/go-cloq/core/crypto"
)

var (
	TestKey1 = "tempkey1"
	TestKey2 = "tempkey2"
	TestVal1 = "val1"
	TestVal2 = "val2"
	TestDict = map[string]string{
		"bitcoin":  "btc",
		"litecoin": "ltc",
		"cloq":     "clq",
		"bismuth":  "bis",
		"ethereum": "eth",
		"viacoin":  "via",
	}
)

func TestPutGet(t *testing.T) {

	testDB := New(1)

	k := []byte("cloqiscool")
	v := []byte("whatisconsensus")

	testDB.Put(k, v)

	vTest := testDB.Get(k)

	if bytes.Compare(v, vTest) != 0 {
		t.Errorf("failed to put and get")
	}

}

func TestDelete(t *testing.T) {

	testDB := New(1)

	k := []byte("cloqiscool")
	v := []byte("whatisconsensus")

	k2 := []byte("genesis")
	v2 := crypto.Blake256(k2)

	testDB.Put(k, v)
	testDB.Put(k2, v2[:])

	testDB.Delete(k)

	vTest := testDB.Get(k)
	vTest2 := testDB.Get(k2)

	if vTest != nil || bytes.Compare(v2[:], vTest2) != 0 {
		t.Errorf("failed to delete key")
	}

}

func TestHas(t *testing.T) {

	testDB := New(1)

	k := []byte("cloqiscool")
	v := []byte("whatisconsensus")

	k2 := []byte("genesis")
	v2 := crypto.Blake256(k2)

	testDB.Put(k, v)
	testDB.Put(k2, v2[:])

	testDB.Delete(k)

	vTest := testDB.Has(k)
	vTest2 := testDB.Has(k2)

	if vTest != false || vTest2 != true {
		t.Errorf("failed has key")
	}

}

func TestTx(t *testing.T) {

	testDB := New(1)

	k := []byte("cloqiscool")
	v := []byte("whatisconsensus")

	k2 := []byte("genesis")
	v2 := crypto.Blake256(k2)

	tx := testDB.NewTx()

	tx.Put(k, v)
	tx.Put(k2, v2[:])

	tx.Delete(k)

	tx.Commit()

	vTest := testDB.Has(k)
	vTest2 := testDB.Has(k2)

	if vTest != false || vTest2 != true {
		t.Errorf("failed has key")
	}
}
func TestBulk(t *testing.T) {

	db := New(10000)

	// create a new Bulk instance
	bulk := db.NewBulk()

	// run a bulk operation
	for i := 0; i < 10000; i++ {
		bulk.Put([]byte(fmt.Sprintf("key%d", i)),
			[]byte(TestVal2))
	}

	bulk.Flush()

	// verify value visibility

	for i := 0; i < 10000; i++ {
		assert.Equal(t, TestVal2, string(db.Get([]byte(fmt.Sprintf("key%d", i)))))
	}

	db.Close()

}

func TestIter(t *testing.T) {

	db := New(10)
	for i := 0; i < 8; i++ {
		db.Put([]byte(strconv.Itoa(i)), []byte(strconv.Itoa(i)))
	}

	i := 0
	for iter := db.Iterator(nil, nil); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		i++
	}

	db.Close()

}

func TestRangeIter(t *testing.T) {

	db := New(10)

	for i := 1; i < 8; i++ {
		db.Put([]byte(strconv.Itoa(i)), []byte(strconv.Itoa(i)))
	}

	// test iteration 2 -> 5
	i := 2
	for iter := db.Iterator([]byte("2"), []byte("5")); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Value()))
		i++
	}
	assert.EqualValues(t, i, 5)

	// nil sames with []byte("0")
	// test iteration 0 -> 5
	i = 1
	for iter := db.Iterator(nil, []byte("5")); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Value()))
		i++
	}
	assert.EqualValues(t, i, 5)

	db.Close()

}

func TestReverseIter(t *testing.T) {

	db := New(10)

	for i := 1; i < 8; i++ {
		db.Put([]byte(strconv.Itoa(i)), []byte(strconv.Itoa(i)))
	}

	// test reverse iteration 5 <- 2
	i := 5
	for iter := db.Iterator([]byte("5"), []byte("2")); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Value()))
		i--
	}
	assert.EqualValues(t, i, 2)

	i = 5
	for iter := db.Iterator([]byte("5"), nil); iter.Valid(); iter.Next() {
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Key()))
		assert.EqualValues(t, strconv.Itoa(i), string(iter.Value()))
		i--
	}
	assert.EqualValues(t, i, 0)

	db.Close()
}
