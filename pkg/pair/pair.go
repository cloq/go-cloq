// Package pair implements a pair type key,value as byte arrays
// it supports sorting by key and flattening to two separate slices.
// It's used extensively to manage trie updates .
package pair

import (
	"bytes"
	"sort"
)

// Pair implements a pair of bytearrays
type Pair struct {
	k []byte
	v []byte
}

// New creates a new pair
func New(key, value []byte) Pair {
	return Pair{key, value}
}

// SortPairs returns a flattened sorted key,pair slices
func SortPairs(keys, values [][]byte) ([][]byte, [][]byte) {
	return Flatten(NewFromList(keys, values))
}

// NewFromList creates a new pair lis
func NewFromList(keys [][]byte, values [][]byte) []Pair {
	pairs := make([]Pair, 0, len(keys))

	for idx, key := range keys {
		p := Pair{key, values[idx]}
		pairs = append(pairs, p)
	}
	return pairs
}

// Key returns the key
func (p *Pair) Key() []byte {
	return p.k
}

// Value returns the value
func (p *Pair) Value() []byte {
	return p.v
}

type byKey []Pair

func (bk byKey) Len() int           { return len(bk) }
func (bk byKey) Swap(i, j int)      { bk[i], bk[j] = bk[j], bk[i] }
func (bk byKey) Less(i, j int) bool { return bytes.Compare(bk[i].k, bk[j].k) == -1 }

// Flatten creates a new flattened list of sorted pairs
func Flatten(pairs []Pair) ([][]byte, [][]byte) {

	keys := make([][]byte, len(pairs))
	values := make([][]byte, len(pairs))
	sort.Sort(byKey(pairs))

	for idx, p := range pairs {
		keys[idx] = append(keys[idx], p.k...)
		values[idx] = append(values[idx], p.v...)
	}

	return keys, values
}
