// Copyright 2018 The OPA Authors.  All rights reserved.
// Use of this source code is governed by an Apache2
// license that can be found in the LICENSE file.

package leb128

import (
	"bytes"
	"testing"
)

// Test cases copied from http://dwarfstd.org/doc/Dwarf3.pdf.

func TestReadVarUint64(t *testing.T) {

	tests := []struct {
		val  []byte
		want uint64
	}{
		{
			val:  []byte("\x02"),
			want: 2,
		},
		{
			val:  []byte("\x7F"),
			want: 127,
		},
		{
			val:  []byte("\x80\x01"),
			want: 128,
		},
		{
			val:  []byte("\x81\x01"),
			want: 129,
		},
		{
			val:  []byte("\x82\x01"),
			want: 130,
		},
		{
			val:  []byte("\xB9\x64"),
			want: 12857,
		},
	}

	for i, tc := range tests {
		result, err := ToUint64(tc.val)
		if err != nil {
			t.Fatalf("Case %d, err: %v", i, err)
		} else if result != tc.want {
			t.Fatalf("Case %d, expected %v, but got %v", i, tc.want, result)
		}
	}

}

func TestReadVarInt64(t *testing.T) {

	tests := []struct {
		val  []byte
		want int64
	}{
		{
			val:  []byte("\x02"),
			want: 2,
		},
		{
			val:  []byte("\x7E"),
			want: -2,
		},
		{
			val:  []byte("\xFF\x00"),
			want: 127,
		},
		{
			val:  []byte("\x81\x7F"),
			want: -127,
		},
		{
			val:  []byte("\x80\x01"),
			want: 128,
		},
		{
			val:  []byte("\x80\x7F"),
			want: -128,
		},
		{
			val:  []byte("\x81\x01"),
			want: 129,
		},
		{
			val:  []byte("\xFF\x7E"),
			want: -129,
		},
	}

	for i, tc := range tests {
		result, err := ToInt64(tc.val)
		if err != nil {
			t.Fatalf("Case %d, err: %v", i, err)
		} else if result != tc.want {
			t.Fatalf("Case %d, expected %v, but got %v", i, tc.want, result)
		}
	}
}

func TestWriteVarUint64(t *testing.T) {

	tests := []struct {
		val   []byte
		input uint64
	}{
		{
			val:   []byte("\x02"),
			input: 2,
		},
		{
			val:   []byte("\x7F"),
			input: 127,
		},
		{
			val:   []byte("\x80\x01"),
			input: 128,
		},
		{
			val:   []byte("\x81\x01"),
			input: 129,
		},
		{
			val:   []byte("\x82\x01"),
			input: 130,
		},
		{
			val:   []byte("\xB9\x64"),
			input: 12857,
		},
	}

	for i, tc := range tests {

		buf := FromUint64(tc.input)

		if !bytes.Equal(buf, tc.val) {
			t.Fatalf("Case %d, expected %v, but got %v", i, tc.val, buf)
		}
	}

}

func TestWriteVarInt64(t *testing.T) {

	tests := []struct {
		val   []byte
		input int64
	}{
		{
			val:   []byte{0x1},
			input: 0x1,
		},
		{
			val:   []byte("\x02"),
			input: 2,
		},
		{
			val:   []byte("\x7E"),
			input: -2,
		},
		{
			val:   []byte("\xFF\x00"),
			input: 127,
		},
		{
			val:   []byte("\x81\x7F"),
			input: -127,
		},
		{
			val:   []byte("\x80\x01"),
			input: 128,
		},
		{
			val:   []byte("\x80\x7F"),
			input: -128,
		},
		{
			val:   []byte("\x81\x01"),
			input: 129,
		},
		{
			val:   []byte("\xFF\x7E"),
			input: -129,
		},
	}

	for i, tc := range tests {

		buf := FromInt64(tc.input)

		if !bytes.Equal(buf, tc.val) {
			t.Fatalf("Case %d, expected %v, but got %v", i, tc.val, buf)
		}
	}
}

func TestMinimalConsistency(t *testing.T) {
	testcase := []struct {
		val   []byte
		input uint64
	}{
		{
			val:   []byte{0x1},
			input: 0x1,
		},
	}

	for _, test := range testcase {
		buf := FromUint64(test.input)
		enc1, _ := ToUint64(buf)
		enc2, _ := ToInt64(buf)

		t.Log(enc1)
		t.Log(enc2)

	}
}
