package merkle

import (
	"bytes"
	"testing"

	"gitlab.com/cloq/go-cloq/pkg/leb128"
)

var testTrees = [][]byte{
	[]byte("Valar Morghulis"),
	[]byte("Satoshi Nakamoto"),
	[]byte("Craig Wright is a fraud"),
	[]byte(""),
	[]byte("Hello Qloc"),
}

var testConsistencyTree = [][]byte{
	[]byte("Hello"),
	[]byte("Hi"),
	[]byte("Hey"),
	[]byte("Hola"),
}

var testDupTrees = [][]byte{
	[]byte("Valar Morghulis"),
	[]byte("Satoshi Nakamoto"),
	[]byte("Craig Wright is a fraud"),
	[]byte(""),
	[]byte("Hello Qloc"),
	[]byte("Hello Qloc"),
}

var testIntTree = [][]byte{
	leb128.FromInt64(1),
	leb128.FromInt64(2),
	leb128.FromInt64(3),
	leb128.FromInt64(4),
	leb128.FromInt64(5),
	leb128.FromInt64(6),
}

var testIntDupTree = [][]byte{
	leb128.FromInt64(1),
	leb128.FromInt64(2),
	leb128.FromInt64(3),
	leb128.FromInt64(4),
	leb128.FromInt64(5),
	leb128.FromInt64(6),
	leb128.FromInt64(5),
	leb128.FromInt64(6),
}
var testTreesRoot = unhexify("9b574ebce21c9005cfd46708ca0de61f3202b83af6a9dae9e64722ef6751b5b6")

func TestRoots(t *testing.T) {

	// empty root hash
	t.Log(hexify(Root([][]byte{})))
	root := Root(testTrees)
	if !bytes.Equal(testTreesRoot, root) {
		t.Errorf("TestRoot failed expected %s, got %s \n", hexify(testTreesRoot), hexify(root))
	}

	t.Log(Root(testConsistencyTree))
}

func TestProofs(t *testing.T) {
	ap, _ := Proof(testTrees, 0)

	for _, p := range ap {
		t.Logf(hexify(p.Val))
	}

	if !Verify(testTrees, 0, ap) {
		t.Error("failed to verify proof")
	}

	if bytes.Equal(Root(testTrees), Root(testDupTrees)) {
		t.Error("Duplicate Tree items result in the same hash")
	}

	if bytes.Equal(Root(testIntTree), Root(testIntDupTree)) {
		t.Error("Duplicate Tree items result in the same hash")
	}
	t.Log(hexify(Root(testIntTree)), hexify(Root(testIntDupTree)))
}
