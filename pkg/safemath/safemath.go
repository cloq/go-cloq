// Copyright 2018 The cloq Authors
// This file is part of the cloq-core library .
//
// The cloq-core library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The cloq-core library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the cloq-core library. If not, see <http://www.gnu.org/licenses/>.

// Package safemath provides math utilities
package safemath

import (
	"errors"
	"math"
)

var (
	// ErrUint64MultOverflow is returned if a multiplication overflows
	ErrUint64MultOverflow = errors.New("uint64 multiplication overflow")
	// ErrUint64AddOverflow is returned if when an addition overflows
	ErrUint64AddOverflow = errors.New("uint64 addition overflow")
	// ErrMathOverflow is generic math overflow
	ErrMathOverflow = errors.New("addition overflow")
)

// AddInt64 returns a + b
// with an integer overflow check.
func AddInt64(a, b int64) (sum int64, ok bool) {
	if (b > 0 && a > math.MaxInt64-b) ||
		(b < 0 && a < math.MinInt64-b) {
		return 0, false
	}
	return a + b, true
}

// MulUint64 multiplies a by b, returning an error if the values would overflow
func MulUint64(a, b uint64) (uint64, error) {
	c := a * b
	if a != 0 && c/a != b {
		return 0, ErrUint64MultOverflow
	}
	return c, nil
}

// AddUint64 adds a and b, returning an error if the values would overflow
func AddUint64(a, b uint64) (uint64, error) {
	c := a + b
	if c < a || c < b {
		return 0, ErrUint64AddOverflow
	}
	return c, nil
}

// AddUint32 adds a and b returning an error if the values would overflow
func AddUint32(a, b uint32) (uint32, error) {
	if math.MaxUint32-a < b {
		return 0, ErrUint64AddOverflow
	}
	return a + b, nil
}
