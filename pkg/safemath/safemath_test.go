package safemath

import (
	"math"
	"testing"
)

func TestAdd64(t *testing.T) {
	var a uint64 = 0xffffffffffffffff
	var x uint64 = math.MaxUint64
	t.Logf("a == x %t ", a == x)
	b, err := AddUint64(a, uint64(1))
	if err == nil || b != 0 {
		t.Errorf("Add should overflow with a = %d and b = %d \n", a, 1)
	}
	t.Logf("Add overflow with a = %d and b = %d  | a + b = %d\n", a, 1, b)

	c, err := AddUint64(a, x)

	if err == nil || c != 0 {
		t.Errorf("Add should overflow tih a = %d and x = %d\n", a, x)
	}
	t.Logf("c = %d\n", c)

}

func TestMul64(t *testing.T) {
	var a uint64 = 0xffffffffffffffff
	var x uint64 = math.MaxUint64 / 2
	b, err := MulUint64(a, uint64(2))
	if err == nil || b != 0 {
		t.Errorf("Mul should overflow with a = %d and b = %d \n", a, 2)
	}
	t.Logf("Mul overflow with a = %d and b = %d  | a * b = %d | a = %x\n ", a, 2, b, a)

	c, err := MulUint64(a, x)

	if err == nil || c != 0 {
		t.Errorf("Mul should overflow tih a = %d and x = %d\n", a, x)
	}
	t.Logf("c = %d\n", c)

	c, err = MulUint64(uint64(2), x)

	if err != nil || c == 0 {
		t.Errorf("Mul shouldn't overflow with a = %d and x = %d\n", 2, x)
	}
	t.Logf("c = %d and %t c = %x\n ", c, c+1 == a, c+1)

}
