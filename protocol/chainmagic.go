package protocol

const (
	// MainnetAddressPrefix for cloq mainnet addresses.
	MainnetAddressPrefix = "cq"
	// TestnetAddressPrefix for cloq testnet addresses.
	TestnetAddressPrefix = "tq"
	// RegnetAddressPrefix for cloq regtest addresses.
	RegnetAddressPrefix = "xq"

	// WhirpoolBranchID is the branchID of whirpool release.
	WhirpoolBranchID uint32 = 0x7ce9fea3
	// MainnetMagic defines network packet magic for mainnet.
	MainnetMagic uint32 = 0x9c67b4de
	// TestnetMagic defines network packet magic for testnet.
	TestnetMagic uint32 = 0xafe8d4c5

	// WhirlpoolConsensusVer is the version of objects in whirlpool network.
	WhirlpoolConsensusVer = 1
)
