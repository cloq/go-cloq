package script

import (
	"errors"
)

// Builder builds a script by gradually adding opcodes or data .
type Builder struct {
	version byte
	script  Script
	err     error
}

// AddOP appends an opcode to the script
func (builder *Builder) AddOP(op OP) *Builder {

	if builder.err != nil {
		return builder
	}
	builder.script = append(builder.script, byte(op))
	return builder
}

// AddData appends a bytearray as opcode data it takes here to length
// prefix the argument and verify it doesn't go over the stack frame limit.
func (builder *Builder) AddData(data []byte) *Builder {
	length := len(data)
	if length > 512 {
		builder.err = errors.New("exceeded max data size")
		return builder
	}
	opcode := OPPush

	var b = make([]byte, 0, len(data)+2)
	b = append(b, byte(opcode))
	b = append(b, byte(length))
	b = append(b, data...)

	builder.script = append(builder.script, b...)

	return builder
}

// Script outputs built script
func (builder *Builder) Script() []byte {
	return builder.script
}

// NewBuilder creates a new instance of script Builder
func NewBuilder(version byte) *Builder {

	script := append([]byte{}, version)
	return &Builder{
		version: version,
		script:  script,
		err:     nil,
	}
}
