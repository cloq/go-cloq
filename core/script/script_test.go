package script

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
)

func TestParseOPcode(t *testing.T) {
	cases := []struct {
		prog    []byte
		pc      uint32
		want    Instruction
		wantErr error
	}{{
		prog: []byte{byte(OPFalse)},
		want: Instruction{Opcode: OPFalse, Data: []byte{0}, Length: 1},
	}, {
		prog: []byte{byte(OPPush), 5, 1, 1, 1, 1, 1},
		want: Instruction{Opcode: OPPush, Data: []byte{1, 1, 1, 1, 1}, Length: 7},
	}, {
		prog:    []byte{byte(OPPush)},
		wantErr: ErrShortProgram,
	}, {
		prog:    []byte{byte(OPPush), 5},
		wantErr: ErrShortProgram,
	}, {
		prog: []byte{byte(OPPush), 6, 1, 1, 1, 1, 1, 255},
		want: Instruction{Opcode: OPPush, Data: []byte{1, 1, 1, 1, 1, 255}, Length: 8},
	}, {
		prog: []byte{byte(OPPush), 1, 1},
		want: Instruction{Opcode: OPPush, Data: []byte{1}, Length: 3},
	}, {
		prog: []byte{byte(OPPush), 2, 1, 255},
		want: Instruction{Opcode: OPPush, Data: []byte{1, 255}, Length: 4},
	}, {
		prog: []byte{byte(OPPush), 1, 1},
		want: Instruction{Opcode: OPPush, Data: []byte{1}, Length: 3},
	}, {
		prog: []byte{byte(OPPush), 4, 1, 255, 255, 255},
		want: Instruction{Opcode: OPPush, Data: []byte{1, 255, 255, 255}, Length: 6},
	}, {
		prog: []byte{byte(OPPush), 4, 1, 1, 1, 1},
		want: Instruction{Opcode: OPPush, Data: []byte{1, 1, 1, 1}, Length: 6},
	}, {
		prog: []byte{byte(OPPush), 3, 1, 1, 255},
		want: Instruction{Opcode: OPPush, Data: []byte{1, 1, 255}, Length: 5},
	}, {
		prog: []byte{byte(OPPush), 32, 146, 218, 217, 68, 62, 77, 214, 215, 10, 127, 17, 135, 33, 1, 235, 255, 135, 226, 23, 152, 228, 251, 178, 111, 164, 191, 89, 14, 180, 64, 231, 27},
		want: Instruction{Opcode: OPPush, Data: []byte{146, 218, 217, 68, 62, 77, 214, 215, 10, 127, 17, 135, 33, 1, 235, 255, 135, 226, 23, 152, 228, 251, 178, 111, 164, 191, 89, 14, 180, 64, 231, 27}, Length: 34},
	}, {
		prog:    []byte{},
		wantErr: ErrFarJump,
	}, {
		prog:    []byte{byte(OP0)},
		pc:      1,
		wantErr: ErrFarJump,
	}, {
		prog:    []byte{byte(OPPush)},
		wantErr: ErrShortProgram,
	}, {
		prog:    []byte{byte(OPPush), 1},
		wantErr: ErrShortProgram,
	}, {
		prog:    []byte{byte(OPPush)},
		wantErr: ErrShortProgram,
	}, {
		prog:    []byte{byte(OPPush), 1, 0},
		want:    Instruction{Opcode: OPPush, Length: 3, Data: []byte{0}},
		wantErr: nil,
	}, {
		prog:    []byte{byte(OPPush)},
		wantErr: ErrShortProgram,
	}, {
		prog:    []byte{byte(OPPush), 1, 0},
		wantErr: nil,
		want:    Instruction{Opcode: OPPush, Length: 3, Data: []byte{0}},
	}}

	for _, c := range cases {
		t.Run(fmt.Sprintf("%d: %x", c.pc, c.prog), func(t *testing.T) {
			got, gotErr := ParseOP(c.prog, c.pc)

			if gotErr != c.wantErr {
				t.Errorf("ParseOpcode(%x, %d) error = %v want %v", c.prog, c.pc, gotErr, c.wantErr)
			}

			if c.wantErr != nil {
				return
			}

			if !instEquals(got, c.want) {
				t.Errorf("ParseOpcode(%x, %d) = %+v want %+v", c.prog, c.pc, got, c.want)
			}
		})
	}
}

func TestCompile(t *testing.T) {
	testCases := []struct {
		script   string
		expected []byte
		err      error
	}{
		{"blake2 0x3c228306552177f5a304cb12a5b5e60897f2f486b64671afdccf0f8dd9410cbd equal", []byte{164, 74, 32, 60, 34, 131, 6, 85, 33, 119, 245, 163, 4, 203, 18, 165, 181, 230, 8, 151, 242, 244, 134, 182, 70, 113, 175, 220, 207, 15, 141, 217, 65, 12, 189, 128}, nil},
		{"wutwut", nil, wrapError(ErrToken, "wutwut")},
		{"sha3 push 0xb966a57f3010512d1ebe8629864c71c909aeb4752e93ffa525f00976c93e33c0", nil, wrapError(ErrToken, "push")},
		{"true 1", []byte{1, 74, 1, 1}, nil},
		{"false 0", []byte{0, 74, 1, 0}, nil},
		{"1", []byte{74, 1, 1}, nil},
	}

	for _, test := range testCases {
		got, gotErr := Compile(test.script)
		if gotErr != nil {
			if gotErr.Error() != test.err.Error() {
				t.Logf("failed with wrong error got : %v,expected : %v", gotErr, test.err)
			}
		}
		if !bytes.Equal(got, test.expected) {
			t.Errorf("failed to assemble script correctly expected %v got %v", test.expected, got)
		}
		// no need to disassemble bad tokenized scripts they're always empty
		if test.err == nil {
			disas, err := Decompile(got)
			if strings.Compare(disas, test.script) != 0 || err != nil {
				t.Logf("failed to disassemble script correctly with error : %v | expected %v got %v", err, test.script, disas)
			}
		} else if test.err != nil {
			disas, err := Decompile(got)
			if disas != "" {
				t.Logf("expected empty string got %v with err : %v", disas, err)

			}
		}

	}

}
func instEquals(inst1, inst2 Instruction) bool {
	if !bytes.Equal(inst1.Data, inst2.Data) || inst1.Length != inst2.Length || inst1.Opcode != inst2.Opcode {
		return false
	}

	return true
}
