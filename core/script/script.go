package script

import (
	"bufio"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/cloq/go-cloq/pkg/leb128"
	"gitlab.com/cloq/go-cloq/pkg/safemath"
	"golang.org/x/crypto/sha3"
)

// Script is raw bytecode
type Script []byte

// Asm returns the script as assembly
func (s Script) Asm() (string, error) {
	asm, err := Decompile(s)
	if err != nil {
		return "", err
	}
	return asm, err
}

// String returns bytecode as hexadecimal
func (s Script) String() string {
	return hex.EncodeToString(s)
}

// Hash returns SHA3 digest of the script's bytecode
func (s Script) Hash() [32]byte {
	return sha3.Sum256(s)
}

// NewScript creates a copy of script
func NewScript(prog []byte) (Script, error) {
	var s = make(Script, len(prog))
	copy(s, prog)
	asm, err := s.Asm()
	if asm == "" || err != nil {
		return nil, err
	}
	return s, nil
}

// ParseOP reads the opcode at ip in program returns an instruction and an error
// in case of failure.
func ParseOP(prog []byte, ip uint32) (Instruction, error) {

	// make sure program isn't bigger than the limit

	progLen := uint32(len(prog))

	// make sure IP isn't in an illegal address
	if ip >= progLen {
		return Instruction{}, ErrFarJump
	}

	// fetch OP code at ip
	op := OP(prog[ip])

	// create new instruction for this opcode
	var inst Instruction

	inst.Opcode = op
	inst.Length = 1

	// if opcode has data (followed by elements) we need to parse it
	// if opcode is in range 0x01...0x16 the data is the opcode itself (number)
	if op == OPTrue || op == OPFalse {
		inst.Data = append(inst.Data, byte(op))
		return inst, nil
	} else if op == OPPush {

		if ip == progLen-1 {
			return inst, ErrShortProgram
		}
		// fetch length prefix
		n := prog[ip+1]
		inst.Length += uint32(n) + 1

		end, err := safemath.AddUint32(ip, inst.Length)
		if err != nil {
			return inst, err
		}

		if end > progLen {
			return inst, ErrShortProgram
		}
		inst.Data = prog[ip+2 : end]
		return inst, nil
	} else if op == OPJMPIF {
		// make sure we have a 4 bytes address to jump to
		inst.Length += 4
		end, err := safemath.AddUint32(ip, inst.Length)
		if err != nil {
			return inst, err
		}
		// make sure we're not jumping too far
		if end > progLen {
			return inst, ErrFarJump
		}
		// store jump address
		inst.Data = prog[ip+1 : end]
		return inst, nil
	}

	return inst, nil
}

// Compile a script to bytecode.
func Compile(s string) ([]byte, error) {

	var bc = make([]byte, 0, 128)
	var err error
	// maps labels to the location each refers to
	locations := make(map[string]uint32)

	// maps unresolved uses of labels to the locations that need to be filled in
	unresolved := make(map[string][]int)

	resolveJump := func(addr string, opcode OP) error {
		bc = append(bc, byte(opcode))
		l := len(bc)

		var tmp [4]byte
		bc = append(bc, tmp[:]...)

		if strings.HasPrefix(addr, "@") {
			unresolved[addr] = append(unresolved[addr], l)
			return nil
		}

		address, err := strconv.ParseUint(addr, 10, 32)
		if err != nil {
			return err
		}
		binary.LittleEndian.PutUint32(bc[l:], uint32(address))
		return nil
	}

	scanner := bufio.NewScanner(strings.NewReader(s))
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		token := scanner.Text()
		if info, ok := opsByName[token]; ok {
			if strings.HasPrefix(token, "push") || strings.HasPrefix(token, "jumpif") {
				return nil, wrapError(ErrToken, token)
			}
			bc = append(bc, byte(info.op))
		} else if strings.HasPrefix(token, "jumpif:") {
			err = resolveJump(strings.TrimPrefix(token, "jumpif:"), OPJMPIF)
			if err != nil {
				return nil, err
			}
		} else if strings.HasPrefix(token, "@") {
			if _, seen := locations[token]; seen {
				return nil, wrapError(ErrUndefinedLabel, token)
			}
			if len(bc) > MaxProgramSize {
				return nil, ErrLongProgram
			}
			locations[token] = uint32(len(bc))
		} else if strings.HasPrefix(token, "0x") {
			bytes, err := hex.DecodeString(strings.TrimPrefix(token, "0x"))
			if err != nil {
				return nil, err
			}
			bc = append(bc, pushData(bytes)...)
		} else if num, err := strconv.ParseInt(token, 10, 64); err == nil {
			bc = append(bc, pushData(leb128.FromInt64(num))...)
		} else {
			return nil, wrapError(ErrToken, token)
		}
	}
	err = scanner.Err()
	if err != nil {
		return nil, err
	}

	for label, uses := range unresolved {
		location, ok := locations[label]
		if !ok {
			return nil, fmt.Errorf("undefined label %s", label)
		}
		for _, use := range uses {
			binary.LittleEndian.PutUint32(bc[use:], location)
		}
	}

	return bc, nil
}

// Decompile a program from raw bytecode to script
func Decompile(prog []byte) (string, error) {

	var asm []Instruction

	// maps program locations (used as jump targets) to a label for each
	var labels = make(map[uint32]string)

	// parse and resolve jumps
	for i := uint32(0); i < uint32(len(prog)); {
		inst, err := ParseOP(prog, i)
		if err != nil {
			return "", err
		}
		switch inst.Opcode {
		case OPJMPIF:
			addr := binary.LittleEndian.Uint32(inst.Data)
			if _, ok := labels[addr]; !ok {
				labelNum := len(labels)
				label := alphabet[labelNum%len(alphabet)]
				if labelNum >= len(alphabet) {
					label += fmt.Sprintf("%d", labelNum/len(alphabet)+1)
				}
				labels[addr] = label
			}
		}
		asm = append(asm, inst)
		i += inst.Length
	}

	var location uint32
	var tokens []string

	for _, inst := range asm {
		if label, ok := labels[location]; ok {
			tokens = append(tokens, "@"+label)
		}

		var token string
		switch inst.Opcode {
		case OPJMPIF:
			addr := binary.LittleEndian.Uint32(inst.Data)
			token = fmt.Sprintf("%s:@%s", inst.Opcode.String(), labels[addr])
		case OPFalse:
			token = "false"
		case OPTrue:
			token = "true"
		default:
			if len(inst.Data) > 0 && len(inst.Data) < 4 {
				num, err := leb128.ToInt64(inst.Data)
				if err != nil {
					token = fmt.Sprintf("NOP")
				}
				token = fmt.Sprintf("%d", num)
			} else if len(inst.Data) > 8 {
				token = fmt.Sprintf("0x%x", inst.Data)
			} else {
				token = inst.Opcode.String()
			}
		}
		tokens = append(tokens, token)

		location += inst.Length
	}

	if label, ok := labels[location]; ok {
		tokens = append(tokens, "@"+label)
	}

	return strings.Join(tokens, " "), nil
}

// alphabet defines a custom 3 word encoding for labels
var alphabet = []string{
	"ava", "bus", "cav", "dot", "elo", "fur", "gon", "hal",
	"ino", "jet", "kip", "lor", "mir", "nex", "osa", "pen",
	"que", "rol", "sol", "tyc", "uno", "vac", "wit", "xen",
	"yak", "zod",
}

// pushData builds OPPush|length|data as a way to encode bytearrays
// within bytecode
func pushData(data []byte) []byte {
	var buf = make([]byte, len(data)+2)
	buf[0] = byte(OPPush)
	buf[1] = byte(len(data))
	copy(buf[2:], data)
	return buf
}
