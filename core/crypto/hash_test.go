package crypto

import (
	"encoding/hex"
	"testing"

	"golang.org/x/crypto/sha3"

	"gitlab.com/cloq/go-cloq/pkg/leb128"
)

func BenchmarkHash(b *testing.B) {
	var i int
	var hashes = make([]Hash, b.N)
	for i = 0; i < b.N; i++ {
		var h Hash
		h = sha3.Sum256(leb128.FromUint64(uint64(i)))
		hashes[i] = h
	}
}

func TestTagged(t *testing.T) {

	tag := []byte{'w', 'i', 't'}
	data := []byte("helloworld")

	hash := sha3.New256()
	hash.Write(data)
	t.Log(hex.EncodeToString(hash.Sum(tag)))

	hash.Reset()
	hash.Write(tag)
	hash.Write(data)
	t.Log(hex.EncodeToString(hash.Sum(nil)))

	data2 := append(tag, data...)
	hash.Reset()
	hash.Write(data2)
	t.Log(hex.EncodeToString(hash.Sum(nil)))

}

func TestMarshal(t *testing.T) {

	h := SHA3Hash([]byte("helloworld"))

	var h1 Hash
	h1.SetBytes(h[:])
	if !h1.Equal(h) {
		t.Error("SetBytes should be consistent")
	}
	mHash, err := h.MarshalJSON()

	if err != nil {
		t.Error("failed to marshal hash to JSON correctly")
	}
	t.Log(string(mHash))
	var uHash Hash

	err = uHash.UnmarshalJSON(mHash)

	if err != nil {
		t.Error("failed to unmarshal json to hash type correctly")
	}
	t.Log(uHash)

	if !uHash.Equal(h) {
		t.Error("failed to correctly marshal hash")
	}
}
