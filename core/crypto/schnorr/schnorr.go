package schnorr

import (
	"crypto"
	"encoding/hex"
	"errors"

	"gitlab.com/cloq/go-cloq/pkg/schnorrkel"
)

const (
	// PublicKeySize is the length of a canonical encoded public key.
	PublicKeySize = 32
	// PrivateKeySize is the length of a canonical encoded private key.
	PrivateKeySize = 32
	// SignatureSize is the length of a canonical encoded signature.
	SignatureSize = 64
	// SeedSize is the length of an initialization seed.
	SeedSize = 32
)

// cloqSchnorrSigningContext is the context for signatures 
var cloqSchnorrSigningContext = []byte("cloq-qvm-sigctx")

// Keypair is a wrapper around schnorrkel public-private keypair
type Keypair struct {
	public  *PublicKey
	private *PrivateKey
}

// PublicKey represents a schnorrkel public key
type PublicKey struct {
	key *schnorrkel.PublicKey
}

// PrivateKey represents a shcnorrkel private key
type PrivateKey struct {
	key *schnorrkel.PrivateKey
}

// NewKeypair returns a schnorrkel Keypair given a schnorrkel secret key
func NewKeypair(priv *schnorrkel.PrivateKey) (*Keypair, error) {
	pub, err := priv.Public()
	if err != nil {
		return nil, err
	}

	return &Keypair{
		public:  &PublicKey{key: pub},
		private: &PrivateKey{key: priv},
	}, nil
}

// NewKeypairFromPrivate returns a schnorrkel Keypair given a *schnorrkel.PrivateKey
func NewKeypairFromPrivate(priv *PrivateKey) (*Keypair, error) {
	pub, err := priv.Public()
	if err != nil {
		return nil, err
	}

	return &Keypair{
		public:  pub.(*PublicKey),
		private: priv,
	}, nil
}

// NewKeypairFromSeed returns a new schnorrkel Keypair given a seed
func NewKeypairFromSeed(seed []byte) (*Keypair, error) {

	var buf [SeedSize]byte

	msc, err := schnorrkel.NewMiniSecretKeyFromRaw(buf)
	if err != nil {
		return nil, err
	}

	priv := msc.ExpandEd25519()
	pub := msc.Public()

	return &Keypair{
		public:  &PublicKey{key: pub},
		private: &PrivateKey{key: priv},
	}, nil
}

// NewPrivateKey creates a new private key using the input bytes
func NewPrivateKey(in []byte) (*PrivateKey, error) {
	if len(in) != PrivateKeySize {
		return nil, errors.New("input to create schnorrkel private key is not 32 bytes")
	}
	priv := new(PrivateKey)
	err := priv.Decode(in)
	return priv, err
}

// GenerateKeypair returns a new schnorrkel keypair
func GenerateKeypair() (*Keypair, error) {
	priv, pub, err := schnorrkel.GenerateKeypair()
	if err != nil {
		return nil, err
	}

	return &Keypair{
		public:  &PublicKey{key: pub},
		private: &PrivateKey{key: priv},
	}, nil
}

// NewPublicKey returns a schnorrkel public key from 32 byte input
func NewPublicKey(in []byte) (*PublicKey, error) {
	if len(in) != PublicKeySize {
		return nil, errors.New("cannot create public key: input is not 32 bytes")
	}

	buf := [PublicKeySize]byte{}
	copy(buf[:], in)
	return &PublicKey{key: schnorrkel.NewPublicKey(buf)}, nil
}

// Sign uses the keypair to sign the message using the schnorrkel signature algorithm
func (kp *Keypair) Sign(msg []byte) ([]byte, error) {
	return kp.private.Sign(msg)
}

// Public returns the public key corresponding to this keypair
func (kp *Keypair) Public() *PublicKey {
	return kp.public
}

// Private returns the private key corresponding to this keypair
func (kp *Keypair) Private() *PrivateKey {
	return kp.private
}

// Sign uses the private key to sign the message using the schnorrkel signature algorithm
func (k *PrivateKey) Sign(msg []byte) ([]byte, error) {
	if k.key == nil {
		return nil, errors.New("key is nil")
	}
	t := schnorrkel.NewSigningContext(cloqSchnorrSigningContext, msg)
	sig, err := schnorrkel.Sign(t, k.key)
	if err != nil {
		return nil, err
	}
	enc := sig.Encode()
	return enc[:], nil
}

// Public returns the public key corresponding to this private key
func (k *PrivateKey) Public() (crypto.PublicKey, error) {
	if k.key == nil {
		return nil, errors.New("key is nil")
	}
	pub, err := k.key.Public()
	if err != nil {
		return nil, err
	}
	return &PublicKey{key: pub}, nil
}

// Encode returns the 32-byte encoding of the private key
func (k *PrivateKey) Encode() []byte {
	if k.key == nil {
		return nil
	}
	enc := k.key.Encode()
	return enc[:]
}

// Decode decodes the input bytes into a private key and sets the receiver the decoded key
// Input must be 32 bytes, or else this function will error
func (k *PrivateKey) Decode(in []byte) error {
	if len(in) != PrivateKeySize {
		return errors.New("input to schnorrkel private key decode is not 32 bytes")
	}
	b := [PrivateKeySize]byte{}
	copy(b[:], in)
	k.key = &schnorrkel.PrivateKey{}
	return k.key.Decode(b)
}

// Verify uses the schnorrkel signature algorithm to verify that the message was signed by
// this public key; it returns true if this key created the signature for the message,
// false otherwise
func Verify(pub *PublicKey, msg, sig []byte) (bool, error) {
	if pub.key == nil {
		return false, errors.New("nil public key")
	}

	if len(sig) != SignatureSize {
		return false, errors.New("invalid signature length")
	}

	b := [SignatureSize]byte{}
	copy(b[:], sig)

	s := &schnorrkel.Signature{}
	err := s.Decode(b)
	if err != nil {
		return false, err
	}

	t := schnorrkel.NewSigningContext(cloqSchnorrSigningContext, msg)
	return schnorrkel.Verify(s, t, pub.key), nil
}

// Encode returns the 32-byte encoding of the public key
func (k *PublicKey) Encode() []byte {
	if k.key == nil {
		return nil
	}

	enc := k.key.Encode()
	return enc[:]
}

// Decode decodes the input bytes into a public key and sets the receiver the decoded key
// Input must be 32 bytes, or else this function will error
func (k *PublicKey) Decode(in []byte) error {
	if len(in) != PublicKeySize {
		return errors.New("input to schnorr public key decode must be 32 bytes")
	}
	b := [PublicKeySize]byte{}
	copy(b[:], in)
	k.key = &schnorrkel.PublicKey{}
	return k.key.Decode(b)
}

// String implements stringer with a 0x-prefix
func (k *PublicKey) String() string {
	enc := k.Encode()
	h := hex.EncodeToString(enc)
	return "0x" + h
}
