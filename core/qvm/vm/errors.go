package vm

import (
	"errors"
	"strings"
)

var (
	// ErrAltStackUnderflow is returned if the alt stack underflows
	ErrAltStackUnderflow = errors.New("alt stack underflow")
	// ErrBadEncoding is returned when a bad encoded value is found
	ErrBadEncoding = errors.New("bad encoded value")
	// ErrEvalStackUnderflow is returned if the data stack underflows
	ErrEvalStackUnderflow = errors.New("eval stack underflow")
	// ErrMaxDataSizeExceeded is returned when the argument to a push op too high
	ErrMaxDataSizeExceeded = errors.New("push op has exceeded max data size")
	// ErrDisallowedOpcode is returned when a disallowed opcode is detected
	ErrDisallowedOpcode = errors.New("disallowed opcode")
	// ErrDivZero is returned when a div by zero is detected
	ErrDivZero = errors.New("division by zero")
	// ErrFarJump is returned when a jump instruction's address is higher than
	// the program size.
	ErrFarJump = errors.New("jumping too far")
	// ErrHalt is returned when a halt execution is executed
	ErrHalt = errors.New("halt executed")
	// ErrNumOutOfRange is returned when a num ops is out of int64 range
	ErrNumOutOfRange = errors.New("numeric value out of int64 range")
	// ErrProgramIndexOutOfRange is returned when an index is out of current bounds
	ErrProgramIndexOutOfRange = errors.New("index out of range error")
	// ErrLongProgram is returned when program length is higher than max allowed
	ErrLongProgram = errors.New("program size exceeds max allowed size")
	// ErrMaxMemoryExceeded is returned when an operation's
	// cost exceeds the max memory.
	ErrMaxMemoryExceeded = errors.New("max memory exceeded")
	// ErrShortProgram is returned when a program is too short (0 byte are illegal)
	ErrShortProgram = errors.New("unexpected end of program")
	// ErrStop is returned when a program's execution stops
	ErrStop = errors.New("stop instruction executed")
	// ErrToken is returned when an unknown token is encoutred
	ErrToken = errors.New("unrecognized token")
	// ErrUnexpected is returned when an unexpected problem happens
	ErrUnexpected = errors.New("unexpected error")
	// ErrUnsupportedVM is returned when VM version is unsupported
	ErrUnsupportedVM = errors.New("unsupported VM")
	// ErrUndefinedLabel is returned when a disassembly label is unknown
	ErrUndefinedLabel = errors.New("undefined label for jump operation")
	// ErrVerifyFailed is returned when a verify op fails
	ErrVerifyFailed = errors.New("verify failed")
	// ErrBadPublicKey is returned when the public key size is different
	// from the specification.
	ErrBadPublicKey = errors.New("bad public key size")
	// ErrBadSignature is returned when the signature size is different
	// than the spec.
	ErrBadSignature = errors.New("bad signature")
	// ErrBadMessageSize is returned when a to be signed message size
	// is different than the spec.
	ErrBadMessageSize = errors.New("bad message size")
	// ErrMaxProgramSizeExceeded is returned when a program is larger than the max allowed size
	ErrMaxProgramSizeExceeded = errors.New("bad program : max program size exceeded")
	// ErrMaxStackItemSize is returned if we try to push an item to the stack longer than
	// the max allowed size
	ErrMaxStackItemSize = errors.New("max stack item size exceeded")
	// ErrMaxStackDepthExceeded is returned if we hit the stackd depth limit
	ErrMaxStackDepthExceeded = errors.New("stack depth limit exceeded")
)

func wrapError(err error, param string) error {

	e := strings.Join([]string{err.Error(), param}, ":")
	return errors.New(e)

}
