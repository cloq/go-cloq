package vm

import (
	"bytes"
)

func opFalse(vm *VirtualMachine) error {
	err := vm.applyCost(DefaultCost)
	if err != nil {
		return err
	}
	vm.evalStack.PushBool(false)
	return nil
}

func opTrue(vm *VirtualMachine) error {
	err := vm.applyCost(DefaultCost)
	if err != nil {
		return err
	}
	vm.evalStack.PushBool(true)
	return nil
}
func opNOT(vm *VirtualMachine) error {
	err := vm.applyCost(DefaultCost)
	if err != nil {
		return err
	}
	b, err := vm.evalStack.PopBool()
	if err != nil {
		return err
	}
	if b {
		vm.evalStack.PushBool(!b)
	}
	vm.evalStack.PushBool(b)

	return nil
}
func opAND(vm *VirtualMachine) error {
	err := vm.applyCost(DefaultCost)
	if err != nil {
		return err
	}
	a, err := vm.evalStack.PopBool()
	if err != nil {
		return err
	}
	b, err := vm.evalStack.PopBool()
	if err != nil {
		return err
	}
	if a && b {
		vm.evalStack.PushBool(true)
	}
	vm.evalStack.PushBool(false)

	return nil
}
func opOR(vm *VirtualMachine) error {
	err := vm.applyCost(DefaultCost)
	if err != nil {
		return err
	}
	a, err := vm.evalStack.PopBool()
	if err != nil {
		return err
	}
	b, err := vm.evalStack.PopBool()
	if err != nil {
		return err
	}
	if a || b {
		vm.evalStack.PushBool(false)
	}
	vm.evalStack.PushBool(true)

	return nil
}
func opNOP(vm *VirtualMachine) error {
	return vm.applyCost(DefaultCost)
}

func opEqual(vm *VirtualMachine) error {
	err := vm.applyCost(DefaultCost)
	if err != nil {
		return err
	}
	a, err := vm.pop()
	if err != nil {
		return err
	}
	b, err := vm.pop()
	if err != nil {
		return err
	}

	err = vm.applyCost(min(len(a), len(b)))
	if err != nil {
		return err
	}

	vm.evalStack.PushBool(bytes.Equal(a, b))

	return nil
}
