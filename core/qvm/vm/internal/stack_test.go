package stack

import (
	"bytes"
	"errors"
	"fmt"
	"testing"
)

// TestStack tests that all of the stack operands work as expected.
func TestStack(t *testing.T) {
	t.Parallel()
	type testCase struct {
		name    string
		before  [][]byte
		operand func(*Stack) error
		err     error
		after   [][]byte
	}
	cases := []testCase{
		{
			"No Op",
			[][]byte{{1}, {2}, {3}, {4}, {5}},
			func(s *Stack) error {
				return nil
			},
			nil,
			[][]byte{{1}, {2}, {3}, {4}, {5}},
		},
		{
			"Peek underflow (byte)",
			[][]byte{{1}, {2}, {3}, {4}, {5}},
			func(s *Stack) error {
				_, err := s.PeekBytes(5)
				return err
			},
			ErrStackIndexOutOfRange,
			[][]byte{{1}, {2}, {3}, {4}, {5}},
		},
		{
			"Peek underflow (int)",
			[][]byte{{1}, {2}, {3}, {4}, {5}},
			func(s *Stack) error {
				_, err := s.PeekInt(5)
				return err
			},
			ErrStackIndexOutOfRange,
			[][]byte{{1}, {2}, {3}, {4}, {5}},
		},
		{
			"Peek underflow (bool)",
			[][]byte{{1}, {2}, {3}, {4}, {5}},
			func(s *Stack) error {
				_, err := s.PeekBool(5)
				return err
			},
			ErrStackIndexOutOfRange,
			[][]byte{{1}, {2}, {3}, {4}, {5}},
		},
		{
			"pop",
			[][]byte{{1}, {2}, {3}, {4}, {5}},
			func(s *Stack) error {
				val, err := s.PopBytes()
				if err != nil {
					return err
				}
				if !bytes.Equal(val, []byte{5}) {
					return errors.New("not equal")
				}
				return err
			},
			nil,
			[][]byte{{1}, {2}, {3}, {4}},
		},
		{
			"pop everything",
			[][]byte{{1}, {2}, {3}, {4}, {5}},
			func(s *Stack) error {
				for i := 0; i < 5; i++ {
					_, err := s.PopBytes()
					if err != nil {
						return err
					}
				}
				return nil
			},
			nil,
			nil,
		},
		{
			"pop underflow",
			[][]byte{{1}, {2}, {3}, {4}, {5}},
			func(s *Stack) error {
				for i := 0; i < 6; i++ {
					_, err := s.PopBytes()
					if err != nil {
						return err
					}
				}
				return nil
			},
			ErrStackIndexOutOfRange,
			nil,
		},
		{
			"pop bool",
			[][]byte{nil},
			func(s *Stack) error {
				val, err := s.PopBool()
				if err != nil {
					return err
				}

				if val {
					return errors.New("unexpected value")
				}
				return nil
			},
			nil,
			nil,
		},
		{
			"pop bool",
			[][]byte{{1}},
			func(s *Stack) error {
				val, err := s.PopBool()
				if err != nil {
					return err
				}

				if !val {
					return errors.New("unexpected value")
				}
				return nil
			},
			nil,
			nil,
		},
		{
			"pop bool",
			nil,
			func(s *Stack) error {
				_, err := s.PopBool()
				return err
			},
			ErrStackIndexOutOfRange,
			nil,
		},
		{
			"non-minimal popInt 0",
			[][]byte{{0x0}},
			func(s *Stack) error {
				_, err := s.PopInt()
				return err
			},
			nil,
			nil,
		},
		{
			"popInt 1",
			[][]byte{{0x01}},
			func(s *Stack) error {
				v, err := s.PopInt()
				if err != nil {
					return err
				}
				if v != 1 {
					return errors.New("1 != 1 on PopInt")
				}
				return nil
			},
			nil,
			nil,
		},
		{
			"non-minimal popInt 1 leading 0",
			[][]byte{{0x01, 0x00, 0x00, 0x00}},
			func(s *Stack) error {
				_, err := s.PopInt()
				return err
			},
			nil,
			nil,
		},
		{
			"popInt 5 byte",
			[][]byte{{0xff, 0xff, 0xff, 0xff, 0x7f}},
			func(s *Stack) error {
				v, err := s.PopInt()
				if err != nil {
					return err
				}
				if v != 34359738367 {
					return fmt.Errorf("%v != 34359738367 on PopInt", v)
				}
				return nil
			},
			nil,
			nil,
		},
		{
			"non-minimal popInt 5-byte leading 0",
			[][]byte{{0xff, 0xff, 0xff, 0x7f, 0x00}},
			func(s *Stack) error {
				x, err := s.PopInt()
				if x != 2147483647 {
					return ErrBadEncodingForInt64
				}
				return err
			},
			ErrBadEncodingForInt64,
			nil,
		},
		{
			"popInt 5-byte leading 0",
			[][]byte{{0xff, 0xff, 0xff, 0xff, 0x7f, 0x00}},
			func(s *Stack) error {
				x, err := s.PopInt()
				if x != 34359738367 {
					return ErrBadEncodingForInt64
				}
				return err
			},
			nil,
			nil,
		},
		{
			"PushInt 0",
			nil,
			func(s *Stack) error {
				s.PushInt(0)
				return nil
			},
			nil,
			[][]byte{{0}},
		},
		{
			"PushInt 1",
			nil,
			func(s *Stack) error {
				s.PushInt(1)
				return nil
			},
			nil,
			[][]byte{{0x1}},
		},
		{
			"PushInt -1",
			nil,
			func(s *Stack) error {
				s.PushInt(-1)
				return nil
			},
			nil,
			[][]byte{{0x7f}},
		},
		{
			"PushInt two bytes",
			nil,
			func(s *Stack) error {
				s.PushInt(256)
				return nil
			},
			nil,
			[][]byte{{0x80, 0x02}},
		},
		{
			"PushInt leading zeros",
			nil,
			func(s *Stack) error {
				s.PushInt(128)
				return nil
			},
			nil,
			[][]byte{{0x80, 0x01}},
		},
		{
			"dup",
			[][]byte{{1}},
			func(s *Stack) error {
				return s.DupN(1)
			},
			nil,
			[][]byte{{1}, {1}},
		},
		{
			"dup2",
			[][]byte{{1}, {2}},
			func(s *Stack) error {
				return s.DupN(2)
			},
			nil,
			[][]byte{{1}, {2}, {1}, {2}},
		},
		{
			"dup3",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				return s.DupN(3)
			},
			nil,
			[][]byte{{1}, {2}, {3}, {1}, {2}, {3}},
		},
		{
			"dup0",
			[][]byte{{1}},
			func(s *Stack) error {
				return s.DupN(0)
			},
			ErrStackDupBadArgument,
			nil,
		},
		{
			"dup-1",
			[][]byte{{1}},
			func(s *Stack) error {
				return s.DupN(-1)
			},
			ErrStackDupBadArgument,
			nil,
		},
		{
			"dup too much",
			[][]byte{{1}},
			func(s *Stack) error {
				return s.DupN(2)
			},
			ErrStackDupBadArgument,
			nil,
		},
		{
			"PushBool true",
			nil,
			func(s *Stack) error {
				s.PushBool(true)

				return nil
			},
			nil,
			[][]byte{{1}},
		},
		{
			"PushBool false",
			nil,
			func(s *Stack) error {
				s.PushBool(false)

				return nil
			},
			nil,
			[][]byte{{0}},
		},
		{
			"PushBool PopBool",
			nil,
			func(s *Stack) error {
				s.PushBool(true)
				val, err := s.PopBool()
				if err != nil {
					return err
				}
				if !val {
					return errors.New("unexpected value")
				}

				return nil
			},
			nil,
			nil,
		},
		{
			"PushBool PopBool 2",
			nil,
			func(s *Stack) error {
				s.PushBool(false)
				val, err := s.PopBool()
				if err != nil {
					return err
				}
				if val {
					return errors.New("unexpected value")
				}

				return nil
			},
			nil,
			nil,
		},
		{
			"PushInt PopBool",
			nil,
			func(s *Stack) error {
				s.PushInt(1)
				val, err := s.PopBool()
				if err != nil {
					return err
				}
				if !val {
					return errors.New("unexpected value")
				}

				return nil
			},
			nil,
			nil,
		},
		{
			"PushInt PopBool 2",
			nil,
			func(s *Stack) error {
				s.PushInt(0)
				val, err := s.PopBool()
				if err != nil {
					return err
				}
				if val {
					return errors.New("unexpected value")
				}

				return nil
			},
			nil,
			nil,
		},
		{
			"Nip top",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				return s.NipN(0)
			},
			nil,
			[][]byte{{1}, {2}},
		},
		{
			"Nip middle",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				return s.NipN(1)
			},
			nil,
			[][]byte{{1}, {3}},
		},
		{
			"Nip low",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				return s.NipN(2)
			},
			nil,
			[][]byte{{2}, {3}},
		},
		{
			"Nip too much",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				// bite off more than we can chew
				return s.NipN(3)
			},
			ErrStackIndexOutOfRange,
			[][]byte{{2}, {3}},
		},
		{
			"keep on tucking",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				return s.Tuck()
			},
			nil,
			[][]byte{{1}, {3}, {2}, {3}},
		},
		{
			"a little tucked up",
			[][]byte{{1}}, // too few arguments for tuck
			func(s *Stack) error {
				return s.Tuck()
			},
			ErrStackInsufficientItems,
			nil,
		},
		{
			"all tucked up",
			nil, // too few arguments for tuck
			func(s *Stack) error {
				return s.Tuck()
			},
			ErrStackInsufficientItems,
			nil,
		},
		{
			"drop 1",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.DropN(1)
			},
			nil,
			[][]byte{{1}, {2}, {3}},
		},
		{
			"drop 2",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.DropN(2)
			},
			nil,
			[][]byte{{1}, {2}},
		},
		{
			"drop 3",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.DropN(3)
			},
			nil,
			[][]byte{{1}},
		},
		{
			"drop 4",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.DropN(4)
			},
			nil,
			nil,
		},
		{
			"drop 4/5",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.DropN(5)
			},
			ErrStackDropBadArgument,
			nil,
		},
		{
			"drop invalid",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.DropN(0)
			},
			ErrStackDropBadArgument,
			nil,
		},
		{
			"Rot1",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.RotN(1)
			},
			nil,
			[][]byte{{1}, {3}, {4}, {2}},
		},
		{
			"Rot2",
			[][]byte{{1}, {2}, {3}, {4}, {5}, {6}},
			func(s *Stack) error {
				return s.RotN(2)
			},
			nil,
			[][]byte{{3}, {4}, {5}, {6}, {1}, {2}},
		},
		{
			"Rot too little",
			[][]byte{{1}, {2}},
			func(s *Stack) error {
				return s.RotN(1)
			},
			ErrStackRotBadArgument,
			nil,
		},
		{
			"Rot0",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				return s.RotN(0)
			},
			ErrStackRotBadArgument,
			[][]byte{{1}, {2}, {3}},
		},
		{
			"Swap1",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.SwapN(1)
			},
			nil,
			[][]byte{{1}, {2}, {4}, {3}},
		},
		{
			"Swap2",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.SwapN(2)
			},
			nil,
			[][]byte{{3}, {4}, {1}, {2}},
		},
		{
			"Swap too little",
			[][]byte{{1}},
			func(s *Stack) error {
				return s.SwapN(1)
			},
			ErrStackSwapBadArgument,
			nil,
		},
		{
			"Swap0",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				return s.SwapN(0)
			},
			ErrStackSwapBadArgument,
			nil,
		},
		{
			"Over1",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.OverN(1)
			},
			nil,
			[][]byte{{1}, {2}, {3}, {4}, {3}},
		},
		{
			"Over2",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.OverN(2)
			},
			nil,
			[][]byte{{1}, {2}, {3}, {4}, {1}, {2}},
		},
		{
			"Over too little",
			[][]byte{{1}},
			func(s *Stack) error {
				return s.OverN(1)
			},
			ErrStackOverBadArgument,
			nil,
		},
		{
			"Over0",
			[][]byte{{1}, {2}, {3}},
			func(s *Stack) error {
				return s.OverN(0)
			},
			ErrStackOverBadArgument,
			nil,
		},
		{
			"Pick1",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.PickN(1)
			},
			nil,
			[][]byte{{1}, {2}, {3}, {4}, {3}},
		},
		{
			"Pick2",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.PickN(2)
			},
			nil,
			[][]byte{{1}, {2}, {3}, {4}, {2}},
		},
		{
			"Pick too little",
			[][]byte{{1}},
			func(s *Stack) error {
				return s.PickN(1)
			},
			ErrStackIndexOutOfRange,
			nil,
		},
		{
			"Roll1",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.RollN(1)
			},
			nil,
			[][]byte{{1}, {2}, {4}, {3}},
		},
		{
			"Roll2",
			[][]byte{{1}, {2}, {3}, {4}},
			func(s *Stack) error {
				return s.RollN(2)
			},
			nil,
			[][]byte{{1}, {3}, {4}, {2}},
		},
		{
			"Roll too little",
			[][]byte{{1}},
			func(s *Stack) error {
				return s.RollN(1)
			},
			ErrStackRollBadArgument,
			nil,
		},
		{
			"Peek bool",
			[][]byte{{1}},
			func(s *Stack) error {
				// Peek bool is otherwise pretty well tested,
				// just check it works.
				val, err := s.PeekBool(0)
				if err != nil {
					return err
				}
				if !val {
					return errors.New("invalid result")
				}
				return nil
			},
			nil,
			[][]byte{{1}},
		},
		{
			"Peek bool 2",
			[][]byte{nil},
			func(s *Stack) error {
				// Peek bool is otherwise pretty well tested,
				// just check it works.
				val, err := s.PeekBool(0)
				if err != nil {
					return err
				}
				if val {
					return errors.New("invalid result")
				}
				return nil
			},
			nil,
			[][]byte{nil},
		},
		{
			"Peek int",
			[][]byte{{1}},
			func(s *Stack) error {
				// Peek int is otherwise pretty well tested,
				// just check it works.
				val, err := s.PeekInt(0)
				if err != nil {
					return err
				}
				if val != 1 {
					return errors.New("invalid result")
				}
				return nil
			},
			nil,
			[][]byte{{1}},
		},
		{
			"PeekInt 5 byte",
			[][]byte{{0xff, 0xff, 0xff, 0xff, 0x7f}, nil},
			func(s *Stack) error {
				v, err := s.PeekInt(1)
				if err != nil {
					return err
				}
				if v != 34359738367 {
					return fmt.Errorf("%v != 34359738367 on PeekInt", v)
				}
				return nil
			},
			nil,
			[][]byte{{0xff, 0xff, 0xff, 0xff, 0x7f}, nil},
		},
		{
			"non-minimal peekInt 5-byte leading 0",
			[][]byte{{0xff, 0xff, 0xff, 0x7f}, nil},
			func(s *Stack) error {
				_, err := s.PeekInt(1)
				return err
			},
			nil,
			[][]byte{{0xff, 0xff, 0xff, 0x7f}, nil},
		},
		{
			"too big peekInt 5-byte leading 0",
			[][]byte{{0xff, 0xff, 0xff, 0xff, 0x7f}, nil},
			func(s *Stack) error {
				_, err := s.PeekInt(1)
				return err
			},
			nil,
			[][]byte{{0xff, 0xff, 0xff, 0xff, 0x7f}, nil},
		},
		{
			"pop int",
			nil,
			func(s *Stack) error {
				s.PushInt(1)
				val, err := s.PopInt()
				if err != nil {
					return err
				}
				if val != 1 {
					return errors.New("invalid result")
				}
				return nil
			},
			nil,
			nil,
		},
		{
			"pop empty",
			nil,
			func(s *Stack) error {
				_, err := s.PopInt()
				return err
			},
			ErrStackIndexOutOfRange,
			nil,
		},
	}

	for _, test := range cases {
		// Setup the initial stack state and perform the test operand.
		s := Stack{}
		for i := range test.before {
			//noinspection GoUnhandledErrorResult
			s.PushBytes(test.before[i])
		}
		err := test.operand(&s)

		if err != test.err {
			t.Logf("%v : bad error value returned expected %v got %v", test.name, test.err, err)
			continue
		}

		if err != nil {
			t.Logf("expected error : %s", err)
			continue
		}

		// Ensure the resulting stack is the expected length.
		if int32(len(test.after)) != s.Depth() {
			t.Errorf("%s: stack depth doesn't match expected: %v "+
				"vs %v", test.name, len(test.after),
				s.Depth())
			continue
		}

		// Ensure all items of the resulting stack are the expected
		// values.
		for i := range test.after {
			val, err := s.PeekBytes(s.Depth() - int32(i) - 1)
			if err != nil {
				t.Errorf("%s: can't peek %dth stack entry: %v",
					test.name, i, err)
				break
			}

			if !bytes.Equal(val, test.after[i]) {
				t.Errorf("%s: %dth stack entry doesn't match "+
					"expected: %v vs %v", test.name, i, val,
					test.after[i])
				break
			}
		}
	}
}
