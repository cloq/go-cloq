package stack

import (
	"bytes"
	"encoding/hex"
	"errors"
)

var (
	// ErrBadEncodingForInt64 is returned when a bytearray
	//  can't be decoded to a valid int64
	ErrBadEncodingForInt64 = errors.New("bad encoded int64")
	// ErrStackIndexOutOfRange is returned when an index doesn't fall within bounds
	// less than 0 or greater than stack depth.
	ErrStackIndexOutOfRange = errors.New("stack index is out of range")
	// ErrStackInsufficientItems is returned when stack items are insufficient
	// for an operation to run
	ErrStackInsufficientItems = errors.New("insufficient items on the stack for operand")
	// ErrStackDropBadArgument is returned when an Drop is executed wih insufficient items on the stack
	// Example : Drop(0) will drop 0 items thus it's a bad argument
	ErrStackDropBadArgument = errors.New("insufficient items on the stack for Drop operation")
	// ErrStackRotBadArgument is returned when a Rot is executed with insufficient items on the stack
	ErrStackRotBadArgument = errors.New("insufficient items on the stack for Rot operation")
	// ErrStackDupBadArgument is returned when a Dup is executed with insufficient items on the stack
	ErrStackDupBadArgument = errors.New("insufficient items on the stack for Dup operation")
	// ErrStackSwapBadArgument is returned when a Swap is executed with insufficient items on the stack
	ErrStackSwapBadArgument = errors.New("insufficient items on the stack for Swap operation")
	// ErrStackRollBadArgument is returned when a Roll is executed with insufficient items on the stack
	ErrStackRollBadArgument = errors.New("insufficient items on the stack for Roll operation")
	// ErrStackOverBadArgument is returned when an Over is executed with insufficient items on the stack
	ErrStackOverBadArgument = errors.New("insufficient items on the stack for Over operation")
)

// Stack is a 2 dim byte slice
// The top of the stack is indexed at 0
type Stack struct {
	stk [][]byte
}

// New returns a new stack instance.
func New(data [][]byte) Stack {
	var s Stack
	s.stk = append(s.stk, data...)
	return s
}

// Depth returns the number of items on the stack.
func (s *Stack) Depth() int32 {
	return int32(len(s.stk))
}

// Top returns the element at the top without removing it
func (s *Stack) Top() ([]byte, error) {
	return s.stk[len(s.stk)-1], nil
}

// PushBytes adds the given  bytearray to the top of the stack.
//
// Stack diagram: [... x1 x2] -> [... x1 x2 data]
func (s *Stack) PushBytes(data []byte) error {
	s.stk = append(s.stk, data)
	return nil
}

// PushInt converts an int64 to a bytearray and pushes it to the stack
//
// Stack diagram: [... x1 x2] -> [... x1 x2 val]
func (s *Stack) PushInt(val int64) {

	b := FromInt64(val)
	s.PushBytes(b)
}

// PushBool converts a boolean to a byte array then pushes
// it onto the top of the stack.
//
// Stack diagram: [... x1 x2] -> [... x1 x2 bool]
func (s *Stack) PushBool(val bool) {
	s.PushBytes(FromBool(val))
}

// PopBytes pops the value off the top of the stack and returns it.
//
// Stack diagram: [... x1 x2 x3] -> [... x1 x2]
func (s *Stack) PopBytes() ([]byte, error) {
	return s.nipN(0)
}

// PopInt pops the value off the top of the stack, converts it to int64
// and returns it.
//
// Stack diagram: [... x1 x2 x3] -> [... x1 x2]
func (s *Stack) PopInt() (int64, error) {
	b, err := s.PopBytes()
	if err != nil {
		return 0, err
	}

	return ToInt64(b)
}

// PopBool pops the value off the top of the stack, converts it into a bool, and
// returns it.
//
// Stack diagram: [... x1 x2 x3] -> [... x1 x2]
func (s *Stack) PopBool() (bool, error) {
	b, err := s.PopBytes()
	if err != nil {
		return false, err
	}

	return ToBool(b), nil
}

// PeekBytes returns the Nth item on the stack without removing it.
func (s *Stack) PeekBytes(idx int32) ([]byte, error) {
	sz := int32(len(s.stk))
	if idx < 0 || idx >= sz {
		return nil, ErrStackIndexOutOfRange
	}

	return s.stk[sz-idx-1], nil
}

// PeekInt returns the Nth item on the stack as a script num without removing
// it.  The act of converting to a script num enforces the consensus rules
// imposed on data interpreted as numbers.
func (s *Stack) PeekInt(idx int32) (int64, error) {
	b, err := s.PeekBytes(idx)
	if err != nil {
		return 0, err
	}

	return ToInt64(b)
}

// PeekBool returns the Nth item on the stack as a bool without removing it.
func (s *Stack) PeekBool(idx int32) (bool, error) {
	b, err := s.PeekBytes(idx)
	if err != nil {
		return false, err
	}

	return ToBool(b), nil
}

// nipN is a function that removes the nth item on the stack and
// returns it.
//
// Stack diagram:
// nipN(0): [... x1 x2 x3] -> [... x1 x2]
// nipN(1): [... x1 x2 x3] -> [... x1 x3]
// nipN(2): [... x1 x2 x3] -> [... x2 x3]
func (s *Stack) nipN(idx int32) ([]byte, error) {
	sz := int32(len(s.stk))
	if idx < 0 || idx > sz-1 {
		return nil, ErrStackIndexOutOfRange
	}

	elem := s.stk[sz-idx-1]
	if idx == 0 {
		s.stk = s.stk[:sz-1]
	} else if idx == sz-1 {
		s.stk = s.stk[1:]
	} else {
		s1 := s.stk[sz-idx : sz]
		s.stk = s.stk[:sz-idx-1]
		s.stk = append(s.stk, s1...)
	}
	return elem, nil
}

// NipN removes the Nth object on the stack
//
// Stack diagram:
// NipN(0): [... x1 x2 x3] -> [... x1 x2]
// NipN(1): [... x1 x2 x3] -> [... x1 x3]
// NipN(2): [... x1 x2 x3] -> [... x2 x3]
func (s *Stack) NipN(idx int32) error {
	_, err := s.nipN(idx)
	return err
}

// Tuck copies the item at the top of the stack and inserts it before the 2nd
// to top item.
//
// Stack diagram: [... x1 x2] -> [... x2 x1 x2]
func (s *Stack) Tuck() error {
	top, err := s.PopBytes()
	if err != nil {
		return ErrStackInsufficientItems
	}
	below, err := s.PopBytes()
	if err != nil {
		return ErrStackInsufficientItems
	}
	s.PushBytes(top)   // stack [... x2]
	s.PushBytes(below) // stack [... x2 x1]
	s.PushBytes(top)   // stack [... x2 x1 x2]

	return nil
}

// DropN removes the top N items from the stack.
//
// Stack diagram:
// DropN(1): [... x1 x2] -> [... x1]
// DropN(2): [... x1 x2] -> [...]
func (s *Stack) DropN(n int32) error {
	if n < 1 {
		return ErrStackDropBadArgument
	}

	for ; n > 0; n-- {
		_, err := s.PopBytes()
		if err != nil {
			return ErrStackDropBadArgument
		}
	}
	return nil
}

// DupN duplicates the top N items on the stack.
//
// Stack diagram:
// DupN(1): [... x1 x2] -> [... x1 x2 x2]
// DupN(2): [... x1 x2] -> [... x1 x2 x1 x2]
func (s *Stack) DupN(n int32) error {
	if n < 1 {
		return ErrStackDupBadArgument
	}

	// Iteratively duplicate the value n-1 down the stack n times.
	// This leaves an in-order duplicate of the top n items on the stack.
	for i := n; i > 0; i-- {
		b, err := s.PeekBytes(n - 1)
		if err != nil {
			return ErrStackDupBadArgument
		}
		s.PushBytes(b)
	}
	return nil
}

// RotN rotates the top 3N items on the stack to the left N times.
//
// Stack diagram:
// RotN(1): [... x1 x2 x3] -> [... x2 x3 x1]
// RotN(2): [... x1 x2 x3 x4 x5 x6] -> [... x3 x4 x5 x6 x1 x2]
func (s *Stack) RotN(n int32) error {
	if n < 1 {
		return ErrStackRotBadArgument
	}

	// Nip the 3n-1th item from the stack to the top n times to rotate
	// them up to the head of the stack.
	entry := 3*n - 1
	for i := n; i > 0; i-- {
		b, err := s.nipN(entry)
		if err != nil {
			return ErrStackRotBadArgument
		}

		s.PushBytes(b)
	}
	return nil
}

// SwapN swaps the top N items on the stack with those below them.
//
// Stack diagram:
// SwapN(1): [... x1 x2] -> [... x2 x1]
// SwapN(2): [... x1 x2 x3 x4] -> [... x3 x4 x1 x2]
func (s *Stack) SwapN(n int32) error {
	if n < 1 {
		return ErrStackSwapBadArgument
	}

	entry := 2*n - 1
	for i := n; i > 0; i-- {
		// Swap 2n-1th entry to top.
		b, err := s.nipN(entry)
		if err != nil {
			return ErrStackSwapBadArgument
		}

		s.PushBytes(b)
	}
	return nil
}

// OverN copies N items from the bottom to the top of the stack.
//
// Stack diagram:
// OverN(1): [... x1 x2 x3] -> [... x1 x2 x3 x2]
// OverN(2): [... x1 x2 x3 x4] -> [... x1 x2 x3 x4 x1 x2]
func (s *Stack) OverN(n int32) error {
	if n < 1 {
		return ErrStackOverBadArgument
	}

	// Copy 2n-1th entry to top of the stack.
	entry := 2*n - 1
	for ; n > 0; n-- {
		b, err := s.PeekBytes(entry)
		if err != nil {
			return ErrStackOverBadArgument
		}
		s.PushBytes(b)
	}

	return nil
}

// PickN copies the item at index N to the top.
//
// Stack diagram:
// PickN(0): [x1 x2 x3] -> [x1 x2 x3 x3]
// PickN(1): [x1 x2 x3] -> [x1 x2 x3 x2]
// PickN(2): [x1 x2 x3] -> [x1 x2 x3 x1]
func (s *Stack) PickN(n int32) error {
	b, err := s.PeekBytes(n)
	if err != nil {
		return err
	}
	s.PushBytes(b)

	return nil
}

// RollN moves the item at index N to the top.
//
// Stack diagram:
// RollN(0): [x1 x2 x3] -> [x1 x2 x3]
// RollN(1): [x1 x2 x3] -> [x1 x3 x2]
// RollN(2): [x1 x2 x3] -> [x2 x3 x1]
func (s *Stack) RollN(n int32) error {
	b, err := s.nipN(n)
	if err != nil {
		return ErrStackRollBadArgument
	}

	s.PushBytes(b)

	return nil
}

// View returns a copy of the stack items from start to end.
func (s *Stack) View(start, end int) Stack {
	var sc Stack
	if end == 0 {
		copy(sc.stk, s.stk[start:])
	}
	copy(sc.stk, s.stk[start:end])

	return sc
}

// Cost returns the item cost of the whole stack
func (s *Stack) Cost() int32 {
	result := 8 * s.Depth()
	for _, item := range s.stk {
		result += int32(len(item))
	}
	return result
}

// String returns the stack in a readable format.
func (s *Stack) String() string {
	var result string
	for _, elem := range s.stk {
		if len(elem) == 0 {
			result += "<empty>\n"
		}
		result += hex.Dump(elem)
	}

	return result
}

// Equal compares two stack
func (s *Stack) Equal(s2 Stack) bool {

	if s.stk == nil && s2.stk == nil {
		return true
	}
	if s.Depth() != s2.Depth() {
		return false
	}

	for idx, b := range s.stk {
		if !bytes.Equal(b, s2.stk[idx]) {
			return false
		}
	}
	return true
}
