package vm

import (
	"reflect"
	"testing"
)

func TestFromToInt64(t *testing.T) {
	tests := []struct {
		n    int64
		want []byte
	}{
		{549755813887, []byte{0xff, 0xff, 0xff, 0xff, 0xff, 0x0f}},
		{128, []byte{0x80, 0x01}},
		{-1, []byte{0x7f}},
	}
	for _, tt := range tests {
		t.Run("TestEncode", func(t *testing.T) {
			if got := FromInt64(tt.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FromInt64(%v) = %0x, want %0x", tt.n, got, tt.want)
			}
		})
	}
	for _, tt := range tests {
		t.Run("TestDecode", func(t *testing.T) {
			if got, err := ToInt64(tt.want); err != nil || !reflect.DeepEqual(got, tt.n) {
				t.Errorf("ToInt64(%v) = %0x, want %0x", tt.want, got, tt.n)
			}
		})
	}
}

func TestFromToUint64(t *testing.T) {
	tests := []struct {
		n    uint64
		want []byte
	}{
		{0, []byte{0x00}},
		{1, []byte{0x01}},
		{2, []byte{0x02}},
		{3, []byte{0x03}},
		{4, []byte{0x04}},
		{5, []byte{0x05}},
		{63, []byte{0x3F}},
		{64, []byte{0x40}},
		{65, []byte{0x41}},
		{100, []byte{0x64}},
		{127, []byte{0x7F}},
		{128, []byte{0x80, 0x01}},
		{129, []byte{0x81, 0x01}},
		{2141192192, []byte{0x80, 0x80, 0x80, 0xFD, 0x07}},
		{^uint64(0), []byte{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x01}},
	}
	for _, tt := range tests {
		t.Run("TestEncode", func(t *testing.T) {
			got := FromUint64(tt.n)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FromUint64(%v) = %0x, want %0x", tt.n, got, tt.want)
			}
		})
	}
	for _, tt := range tests {
		t.Run("TestDecode", func(t *testing.T) {
			if got, err := ToUint64(tt.want); err != nil || !reflect.DeepEqual(got, tt.n) {
				t.Errorf("ToUint64(%v) = %0x, want %0x", tt.want, got, tt.n)
			}
		})
	}
}

func TestBool(t *testing.T) {
	t.Log(ToBool([]byte{0}))
	t.Log(ToBool([]byte{1}))
	t.Log(FromBool(false))
	t.Log(FromBool(true))
	t.Log(ToBool(nil))

}
