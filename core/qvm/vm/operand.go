package vm

// implementation of stack operand callbacks

func opToAltStack(vm *VirtualMachine) error {
	err := vm.applyCost(2)
	if err != nil {
		return err
	}

	top, err := vm.evalStack.PopBytes()
	if err != nil {
		return err
	}
	vm.altStack.PushBytes(top)
	return nil
}

func opFromAltStack(vm *VirtualMachine) error {
	err := vm.applyCost(2)
	if err != nil {
		return err
	}

	top, err := vm.altStack.PopBytes()
	if err != nil {
		return err
	}
	vm.evalStack.PushBytes(top)
	return nil
}

func opOver(vm *VirtualMachine) error {
	err := vm.applyCost(1)
	if err != nil {
		return err
	}
	return vm.evalStack.OverN(1)
}

func op2Over(vm *VirtualMachine) error {
	err := vm.applyCost(2)
	if err != nil {
		return err
	}
	return vm.evalStack.OverN(2)
}
func opDup(vm *VirtualMachine) error {

	err := vm.applyCost(1)
	if err != nil {
		return err
	}
	return vm.evalStack.DupN(1)
}
func op2Dup(vm *VirtualMachine) error {

	err := vm.applyCost(2)
	if err != nil {
		return err
	}
	return vm.evalStack.DupN(2)
}
func opDrop(vm *VirtualMachine) error {

	err := vm.applyCost(1)
	if err != nil {
		return err
	}
	return vm.evalStack.DropN(1)
}

func op2Drop(vm *VirtualMachine) error {

	err := vm.applyCost(2)
	if err != nil {
		return err
	}
	return vm.evalStack.DropN(2)
}

func opSwap(vm *VirtualMachine) error {
	err := vm.applyCost(1)
	if err != nil {
		return err
	}
	return vm.evalStack.SwapN(1)
}
func op2Swap(vm *VirtualMachine) error {
	err := vm.applyCost(2)
	if err != nil {
		return err
	}
	return vm.evalStack.SwapN(2)
}
func opDepth(vm *VirtualMachine) error {
	err := vm.applyCost(1)
	if err != nil {
		return err
	}

	vm.evalStack.PushInt(int64(vm.evalStack.Depth()))
	return nil
}
func opPick(vm *VirtualMachine) error {
	err := vm.applyCost(2)
	if err != nil {
		return err
	}
	n, err := vm.popInt()
	if err != nil {
		return err
	}
	if n < 0 || n > int64(vm.evalStack.Depth()) {
		return ErrEvalStackUnderflow
	}

	return vm.evalStack.PickN(int32(n))

}

func opRoll(vm *VirtualMachine) error {
	err := vm.applyCost(2)
	if err != nil {
		return err
	}
	n, err := vm.popInt()
	if err != nil {
		return err
	}
	if n < 0 || n > int64(vm.evalStack.Depth()) {
		return ErrEvalStackUnderflow
	}

	return vm.evalStack.RollN(int32(n))
}
