package vm

import (
	"testing"

	stack "gitlab.com/cloq/go-cloq/core/qvm/vm/internal"
)

func TestStackOps(t *testing.T) {
	t.Log(opsByName)
	type testStruct struct {
		op      OP
		startVM *VirtualMachine
		wantErr error
		wantVM  *VirtualMachine
	}

	cases := []testStruct{
		{
			op: OPToAltStack,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{1}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 998,
				altStack:  stack.New([][]byte{{1}}),
			},
		}, {
			op: OPFromAltStack,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				altStack:  stack.New([][]byte{{1}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 998,
				evalStack: stack.New([][]byte{{1}}),
			},
		}, {
			op: OPFromAltStack,
			startVM: &VirtualMachine{

				maxMemory: 1000,
			},
			wantErr: stack.ErrStackIndexOutOfRange,
		}, {
			op: OPDepth,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{1}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 999,
				evalStack: stack.New([][]byte{{1}, {1}}),
			},
		}, {
			op: OPDepth,
			startVM: &VirtualMachine{

				maxMemory: 0,
				evalStack: stack.New([][]byte{{1}}),
			},
			wantErr: ErrMaxMemoryExceeded,
		}, {
			op: OPDrop,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{1}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 999,
				evalStack: stack.New(nil),
			},
		}, {
			op: OPDrop,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{1}, {2}, {3}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 999,
				evalStack: stack.New([][]byte{{1}, {2}}),
			},
		}, {
			op: OPDup,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{1}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 999,
				evalStack: stack.New([][]byte{{1}, {1}}),
			},
		}, {
			op: OPDup,
			startVM: &VirtualMachine{

				maxMemory: 0,
				evalStack: stack.New([][]byte{{1}}),
			},
			wantErr: ErrMaxMemoryExceeded,
		}, {
			op: OPOver,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{2}, {1}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 999,
				evalStack: stack.New([][]byte{{2}, {1}, {2}}),
			},
		}, {
			op: OPOver,
			startVM: &VirtualMachine{

				maxMemory: 0,
				evalStack: stack.New([][]byte{{2}, {1}}),
			},
			wantErr: ErrMaxMemoryExceeded,
		}, {
			op: OPOver,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{3}, {4}, {2}, {1}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 999,
				evalStack: stack.New([][]byte{{3}, {4}, {2}, {1}, {2}}),
			},
		}, {
			op: OPSwap,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{2}, {1}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 999,
				evalStack: stack.New([][]byte{{1}, {2}}),
			},
		}, {
			op: OPSwap,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{3}, {4}, {1}, {2}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 999,
				evalStack: stack.New([][]byte{{3}, {4}, {2}, {1}}),
			},
		}, {
			op: OP2Swap,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{3}, {4}, {1}, {2}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 998,
				evalStack: stack.New([][]byte{{1}, {2}, {3}, {4}}),
			},
		}, {
			op: OPPick,
			startVM: &VirtualMachine{
				maxMemory: 1000,
				evalStack: stack.New([][]byte{{0xff, 0xff}, {2}, {1}, {2}}),
			},
			wantVM: &VirtualMachine{
				maxMemory: 998,
				evalStack: stack.New([][]byte{{0xff, 0xff}, {2}, {1}, {0xff, 0xff}}),
			},
		}, {
			op: OPPick,
			startVM: &VirtualMachine{
				maxMemory: 1,
				evalStack: stack.New([][]byte{{0xff, 0xff}, {2}, {1}, {2}}),
			},
			wantErr: ErrMaxMemoryExceeded,
		}, {
			op: OPRoll,
			startVM: &VirtualMachine{
				maxMemory: 1000,
				evalStack: stack.New([][]byte{{3}, {2}, {1}, {2}}),
			},
			wantVM: &VirtualMachine{
				maxMemory: 998,
				evalStack: stack.New([][]byte{{2}, {1}, {3}}),
			},
		}, {
			op: OP2Over,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{3}, {4}, {1}, {2}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 998,
				evalStack: stack.New([][]byte{{3}, {4}, {1}, {2}, {3}, {4}}),
			},
		}, {
			op: OP2Drop,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{3}, {4}, {1}, {2}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 998,
				evalStack: stack.New([][]byte{{3}, {4}}),
			},
		}, {
			op: OP2Dup,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{3}, {4}, {1}, {2}}),
			},
			wantVM: &VirtualMachine{

				maxMemory: 998,
				evalStack: stack.New([][]byte{{3}, {4}, {1}, {2}, {1}, {2}}),
			},
		}, {
			op: OP2Swap,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{5}, {1}, {2}}),
			},
			wantErr: stack.ErrStackSwapBadArgument,
		}, {
			op: OP2Over,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{9}, {2}}),
			},
			wantErr: stack.ErrStackOverBadArgument,
		}, {
			op: OP2Drop,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{8}}),
			},
			wantErr: stack.ErrStackDropBadArgument,
		}, {
			op: OP2Dup,
			startVM: &VirtualMachine{

				maxMemory: 1000,
				evalStack: stack.New([][]byte{{7}}),
			},
			wantErr: stack.ErrStackDupBadArgument,
		},
	}
	stackops := []OP{
		OPDepth, OPFromAltStack, OPToAltStack, OPDrop, OPDup,
	}
	for _, op := range stackops {
		cases = append(cases, testStruct{
			op: op,
			startVM: &VirtualMachine{
				maxMemory: 0},
			wantErr: ErrMaxMemoryExceeded,
		})
	}

	for i, c := range cases {
		err := opTable[c.op].callback(c.startVM)

		if err != c.wantErr {
			t.Errorf("case %d, op %s: got err = %v want %v", i, opTable[c.op].name, err, c.wantErr)
			continue
		}
		if c.wantErr != nil {
			continue
		}

		if !vmEqual(c.startVM, c.wantVM) {
			t.Errorf("case %d, op %s: unexpected vm result\n\tgot:  %+v\n\twant: %+v\n", i, opTable[c.op].name, c.startVM, c.wantVM)
		}
	}
}
