package vmfuzz

import (
	"gitlab.com/cloq/go-cloq/core/qvm/vm"
	"gitlab.com/cloq/go-cloq/core/script"
)

// Fuzz VM and Script
func Fuzz(data []byte) int {

	prog, err := script.Decompile(data)
	if err != nil {
		if prog != "" {
			panic("prog != nil on error")
		}
		return 0
	}

	fuzzVM := vm.New(512, data, nil)

	b, errVM := fuzzVM.Run(nil)
	if errVM != nil {
		if b != false {
			panic("VM result should be false on error")
		}
		return 0
	}
	return 1

}
