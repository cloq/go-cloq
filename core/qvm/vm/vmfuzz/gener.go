package vmfuzz

import (
	cryptorand "crypto/rand"
	"fmt"
	"io/ioutil"
	"math/rand"
	"strconv"

	"gitlab.com/cloq/go-cloq/core/script"
)

var ops = [256]string{"false", "true", "not", "equal", "and", "or", "push", "toalt",
	"fromalt", "depth", "drop", "dup", "over", "swap", "drop2", "dup2",
	"over2", "swap2", "jumpif", "verify", "halt", "sha256", "sha3", "blake2",
	"checksig", "txid", "checksigtx"}

func stubGenTest() {
	// This is a stub function to generate testcases with go test.
	// The testcases will sample and dump opcodes to the /corpus/ directory.
	prog := script.NewBuilder(1)
	for i := 0; i < 10; i++ {
		limit := rand.Intn(30)
		for j := 0; j < limit; j++ {
			idx := rand.Intn(20)
			token := ops[idx]
			op, _ := script.GetOPByName(token)
			pushSize := rand.Intn(300)
			prog.AddOP(op)
			prog.AddData(randBytes(pushSize))

		}
		fmt.Println("I am here")
		v := strconv.Itoa(i)
		err := ioutil.WriteFile(v, prog.Script(), 0644)
		if err != nil {
			panic(err)

		}
	}
}

func randBytes(size int) []byte {
	var buf = make([]byte, size)
	random := cryptorand.Reader
	random.Read(buf)
	return buf
}
