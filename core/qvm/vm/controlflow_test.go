package vm

import (
	"testing"

	stack "gitlab.com/cloq/go-cloq/core/qvm/vm/internal"
)

func TestControlOps(t *testing.T) {
	type testCase struct {
		op      OP
		startVM *VirtualMachine
		wantErr error
		wantVM  *VirtualMachine
	}
	cases := []testCase{{
		op: OPJMPIF,
		startVM: &VirtualMachine{
			maxMemory: 50000,
			ip:        0,
			nextIP:    1,
			evalStack: stack.New([][]byte{{1}}),
			memory:    []byte{0x05, 0x00, 0x00, 0x00},
		},
		wantErr: nil,
		wantVM: &VirtualMachine{
			maxMemory: 49999,
			ip:        0,
			nextIP:    5,
			evalStack: stack.New([][]byte{}),
			memory:    []byte{0x05, 0x00, 0x00, 0x00},
		},
	}, {
		op: OPJMPIF,
		startVM: &VirtualMachine{
			maxMemory: 50000,
			ip:        0,
			nextIP:    1,
			evalStack: stack.New([][]byte{{}}),
			memory:    []byte{0x05, 0x00, 0x00, 0x00},
		},
		wantErr: nil,
		wantVM: &VirtualMachine{
			maxMemory: 49999,
			ip:        0,
			nextIP:    1,
			evalStack: stack.New([][]byte{}),
			memory:    []byte{0x05, 0x00, 0x00, 0x00},
		},
	}, {
		op: OPVerify,
		startVM: &VirtualMachine{
			ip:        0,
			maxMemory: 50000,

			evalStack: stack.New([][]byte{{1}}),
		},
		wantErr: nil,
		wantVM: &VirtualMachine{
			maxMemory: 49999,

			evalStack: stack.New([][]byte{}),
		},
	}, {
		op: OPVerify,
		startVM: &VirtualMachine{
			maxMemory: 50000,

			evalStack: stack.New([][]byte{{1, 1}}),
		},
		wantErr: nil,
		wantVM: &VirtualMachine{
			maxMemory: 49999,
			evalStack: stack.New([][]byte{}),
		},
	}, {
		op: OPVerify,
		startVM: &VirtualMachine{
			maxMemory: 50000,

			evalStack: stack.New([][]byte{{}}),
		},
		wantErr: ErrVerifyFailed,
	}, {
		startVM: &VirtualMachine{maxMemory: 50000},
		op:      OPHalt,
		wantErr: ErrHalt,
	}}

	limitChecks := []OP{
		OPCall, OPVerify, OPHalt,
	}

	for _, op := range limitChecks {
		cases = append(cases, testCase{
			op:      op,
			startVM: &VirtualMachine{maxMemory: 0},
			wantErr: ErrMaxMemoryExceeded,
		})
	}

	for i, c := range cases {
		err := opTable[c.op].callback(c.startVM)

		if err != c.wantErr {
			t.Errorf("case %d, op %s: got err = %v want %v", i, c.op.String(), err, c.wantErr)
			continue
		}
		if c.wantErr != nil {
			continue
		}

		if !vmEqual(c.startVM, c.wantVM) {
			t.Errorf("case %d, op %s: unexpected vm result\n\tgot:  %+v\n\twant: %+v\n", i, c.op.String(), c.startVM, c.wantVM)
		}
	}
}
