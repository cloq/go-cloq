package vm

import (
	"sync"
	"testing"

	"gitlab.com/cloq/go-cloq/core/script"
)

type TestCase struct {
	name     string
	script   string
	args     [][]byte
	expected bool
	err      error
}

func TestVMRun(t *testing.T) {
	testCases := map[string]TestCase{
		"sha256 equality": {
			name:     "sha256 equality",
			script:   "dup2 equal verify sha256 swap sha256 equal",
			args:     [][]byte{FromInt64(414141), FromInt64(414141)},
			expected: true,
			err:      nil,
		}, "preimage blake2": {
			name:   "preimage blake2",
			script: "blake2 0x3c228306552177f5a304cb12a5b5e60897f2f486b64671afdccf0f8dd9410cbd equal",
			args:   [][]byte{[]byte("helloworld")},

			expected: true,
			err:      nil,
		},
		"preimage sha3": {
			name:   "preimage sha3",
			script: "sha3 0x92dad9443e4dd6d70a7f11872101ebff87e21798e4fbb26fa4bf590eb440e71b equal",
			args:   [][]byte{[]byte("helloworld")},

			expected: true,
			err:      nil,
		}, "conditional preimage if branch fails else succeed": {
			name:   "conditional preimage if branch fails else succeed",
			script: "dup blake2 0xb966a57f3010512d1ebe8629864c71c909aeb4752e93ffa525f00976c93e33c0 equal jumpif:@end sha256 0x6cd8a81bea00783c3fcfca07eef8c80adc1fb1ba46dc611d5554983e6ae3a7ea equal @end true",
			args:   [][]byte{[]byte("hellobob")},

			expected: true,
			err:      nil,
		}, "conditional preimage if branch succeed": {
			name:   "conditional preimage if branch succeed",
			script: "dup blake2 0xb966a57f3010512d1ebe8629864c71c909aeb4752e93ffa525f00976c93e33c0 equal jumpif:@end sha256 0x6cd8a81bea00783c3fcfca07eef8c80adc1fb1ba46dc611d5554983e6ae3a7ea equal @end true",
			args:   [][]byte{[]byte("helloalice")},

			expected: true,
			err:      nil,
		}, "infinite ping pong jmp": {
			name:   "infinite ping pong jmp",
			script: "@start true jumpif:@end @end true jumpif:@start",
			args:   nil,

			expected: false,
			err:      ErrMaxMemoryExceeded,
		}, "p2pkh equivalent with failure": {
			name:   "p2pkh equivalent with failure",
			script: "sha3 0x3526218466de77a9ddbc35f7050d7a506c845508e7e1e5e13d44868cd326d002 equal verify checksig",
			args:   [][]byte{decodeHex("ea76a0bcee1f285d1d53a566297fddf8c954d02e722ed6160022ec80e3a691fb56e3383b52256d65fd9d828d0d5da4deb56684152b3ab8b3948aaf71b1208f01"), decodeHex("b0a20339bb05f8f734b2121a84f7672f0bccff4f20eadf48ff81b34a82ef010c")},

			expected: false,
			err:      ErrBadSigScheme,
		}, "p2pkh equivalent": {
			name:   "p2pkh equivalent",
			script: "sha3 0x3526218466de77a9ddbc35f7050d7a506c845508e7e1e5e13d44868cd326d002 equal verify checksig",
			args:   [][]byte{decodeHex("2687a26ffe30fbda31464fb71ed7118eff7d46ef7b6301a2555a956caaa1d35a2897ca38220b41888af1291f0a5ab3b1b28e7540b688d88bc0982cc3bf4c4b88"), decodeHex("b0a20339bb05f8f734b2121a84f7672f0bccff4f20eadf48ff81b34a82ef010c"), decodeHex("80569b52cbd52f7a221153903cdd39a92e380f86b4327f5cf8f05b5d1619ff25"), {byte(SigSchemeSchnorr)}, decodeHex("b0a20339bb05f8f734b2121a84f7672f0bccff4f20eadf48ff81b34a82ef010c")},

			expected: true,
			err:      nil,
		},
	}
	var wg sync.WaitGroup
	outputs := make(chan Result, 8)

	for _, test := range testCases {
		testCase := test
		prog, err := script.Compile(testCase.script)
		if err != nil {
			t.Logf("failed to assemble script with error %v", err)
		}

		/*
			t.Logf("test case %s", test.name)
			t.Logf("compiled script %v", hex.EncodeToString(prog))
			t.Logf("disassemled script %v", dis)
		*/
		vm := New(1000, prog, testCase.args)

		// Our execution model consists of wrapping the vm Run func inside
		// a go routine this gives a better predictable result .
		go func(outputs chan Result, title string) {
			var res Result
			res.name = title
			res.success, res.err = vm.Run(nil)
			outputs <- res
		}(outputs, testCase.name)
		wg.Add(1)
		// Because one script can take longer than an other the output channel
		// will hold out of order results we log them to notice that the infinite loop
		// script which drains all the memory taking more time than others is the one who's
		// error is last read.

		go func(ch <-chan Result, wg *sync.WaitGroup) {
			defer wg.Done()
			var res = <-ch
			var expected = testCases[res.name].expected
			var err = testCases[res.name].err
			t.Logf("Testresult :\t Name : %s | Expected Result : %v | VM Result : %v | Error : %v", res.name, expected, res.success, res.err)
			if res.success != expected {
				t.Errorf("expected: %v got %v", expected, res.success)
			}
			if res.err != nil && !res.success {
				if res.err != err {
					t.Errorf("Testcase : %v -> expected failure with : %v got %v \n", res.name, err, res.err)
				}

			}
		}(outputs, &wg)

	}
	wg.Wait()
	t.Log("Verification finished")

}
