package vm

import "fmt"

// OP designates an opcode
type OP uint8

// String implements Stringer for OPCodes
func (op OP) String() string {
	return opTable[op].name
}

// Instruction is a type that wraps an opcode its length and data.
type Instruction struct {
	Opcode OP     // instruction's opcode
	Length uint32 // specifies the length of the argument to follow
	Data   []byte // opcode data
}

// opInfo stores information about an opcode
type opInfo struct {
	op       OP                          // opcode's byte
	arity    uint8                       // opcodes's arity
	name     string                      // opcode's name
	callback func(*VirtualMachine) error // opcode's callback
}

const (
	// OPFalse for a False boolean
	OPFalse OP = 0x0
	// OPTrue for a True boolean
	OPTrue OP = 0x1

	// The following are interpreted as int32 numbers

	// OP0 is synonymous to false
	OP0 = OPFalse
	// OP1 is synonymous to true
	OP1 = OPTrue

	// OPPush pushes the length-prefixed (1 byte) byte array to the stack.
	OPPush OP = 0x4a

	// OPNOP does nothing
	OPNOP OP = 0x60

	// VM operations 0x61--0x69

	// OPJMPIF jumps to the label if the top of the stack evaluates to true.
	OPJMPIF OP = 0x62

	// OPCall spawns a child vm to execute a program pushes true
	// if child vm executes successfully otherwise pushes false
	OPCall OP = 0x63
	// OPVerify checks the top of the stack if true it pops it
	// otherwise execution fails.
	OPVerify OP = 0x64
	// OPHalt stops execution automatically .
	OPHalt OP = 0x65

	// Stack operands 0x6a - 0x7f

	// OPToAltStack moves the top value to altstack.
	OPToAltStack OP = 0x6a
	// OPFromAltStack moves top value from altstack to data stack.
	OPFromAltStack OP = 0x6b
	// OPDepth pushes the stack depth to the top.
	OPDepth OP = 0x6c
	// OPDrop drops top item from the stack.
	OPDrop OP = 0x6d
	// OP2Drop drops top two items from the stack.
	OP2Drop OP = 0x6e
	// OPDup duplicates top item.
	OPDup OP = 0x6f
	// OP2Dup duplicates top two items.
	OP2Dup OP = 0x71
	// OPOver duplicates the second to top item on the stack.
	OPOver OP = 0x72
	// OP2Over duplicates the two items below the second to top item on the stack.
	OP2Over OP = 0x73
	// OPSwap swaps top two stack elements
	OPSwap OP = 0x74
	// OP2Swap swaps 2 items below the top 2 items on the stack.
	OP2Swap OP = 0x75
	// OPPick copies the n-th item to the top of the stack
	OPPick OP = 0x76
	// OPRoll moves the n-th item to the top of the stack
	OPRoll OP = 0x77

	// Binary operations with truncation 0x81--0x8f

	// Equality operators for byte arrays

	// OPEqual checks for equality.
	OPEqual OP = 0x80

	// Booleans 0x9a-0x9f

	// OPAND runs AND on two booleans from the stack
	OPAND OP = 0x9a
	// OPOR runs OR on two booleans from the stack
	OPOR OP = 0x9b
	// OPNOT pops a boolean and negates it
	OPNOT OP = 0x9c

	// Cryptography 0xa1-0xaf

	// OPSHA256 computes the SHA2-256 hash of the top item.
	OPSHA256 OP = 0xa2
	// OPSHA3 computes the SHA3-256 hash of the top item.
	OPSHA3 OP = 0xa3
	// OPBlake2 computes the Blake2b-256 hash of the top item.
	OPBlake2 OP = 0xa4

	// OPCheckSig verifies a signature on a message from the stack.
	OPCheckSig OP = 0xaa
	// OPCheckMultiSig runs checks for multisigners.
	OPCheckMultiSig OP = 0xab
	// OPCheckSigTx verifies a signature on txid (implicitly).
	OPCheckSigTx OP = 0xac

	// Introspection 0xc1--0xc9

	// OPMintime checks if output is older than n.
	// "30d" mintime will verify that Output.Height + 30 days > current height
	OPMintime OP = 0xc1
	// OPMaxtime checks if output is younger than n.
	// "60d" maxtime will verify that Output.Height + 60d < current height
	OPMaxtime OP = 0xc2
	// OPCheckOutput checks if output hash is the one in the contract
	OPCheckOutput OP = 0xc3
	// OPTXID computes the txid and pushes it into the stack
	OPTXID OP = 0xa9
)

var opsByName map[string]opInfo

// opTable is an array of opcode information that serves as
// a gateway to each opcode's name, arity and callback
var (
	opTable = [256]opInfo{
		OPFalse:        {OPFalse, 0, "false", opFalse},
		OPTrue:         {OPTrue, 0, "true", opTrue},
		OPNOT:          {OPNOT, 0, "not", opNOT},
		OPEqual:        {OPEqual, 0, "equal", opEqual},
		OPAND:          {OPAND, 0, "and", opAND},
		OPOR:           {OPOR, 0, "or", opOR},
		OPPush:         {OPPush, 1, "push", opPush},
		OPToAltStack:   {OPToAltStack, 0, "toalt", opToAltStack},
		OPFromAltStack: {OPFromAltStack, 0, "fromalt", opFromAltStack},
		OPDepth:        {OPDepth, 0, "depth", opDepth},
		OPDrop:         {OPDrop, 0, "drop", opDrop},
		OPDup:          {OPDup, 0, "dup", opDup},
		OPOver:         {OPOver, 0, "over", opOver},
		OPSwap:         {OPSwap, 0, "swap", opSwap},
		OP2Drop:        {OP2Drop, 0, "drop2", op2Drop},
		OP2Dup:         {OP2Dup, 0, "dup2", op2Dup},
		OP2Over:        {OP2Over, 0, "over2", op2Over},
		OP2Swap:        {OP2Swap, 0, "swap2", op2Swap},
		OPRoll:         {OPRoll, 1, "roll", opRoll},
		OPPick:         {OPPick, 1, "pick", opPick},
		OPJMPIF:        {OPJMPIF, 0, "jumpif", opJumpIf},
		OPVerify:       {OPVerify, 0, "verify", opVerify},
		OPHalt:         {OPHalt, 0, "halt", opHalt},
		OPSHA256:       {OPSHA256, 1, "sha256", opSHA256},
		OPSHA3:         {OPSHA3, 1, "sha3", opSHA3},
		OPBlake2:       {OPBlake2, 1, "blake2", opBLAKE2},
		OPCheckSig:     {OPCheckSig, 5, "checksig", opCheckSig},
		OPTXID:         {OPTXID, 0, "txid", opTXID},
		OPCheckSigTx:   {OPCheckSigTx, 4, "checksigtx", opCheckSigTx},
		OPMintime:      {OPMintime, 1, "mintime", opMintime},
		OPMaxtime:      {OPMaxtime, 1, "maxtime", opMaxtime},
		OPCheckOutput:  {OPCheckOutput, 1, "checkoutput", opCheckOutputHash},
	}
)
var isDisabled [256]bool

// GetOPByName returns an opcode by its token.
func GetOPByName(token string) (OP, bool) {
	op, ok := opsByName[token]
	if ok {
		return op.op, true
	}
	return OPNOP, false
}

func init() {

	opsByName = make(map[string]opInfo)
	for _, op := range opTable {
		opsByName[op.name] = op
	}
	for i := 0; i <= 255; i++ {
		if opTable[i].name == "" {
			opTable[i] = opInfo{OP(i), 0, fmt.Sprintf("NOPx%02x", i), opNop}
			isDisabled[i] = true
		}
	}

}
