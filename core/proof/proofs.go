// Package proof provide functions that generate fraud-proofs and authentication
// proofs either for off-line transaction, double-spends, fictious spends...
package proof

import (
	"bytes"

	"gitlab.com/cloq/go-cloq/pkg/merkle"
	"golang.org/x/crypto/sha3"
)

var trieHeight = 256
var defaultLeaf = []byte{0x0}

// CompactTrieAuditPath is a bitmap compact audit path for a sparse nerkle tree.
type CompactTrieAuditPath struct {
	bitmap []byte
	ap     [][]byte
}

// AuditPath represents a list of hashes used to reconstruct recursively the
// merkle root.
type AuditPath []merkle.AuditHash

// MerkleRoot retuAuditPathrns the root of the merkle tree
func MerkleRoot(items [][]byte) []byte {
	return merkle.Root(items)
}

// MerkleProof returns a merkle list proof
func MerkleProof(items [][]byte, index int) (AuditPath, error) {

	return merkle.Proof(items, index)

}

// MerkleVerify verifies if an audith path and items are correct
func MerkleVerify(items [][]byte, index int, ap AuditPath) bool {
	return merkle.Verify(items, index, ap)
}

// VerifyTrieInclusionProof verifies that key/value is included in the trie with latest root
func VerifyTrieInclusionProof(root []byte, ap [][]byte, key, value []byte) bool {
	// Hasher exports default hash function for trie
	var Hasher = func(data ...[]byte) []byte {
		hasher := sha3.New256()
		for i := 0; i < len(data); i++ {
			hasher.Write(data[i])
		}
		return hasher.Sum(nil)
	}
	leafHash := Hasher(key, value, []byte{byte(trieHeight - len(ap))})
	return bytes.Equal(root, verifyInclusion(ap, 0, key, leafHash))
}

// verifyInclusion returns the merkle root by hashing the merkle proof items
func verifyInclusion(ap [][]byte, keyIndex int, key, leafHash []byte) []byte {
	// Hasher exports default hash function for trie
	var Hasher = func(data ...[]byte) []byte {
		hasher := sha3.New256()
		for i := 0; i < len(data); i++ {
			hasher.Write(data[i])
		}
		return hasher.Sum(nil)
	}
	if keyIndex == len(ap) {
		return leafHash
	}
	if bitIsSet(key, keyIndex) {
		return Hasher(ap[len(ap)-keyIndex-1], verifyInclusion(ap, keyIndex+1, key, leafHash))
	}
	return Hasher(verifyInclusion(ap, keyIndex+1, key, leafHash), ap[len(ap)-keyIndex-1])
}

// VerifyTrieNonInclusionProof verifies a proof of non inclusion,
// Returns true if the non-inclusion is verified
func VerifyTrieNonInclusionProof(root []byte, ap [][]byte, key, value, proofKey []byte) bool {
	// Check if an empty subtree is on the key path
	if len(proofKey) == 0 {
		// return true if a DefaultLeaf in the key path is included in the trie
		return bytes.Equal(root, verifyInclusion(ap, 0, key, defaultLeaf))
	}
	// Check if another kv leaf is on the key path in 2 steps
	// 1- Check the proof leaf exists
	if !VerifyTrieInclusionProof(root, ap, proofKey, value) {
		// the proof leaf is not included in the trie
		return false
	}
	// 2- Check the proof leaf is on the key path
	var b int
	for b = 0; b < len(ap); b++ {
		if bitIsSet(key, b) != bitIsSet(proofKey, b) {
			// the proofKey leaf node is not on the path of the key
			return false
		}
	}
	// return true because we verified another leaf is on the key path
	return true
}

func bitIsSet(bits []byte, i int) bool {
	return bits[i/8]&(1<<uint(7-i%8)) != 0
}
func bitSet(bits []byte, i int) {
	bits[i/8] |= 1 << uint(7-i%8)
}
