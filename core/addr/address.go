// Package addr defines cloq addresses and their semantics.
package addr

import (
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/pkg/bech32"
	"gitlab.com/cloq/go-cloq/protocol"
)

// Address is a bech32 encoded script
type Address string

// Encode a new address from a script
func Encode(script []byte, net string) (string, error) {

	var prefix string

	// Compute script hash
	scriptHash := crypto.SHA3(protocol.TagScript, script)
	switch net {
	case "mainnet":
		prefix = protocol.MainnetAddressPrefix
	case "testnet":
		prefix = protocol.TestnetAddressPrefix
	default:
		prefix = protocol.RegnetAddressPrefix
	}

	return bech32.EncodeCloqAddress(prefix, scriptHash)
}

// Decode an address to a script hash
func Decode(addr string) (string, []byte, error) {

	return bech32.DecodeCloqAddress(addr)
}

// EncodeHash a new address from a script hash
func EncodeHash(scriptHash crypto.Hash, net string) (string, error) {

	var prefix string

	switch net {
	case "mainnet":
		prefix = protocol.MainnetAddressPrefix
	case "testnet":
		prefix = protocol.TestnetAddressPrefix

	default:
		prefix = protocol.RegnetAddressPrefix
	}

	return bech32.EncodeCloqAddress(prefix, scriptHash[:])
}
