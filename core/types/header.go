package types

import (
	"errors"
	"math/big"

	"github.com/fxamacker/cbor/v2"
	"gitlab.com/cloq/go-cloq/core/crypto"
)

var (
	errInvalidTimestamp        = errors.New("invalid timestamp for block header")
	errInvalidRoot             = errors.New("invalid root for block header")
	errInvalidTarget           = errors.New("invalid target for block")
	errInvalidChainWork        = errors.New("invalid chainwork for block")
	errInvalidTransactionCount = errors.New("block is invalid contains no transactions")
)

// Header represents a block header in the blockchain.
// A Header contains all the data required to do fast-sync
// an contains all the arguments required to request and verify
// witness proofs of inclusion (outpoints created at block k)
// witness proofs of execution (outpoints spent at block k)
// witness proofs of a state transition (tx executed contracts and created new ones)
// the rest of the data is used to verify proof of work.
type Header struct {
	_              struct{}      `cbor:"omitempty,toarray"`
	Version        uint64        `json:"version"`        // Header version
	Height         uint64        `json:"height"`         // height in the chain
	Time           uint64        `json:"timestamp"`      // generation timestamp
	Nonce          uint64        `json:"nonce"`          // Work scratchpad
	Parent         crypto.Hash   `json:"parent"`         // Previous header hash
	Uncles         []crypto.Hash `json:"uncles"`         // Uncles of this header according to GHOST
	Root           crypto.Hash   `json:"stateRoot"`      // State trie root after update
	CommitmentRoot crypto.Hash   `json:"commitmentRoot"` // merkle root of txids
	WitnessRoot    crypto.Hash   `json:"witnessRoot"`    // merkle root of witness txids
	TransitionRoot crypto.Hash   `json:"transitionRoot"` // merkle root of input contracts executed and open contracts created in this block.
	Target         crypto.Hash   `json:"target"`         // Work target in explicit big int
	ChainWork      crypto.Hash   `json:"chainWork"`      // cumulative chainwork of this header
}

// Validate checks that a header is valid syntactically
func (hdr *Header) Validate() (bool, error) {

	if hdr.Time == 0 {
		return false, errInvalidTimestamp
	}
	if hdr.TransitionRoot.Equal(crypto.EmptyHash) {
		return false, errInvalidRoot
	}
	if hdr.ChainWork.Big().Cmp(big.NewInt(0)) <= 0 {
		return false, errInvalidChainWork
	}
	if hdr.Target.Big().Cmp(big.NewInt(0)) <= 0 {
		return false, errInvalidTarget
	}
	return true, nil

}

// GetConsensusVersion returns the block header version
func (hdr *Header) GetConsensusVersion() uint64 {
	return hdr.Version
}

// GetParent returns parent block hash
func (hdr *Header) GetParent() crypto.Hash {
	return hdr.Parent
}

// GetHeight returns block height
func (hdr *Header) GetHeight() uint64 {
	return hdr.Height
}

// GetTimestamp returns block timestamp
func (hdr *Header) GetTimestamp() uint64 {
	return hdr.Time
}

// GetNonce returns block's nonce
func (hdr *Header) GetNonce() uint64 {
	return hdr.Nonce
}

// GetUncles returns block's nonce
func (hdr *Header) GetUncles() []crypto.Hash {
	return hdr.Uncles
}

// GetTarget returns block's target
func (hdr *Header) GetTarget() crypto.Hash {
	return hdr.Target
}

// GetChainWork return's header difficulty
func (hdr *Header) GetChainWork() crypto.Hash {
	return hdr.ChainWork
}

// GetStateRoot returns the state trie root after update
func (hdr *Header) GetStateRoot() crypto.Hash {
	return hdr.Root
}

// GetCommitmentRoot returns the root of merkle tree of txids (root(tree(commitments)))
func (hdr *Header) GetCommitmentRoot() crypto.Hash {
	return hdr.CommitmentRoot
}

// GetWitnessRoot returns the root of merkle tree of wtxids (root(tree(wtxid)))
func (hdr *Header) GetWitnessRoot() crypto.Hash {
	return hdr.WitnessRoot
}

// GetTransitionRoot returns a merkle tree root of outpoints spent and created in this block
func (hdr *Header) GetTransitionRoot() crypto.Hash {
	return hdr.TransitionRoot
}

// Hash computes the blockheader hash
func (hdr *Header) Hash() (crypto.Hash, error) {

	hdrBytes, err := hdr.Marshal()
	return crypto.HeaderHash(hdrBytes), err
}

// NewEmptyHeader creates a consistent header that is empty
func NewEmptyHeader() *Header {
	return &Header{
		Version:        0,
		Height:         0,
		Time:           0,
		Nonce:          0,
		Parent:         crypto.Hash{},
		Uncles:         nil,
		Root:           crypto.Hash{},
		CommitmentRoot: crypto.Hash{},
		WitnessRoot:    crypto.Hash{},
		TransitionRoot: crypto.Hash{},
		Target:         crypto.Hash{},
		ChainWork:      crypto.Hash{},
	}
}

// NewHeader generates a new block header
func NewHeader(parent, stateRoot, txTree, witnessTree, transitionTree crypto.Hash, uncles []crypto.Hash,
	height, nonce uint64, timestamp uint64, target crypto.Hash, chainWork crypto.Hash) *Header {
	return &Header{
		Parent:         parent,
		Uncles:         uncles,
		Root:           stateRoot,
		CommitmentRoot: txTree,
		WitnessRoot:    witnessTree,
		TransitionRoot: transitionTree,
		Height:         height,
		Time:           timestamp,
		Nonce:          nonce,
		Target:         target,
		ChainWork:      chainWork,
	}

}

// Marshal implements serialization for block headers
func (hdr *Header) Marshal() ([]byte, error) {
	return cbor.Marshal(hdr)
}

// Unmarshal implements deserialization
func (hdr *Header) Unmarshal(buf []byte) error {
	return cbor.Unmarshal(buf, hdr)
}

// Compare two headers return true if caller is better false otherwise.

// Clone returns a copy of the header
func (hdr *Header) Clone() *Header {

	return &Header{
		Version:        hdr.GetConsensusVersion(),
		Height:         hdr.GetHeight(),
		Time:           hdr.GetTimestamp(),
		Nonce:          hdr.GetNonce(),
		Parent:         hdr.GetParent(),
		Uncles:         hdr.GetUncles(),
		Root:           hdr.GetStateRoot(),
		CommitmentRoot: hdr.GetCommitmentRoot(),
		WitnessRoot:    hdr.GetWitnessRoot(),
		TransitionRoot: hdr.GetTransitionRoot(),
		Target:         hdr.GetTarget(),
		ChainWork:      hdr.GetChainWork(),
	}
}
