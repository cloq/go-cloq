package types

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOpenContracts(t *testing.T) {

	contracts := genOpenContracts(100)

	for _, contract := range contracts {

		b, err := contract.Marshal()
		if err != nil {
			t.Fatal("serialization failed with error :", err)
		}
		var oc = new(OpenContract)
		err = oc.Unmarshal(b)
		if err != nil {
			t.Fatal("deserialization failed with error :", err)
		}
		assert.Equal(t, contract, oc)

	}
}
