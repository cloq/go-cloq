package types

import (
	"encoding/json"

	"github.com/fxamacker/cbor/v2"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/proof"
)

// Block represents a header and a body composed of transactions and proofs.
// Transactions within a block are ordered lexicographically and are required for
// a block to be considered valid.
// For each transaction input there's a witness a Bit Sparse Merkle Tree inclusion proof.
// The inclusion proofs are used for the stateless validation on light clients.
type Block struct {
	_       struct{} `cbor:"omitempty,toarray"`
	*Header `json:"header"`
	*Body   `json:"body"`
}

// Body wraps the blockbody
type Body struct {
	Txs    []*Transaction               `json:"transactions"` // transaction list
	Proofs []proof.CompactTrieAuditPath `json:"proofs"`       // inclusion proofs for stateless validation
}

// Validate checks if a block is valid syntactically.
func (block *Block) Validate() (bool, error) {

	ok, err := block.Header.Validate()
	if !ok || err != nil {
		return false, err
	}
	if len(block.Body.Txs) == 0 {
		return false, errInvalidTransactionCount
	}
	return true, nil
}

// Marshal implements the serialization interface.
func (block *Block) Marshal() ([]byte, error) {
	return cbor.Marshal(block.Body)
}

// Unmarshal implements the deserialization interface.
func (block *Block) Unmarshal(buf []byte) error {
	return cbor.Unmarshal(buf, block.Body)
}

// MarshalFull implements serialization of the full block not just the body
func (block *Block) MarshalFull() ([]byte, error) {

	return cbor.Marshal(block)
}

// UnmarshalFull implements deserialization of the full block not just the body
func (block *Block) UnmarshalFull(buf []byte) error {

	return cbor.Unmarshal(buf, block)
}

// NewBlock creates a new block
func NewBlock(hdr *Header, txs []*Transaction, proofs []proof.CompactTrieAuditPath) *Block {

	blk := &Block{
		Header: hdr,
		Body: &Body{
			txs,
			proofs,
		},
	}
	return blk
}

// NewEmptyBlock creates an empty block
func NewEmptyBlock() *Block {
	return &Block{Header: &Header{}, Body: &Body{}}
}

// Transactions returns the list of transactions in a block
func (block *Block) Transactions() []*Transaction {
	return block.Txs
}

// Hash returns the block's header hash
func (block *Block) Hash() (crypto.Hash, error) {
	return block.Header.Hash()
}

// CreatedContracts returns a list of all contracts created coinbase contract not included.
func (block *Block) CreatedContracts() ([]Contract, error) {

	txCount := len(block.Txs)

	outs := make([]Contract, 0, txCount)

	for _, transaction := range block.Txs[1:] {
		txContracts, err := transaction.CreatedContracts()
		if err != nil {
			return nil, err
		}
		outs = append(outs, txContracts...)
	}
	return outs, nil
}

// CoinbaseTx returns the coinbase transaction.
func (block *Block) CoinbaseTx() (*Transaction, error) {

	if len(block.Txs) == 0 {
		return nil, errInvalidTransactionCount
	}
	return block.Txs[0], nil
}

// CoinbaseContract returns the created coinbase contract
func (block *Block) CoinbaseContract() (Contract, error) {

	coinbaseTx, err := block.CoinbaseTx()
	if err != nil {
		return Contract{}, err
	}

	coinbaseTXID, err := coinbaseTx.Hash()
	if err != nil {
		return Contract{}, err
	}
	coinbaseOutput, err := coinbaseTx.Outputs()
	if err != nil {
		return Contract{}, err
	}

	if len(coinbaseOutput) != 1 {
		return Contract{}, err
	}

	return NewContract(coinbaseTXID, 0, coinbaseOutput[0]), nil

}

// CreatedContractsHashes returns a list of hashes of the contract created
func (block *Block) CreatedContractsHashes() ([][]byte, error) {

	txCount := len(block.Txs)

	outHashes := make([][]byte, 0, txCount)

	for _, transaction := range block.Txs {
		txContractHashes, err := transaction.CreatedContractsHash()
		if err != nil {
			return nil, err
		}
		outHashes = append(outHashes, txContractHashes...)
	}
	return outHashes, nil
}

// ExecutedContracts returns a list of the hashes of contracts executed
// skipping the coinbase input.
func (block *Block) ExecutedContracts() ([][]byte, error) {
	// allocate twice as much capacity as the number of transactions
	executedContracts := make([][]byte, 0, 2*len(block.Txs))
	// Skip coinbase transaction
	for _, transaction := range block.Txs[1:] {

		txInputs, err := transaction.ExecutedContracts()
		if err != nil {
			return nil, err
		}

		executedContracts = append(executedContracts, txInputs...)
	}
	return executedContracts, nil
}

// String implements string marshalling for block
func (block *Block) String() string {

	blk, err := json.Marshal(block)
	if err != nil {
		return ""
	}
	return string(blk)
}
