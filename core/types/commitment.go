package types

import (
	"github.com/fxamacker/cbor/v2"
	"github.com/pkg/errors"
	"gitlab.com/cloq/go-cloq/pkg/merkle"

	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/script"
	"gitlab.com/cloq/go-cloq/protocol"
)

// SigHash specifies what part of the transaction was signed by this input.
type SigHash uint8

const (
	// SigHashAll covers Hash(Commitment) i.e the TXID
	SigHashAll SigHash = 0x1
)

var (

	// ErrInputMalformed is returned when a bad serializiation of input is encountred.
	ErrInputMalformed = errors.New("input is malformed ")
	// ErrOutputMalformed is returned when a bad serializiation of output is encountred
	ErrOutputMalformed = errors.New("output is malformed ")
	// ErrCommitmentMalformed is returned when a bad serializiation of commitment is encountred
	ErrCommitmentMalformed = errors.New("commitment is malformed ")
	// ErrNilInputs is returned when a transaction as a null input
	ErrNilInputs = errors.New("transaction can't have empty inputs")
	// ErrNilOutputs is returned when a transaction has no outputs.
	ErrNilOutputs = errors.New("transaction can't have empty outputs")
	// ErrDuplicateInputs is returned when an input is duplicated in a transaction
	ErrDuplicateInputs = errors.New("transaction can't have duplicate inputs")
)

// Input is a hash reference to a currently executable contract (present in the state).
// Accompagnied by the script itself used to create the contract (spender recreates conditions).
// This script should evaluate to the address of the output we're spending.
// This alleviates the fee payments from sender to receiver and is useful for
// complex scripts.
type Input struct {
	_         struct{}      `cbor:"omitempty,toarray"`
	Origin    crypto.Hash   `json:"origin"`
	Validator script.Script `json:"validator"`
	Sighash   uint8         `json:"sighash"`
}

// Marshal implements the serialization interface using CBOR.
func (in *Input) Marshal() ([]byte, error) {

	inBytes, err := cbor.Marshal(in)
	return inBytes, err

}

// Unmarshal implements the deserialization interface using CBOR.
func (in *Input) Unmarshal(b []byte) error {
	return cbor.Unmarshal(b, in)
}

// Hash implements the SHA3 algorithm over an input using the tag 'in'.
func (in *Input) Hash() (crypto.Hash, error) {
	var h crypto.Hash

	inBytes, err := in.Marshal()

	if err != nil {
		return h, err
	}
	h = crypto.SHA3Hash(protocol.TagInput, inBytes)

	return h, nil
}

// Output is the new contract we're creating it has an address, a value and locktime.
type Output struct {
	_        struct{} `cbor:"omitempty,toarray"`
	Address  string   `json:"address"`
	Value    uint64   `json:"value"`
	Locktime uint64   `json:"locktime"`
}

// Marshal implements the serialization interface using CBOR.
func (out *Output) Marshal() ([]byte, error) {

	outBytes, err := cbor.Marshal(out)
	return outBytes, err

}

// Unmarshal implements the deserialization interface using CBOR.
func (out *Output) Unmarshal(b []byte) error {
	return cbor.Unmarshal(b, out)
}

// Hash implements the SHA3 algorithm over an input using the tag 'in'.
func (out *Output) Hash() (crypto.Hash, error) {
	var h crypto.Hash

	outBytes, err := out.Marshal()

	if err != nil {
		return h, err
	}
	h = crypto.SHA3Hash(protocol.TagOutput, outBytes)

	return h, nil
}

// Commitment represents the data a commiter (transaction creator)
// wishes to commit to (sign).
// While no signature enforcement is required, it's preferable to require
// a signature in all your scripts.
// Generally when creating a transaction we want to commit to it's version
// coinbase status (for miners) and the inputs we're spending and outputs
// we're creating.
// The commitment hash is alternatively called "txid" which is a generic unmalleable
// hash of transaction contents that can be signed or checked for transaction validity.
type Commitment struct {
	_       struct{} `cbor:"omitempty,toarray"`
	Inputs  []Input  `json:"inputs"`
	Outputs []Output `json:"outputs"`
}

// NewCommitment creates a new instance of commitment
func NewCommitment(inputs []Input, outputs []Output) Commitment {
	var comm Commitment
	comm.Inputs = inputs
	comm.Outputs = outputs
	return comm
}

// Marshal implements the serialization interface using CBOR.
func (commit *Commitment) Marshal() ([]byte, error) {
	return cbor.Marshal(commit)
}

// Unmarshal implements the deserialization interface using CBOR.
func (commit *Commitment) Unmarshal(b []byte) error {
	err := cbor.Unmarshal(b, commit)
	if err != nil {
		return err
	}
	if len(commit.Inputs) == 0 || len(commit.Outputs) == 0 {
		return ErrNilInputs
	}

	return nil
}

// Hash implements the SHA3 algorithm over an input using the tag 'in'.
func (commit *Commitment) Hash() (crypto.Hash, error) {

	var leaves = make([][]byte, 0, len(commit.Inputs)+len(commit.Outputs))

	for _, input := range commit.Inputs {

		inputHash, err := input.Hash()
		if err != nil {
			return crypto.EmptyHash, nil
		}
		leaves = append(leaves, inputHash.Bytes())
	}

	for _, output := range commit.Outputs {

		outputHash, err := output.Hash()
		if err != nil {
			return crypto.EmptyHash, nil
		}
		leaves = append(leaves, outputHash.Bytes())
	}

	commitmentRoot := merkle.Root(leaves)

	h := crypto.SHA3Hash(protocol.TagTXID, commitmentRoot)

	return h, nil
}

func validateCommitment(commit *Commitment) (bool, error) {

	if len(commit.Inputs) == 0 || len(commit.Outputs) == 0 {
		return false, ErrNilInputs
	}

	hasNilInputs := func(ins []Input) bool {
		for _, input := range ins {
			if input.Origin == crypto.EmptyHash {
				return true
			}
		}
		return false
	}
	// check for duplicates or null inputs
	hasDups := func(ins []Input) bool {

		m := make(map[string]bool)

		for _, input := range ins {
			_, ok := m[string(input.Origin[:])]
			if ok {
				return true

			}

		}
		return false
	}

	if hasDups(commit.Inputs) {
		return false, ErrDuplicateInputs
	}
	if hasNilInputs(commit.Inputs) {
		return false, ErrNilInputs
	}
	return true, nil
}

func validateCoinbaseCommitment(commit *Commitment) (bool, error) {

	if len(commit.Inputs) != 1 || len(commit.Outputs) < 1 {
		return false, ErrNilInputs
	}
	if commit.Inputs[0].Origin != crypto.EmptyHash {
		return false, ErrInputMalformed
	}

	if len(commit.Inputs[0].Validator) < protocol.MinCoinbaseValidator || len(commit.Inputs[0].Validator) > protocol.MaxCoinbaseValidator {
		return false, errors.New("bad validator length for coinbase tx")
	}
	return true, nil
}

// Validate a commitment
func (commit *Commitment) Validate(isCoinbase bool) (bool, error) {
	if isCoinbase {
		return validateCoinbaseCommitment(commit)
	}
	return validateCommitment(commit)
}
