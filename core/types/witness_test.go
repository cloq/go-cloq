package types

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

var witData = []Redeemer{
	[][]byte{[]byte("helloworld")},
	[][]byte{[]byte("hellobob")},
	[][]byte{[]byte("0xabcdef"), []byte("0xdeadbeef")},
}

func TestWitness(t *testing.T) {
	var wit Witness
	var err error

	wit.Push(witData[0])
	wit.Push(witData[1])

	witRLP, err := wit.Marshal()

	if err != nil {
		t.Error("failed to serialize witness with error", err)
	}
	var witDec Witness
	err = witDec.Unmarshal(witRLP)
	if err != nil {
		t.Error("failed to deserialize witness with error", err)
	}
	witJSON, err := json.MarshalIndent(witDec, "", " ")
	if err != nil {
		t.Error("failed to serialize witness with error", err)
	}
	t.Log(string(witJSON))
	assert.Equal(t, wit, witDec)

}
