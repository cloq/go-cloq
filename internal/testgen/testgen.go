package testgen

import (
	"crypto/rand"
	"math/big"
	mathrand "math/rand"
	"time"

	"gitlab.com/cloq/go-cloq/pkg/bech32"

	"gitlab.com/cloq/go-cloq/core/addr"
	"gitlab.com/cloq/go-cloq/core/crypto"
	"gitlab.com/cloq/go-cloq/core/proof"
	"gitlab.com/cloq/go-cloq/core/script"
	"gitlab.com/cloq/go-cloq/core/types"
	"gitlab.com/cloq/go-cloq/pkg/leb128"
	"gitlab.com/cloq/go-cloq/protocol"
	"golang.org/x/crypto/sha3"
)

var testScripts = []string{
	"sha3 0x92dad9443e4dd6d70a7f11872101ebff87e21798e4fbb26fa4bf590eb440e71b equal",
	"dup blake2 0xb966a57f3010512d1ebe8629864c71c909aeb4752e93ffa525f00976c93e33c0 equal jumpif:@end sha256 0x6cd8a81bea00783c3fcfca07eef8c80adc1fb1ba46dc611d5554983e6ae3a7ea equal @end true",
}

var testWitnesses = []types.Redeemer{
	[][]byte{[]byte("helloworld")},
	[][]byte{[]byte("hellobob")},
}

// GenTransaction Generates a dummy transaction.
func GenTransaction() *types.Transaction {
	inputs := make([]types.Input, 0, 8)
	var wit types.Witness
	for i := 0; i < 8; i++ {
		b := leb128.FromInt64(int64(i))
		hash := sha3.Sum256(b)
		testScript := testScripts[i%2]
		testScriptB, _ := script.Compile(testScript)
		inputs = append(inputs, types.Input{Origin: hash, Validator: testScriptB})
		wit.Push(testWitnesses[i%2])
	}

	outputs := make([]types.Output, 1)
	hash := crypto.SHA3Hash([]byte("a very weird script"))
	address, _ := addr.EncodeHash(hash, "testnet")
	outputs[0] = types.Output{
		Address:  address,
		Value:    5600,
		Locktime: 100,
	}
	commitment := types.NewCommitment(inputs, outputs)
	transaction := types.NewTransaction(0, 0, 0, 0, commitment, wit)

	return transaction
}

// GenBlocks Generates a slice of dummy blocks
func GenBlocks() []*types.Block {

	// let's generate some coinbase blocks
	hashes := make([]crypto.Hash, 10)

	blocks := make([]*types.Block, 10)

	var err error
	var i uint64
	hashes[0] = crypto.SHA3Hash([]byte("test-genesis-hash"))
	for i = 1; i < 10; i++ {
		coinbaseTX := GenCoinbase(i)
		if err != nil {
			panic("failure to calculate test hash")
		}
		hdr := types.NewHeader(hashes[i-1], crypto.EmptyHash, hashes[i], crypto.EmptyHash, crypto.EmptyHash, []crypto.Hash{crypto.EmptyHash}, i, 96580, uint64(time.Now().Unix()), crypto.SetBig(protocol.RegressionPoWLimit), crypto.SetBig(big.NewInt(171772)))
		blk := types.NewBlock(hdr, append([]*types.Transaction{}, coinbaseTX, GenTransaction()), []proof.CompactTrieAuditPath{{}})
		blkHash, _ := blk.Hash()
		hashes[i] = blkHash

		blocks[i] = blk
	}

	return blocks[1:]
}

// GenBlocksWithFork generates two chains of blocks with a common fork.
func GenBlocksWithFork(forkPoint *types.Block, forkheight uint64) ([]*types.Block, []*types.Block) {
	forkPointHash, _ := forkPoint.Hash()

	chain1 := GenBlocksAtHeight(forkheight, forkPointHash)
	chain2 := GenBlocksAtHeight(forkheight, forkPointHash)

	chain1[0].Parent = forkPointHash
	chain2[0].Parent = forkPointHash

	chain1 = append([]*types.Block{forkPoint}, chain1...)
	chain2 = append([]*types.Block{forkPoint}, chain2...)

	return chain1, chain2
}

// GenBlocksAtHeight Generates a slice of dummy blocks
func GenBlocksAtHeight(k uint64, forkHash crypto.Hash) []*types.Block {

	// let's generate some coinbase blocks
	hashes := make([]crypto.Hash, 10)

	blocks := make([]*types.Block, 10)

	var err error
	var i uint64
	height := k
	hashes[0] = forkHash
	for i = 1; i < 10; i++ {

		coinbaseTX := GenCoinbase(i)
		if err != nil {
			panic("failure to calculate test hash")
		}
		hdr := types.NewHeader(hashes[i-1], crypto.EmptyHash, hashes[i], crypto.EmptyHash, crypto.EmptyHash, []crypto.Hash{crypto.EmptyHash}, height, mathrand.Uint64(), uint64(time.Now().Unix()), crypto.SetBig(protocol.RegressionPoWLimit), crypto.SetBig(big.NewInt(171772)))
		blk := types.NewBlock(hdr, append([]*types.Transaction{}, coinbaseTX, GenTransaction()), []proof.CompactTrieAuditPath{{}})
		blkHash, _ := blk.Hash()
		hashes[i] = blkHash
		blocks[i] = blk
		height++
	}
	return blocks[1:]

}

// GenBlocksWithValidTime Generates a slice of dummy blocks with valid timestamp spacing
func GenBlocksWithValidTime(count int, height uint64) []*types.Block {

	// let's generate some coinbase blocks
	hashes := make([]crypto.Hash, count)

	blocks := make([]*types.Block, count)

	var err error
	var i uint64
	hashes[0] = crypto.SHA3Hash([]byte("test-genesis-hash"))
	ts := time.Now()

	for i = 1; i < uint64(count); i++ {

		coinbaseTX := GenCoinbase(i)
		if err != nil {
			panic("failure to calculate test hash")
		}
		hdr := types.NewHeader(hashes[i-1], crypto.EmptyHash, hashes[i], crypto.EmptyHash, crypto.EmptyHash, []crypto.Hash{crypto.EmptyHash}, height, 96580, uint64((ts.Add(time.Duration(19*i) * time.Second).Unix())), crypto.SetBig(protocol.RegressionPoWLimit), crypto.SetBig(big.NewInt(171772)))
		blk := types.NewBlock(hdr, append([]*types.Transaction{}, coinbaseTX, GenTransaction()), []proof.CompactTrieAuditPath{{}})
		blkHash, _ := blk.Hash()
		hashes[i] = blkHash

		blocks[i] = blk
		height++
	}

	return blocks[1:]

}

// GenCoinbase Generates a dummy coinbase transaction
func GenCoinbase(height uint64) *types.Transaction {
	v := GenerateRandomBytes(32)
	nullInput := []types.Input{{Origin: crypto.EmptyHash, Validator: v[:16]}}
	address, _ := addr.EncodeHash(GenerateRandomBytes(32), protocol.TestnetAddressPrefix)
	output := []types.Output{{Address: address, Value: protocol.GetBlockIssuance(height), Locktime: protocol.CoinbaseMaturity}}
	return types.NewTransaction(protocol.WhirlpoolConsensusVer, 0, 0, 10, types.NewCommitment(nullInput, output), types.NewWitness())
}

// GenOpenContracts Generates a list of dummy Open Contracts.
func GenOpenContracts(size int) []*types.OpenContract {

	var txids [][32]byte
	indexes := make([]uint32, 0)
	values := make([]uint64, 0)
	addresses := make([]string, 0)
	var h crypto.Hash
	for i := 0; i < size; i++ {
		key := make([]byte, 32)
		_, err := rand.Read(key)
		if err != nil {
			panic(err)
		}
		h = crypto.SHA3Hash(key)
		txids = append(txids, h)
		indexes = append(indexes, uint32(i*3))
		r := crypto.Blake256(txids[i][:])
		addr, _ := bech32.EncodeCloqAddress("cq", r[:])
		addresses = append(addresses, addr)
		values = append(values, uint64(i*100000/25))

	}
	ocss := make([]*types.OpenContract, 0)

	for i, txid := range txids {
		out := types.Output{Address: addresses[i], Value: values[i], Locktime: uint64(i)}
		ocs := &types.OpenContract{Contract: types.NewContract(txid, indexes[i], out), InclusionHeight: uint64(size % 375), Coinbase: false}
		ocss = append(ocss, ocs)
	}
	return ocss

}

// GenerateRandomBytes returns securely generated random bytes.
// credits https://stackoverflow.com/questions/35781197/generating-a-random-fixed-length-byte-array-in-go
func GenerateRandomBytes(n int) crypto.Hash {
	var b crypto.Hash
	_, err := rand.Read(b[:])
	// failure of this is caused by the underlying operating system.
	if err != nil {
		panic(err)
	}
	return b
}
