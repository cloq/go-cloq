# go-cloq

![cloq]()

---

go-cloq is an open-source reference implementation in Golang of the cloq protocol.
It adheres to the specification and can be used as a guide to write your own.

- Status : **ALPHA**

## License

go-cloq as all Cloq software is covered by the MIT License.

## Releases

- Whirlpool (net1) : Functional testnet. (internal)
- Tadpole (tesnet) : Testing in public stance. (public release)
- Magellan (mainnet) : Mainnet (actual big fireworks public release)
